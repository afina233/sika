<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-map-marker icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Mutasi TPK
                <div class="page-title-subheading">Proses mutasi seluruh keluarga dari suatu TPK ke TPK lain.
                </div>
            </div>
        </div>
        <div class="page-title-actions">
        </div>    
    </div>
</div>
<?php if ($this->session->mutasi == 1) { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Mutasi TPK berhasil dilakukan!</strong>
        </div>
    </div>
</div>
<?php } elseif ($this->session->mutasi == 2) { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Mutasi TPK gagal dilakukan!</strong> Tidak ada keluarga yang terdaftar pada TPK asal.
        </div>
    </div>
</div>
<?php } $this->session->mutasi = 0; ?>
<form method="POST" class="needs-validation" action="<?php echo base_url('PesertaController/mutasiTPKProses'); ?>" novalidate>
    <div class="tab-content">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative row form-group"><label for="area_pelayanan" class="col-sm-2 col-form-label">Area Pelayanan</label>
                            <div class="col-sm-8">
                                <select name="area_pelayanan" id="area_pelayanan" class="form-control" required>
                                    <option hidden="" value="">Pilih Area Pelayanan</option>
                                    <?php foreach ($area as $value): ?>
                                    <option value="<?php echo $value->kode_area ?>"><?php echo $value->nama_area ?></option>
                                    <?php endforeach ?>
                                </select>
                                <div class="invalid-feedback">
                                    Area Pelayanan tidak boleh kosong.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative row form-group"><label for="tpk_asal" class="col-sm-2 col-form-label">TPK Asal</label>
                            <div class="col-sm-8">
                                <select name="tpk_asal" id="tpk_asal" class="form-control" required>
                                    <option hidden="" value="">Pilih TPK</option>
                                </select>
                                <div class="invalid-feedback">
                                    TPK Asal tidak boleh kosong.
                                </div>

                            </div>
                        </div>
                        <div class="position-relative row form-group"><label for="alamat_tpk_asal" class="col-sm-2 col-form-label">Alamat TPK</label>
                            <div class="col-sm-8">
                                <select name="alamat_tpk_asal" id="alamat_tpk_asal" disabled="" class="form-control" required>
                                </select>
                            </div>
                        </div>
                        <div class="position-relative row form-group"><label for="kota_tpk_asal" class="col-sm-2 col-form-label">Kota TPK</label>
                            <div class="col-sm-8">
                                <select name="kota_tpk_asal" id="kota_tpk_asal" disabled="" class="form-control" required>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative row form-group"><label for="tpk_tujuan" class="col-sm-2 col-form-label">TPK Tujuan</label>
                            <div class="col-sm-8">
                                <select name="tpk_tujuan" id="tpk_tujuan" class="form-control" required>
                                    <option hidden="" value="">Pilih TPK</option>
                                </select>
                                <div class="invalid-feedback">
                                    TPK Tujuan tidak boleh kosong.
                                </div>

                            </div>
                        </div>
                        <div class="position-relative row form-group"><label for="alamat_tpk_tujuan" class="col-sm-2 col-form-label">Alamat TPK</label>
                            <div class="col-sm-8">
                                <select name="alamat_tpk_tujuan" id="alamat_tpk_tujuan" disabled="" class="form-control" required>
                                </select>
                            </div>
                        </div>
                        <div class="position-relative row form-group"><label for="kota_tpk_tujuan" class="col-sm-2 col-form-label">Kota TPK</label>
                            <div class="col-sm-8">
                                <select name="kota_tpk_tujuan" id="kota_tpk_tujuan" disabled="" class="form-control" required>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="col-md-12" align="center">
                    <button class="mt-1 btn btn-primary" data-toggle="modal">Mutasi TPK</button>
                </div>
            </div>
        </div>
    </div>
</form>