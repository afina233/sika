<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-add-user icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Edit Anak
                <div class="page-title-subheading">Penyuntingan data anak.
                </div>
            </div>
        </div>  
    </div>
</div>

<?php if ($this->session->berhasil_edit == 1){?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Peserta telah berhasil di-edit!</strong>
        </div>
    </div>  
</div>
<?php } elseif($this->session->gagal_edit == 1) { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>NIK tidak dapat diubah!</strong> NIK sudah digunakan oleh peserta lain.
        </div>
    </div>
</div>
<?php } elseif($this->session->input_berhasil == 1) { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Peserta baru telah berhasil ditambahkan!</strong>
        </div>
    </div>  
</div>
<?php } $this->session->berhasil_edit = 0; $this->session->gagal_edit = 0; $this->session->input_berhasil = 0; ?>




<form action="<?php echo base_url('PesertaController/editAnak/'.$peserta->row()->kode_peserta); ?>" enctype="multipart/form-data"  method="POST" class="needs-validation" novalidate>

    <div class="tab-content">
        <div class="row">
            <div class="col-md-6">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Foto Anak</h5>
                        <div class="position-relative row form-group">
                            <img class="col-sm-3 col-form-label" name="foto_kk_preview" id="image-preview" src="
                            <?php 
                                
                            if ($peserta_data_pribadi->row()->foto) {
                                echo base_url().'../uploads/foto/'.$peserta_data_pribadi->row()->foto;
                            }else{
                            
                                echo base_url() . '../assets/images/user-placeholder.jpg';
                            }    
                            ?>">
                            
                        </div>
                        <div class="position-relative row form-group">
                            <div class="col-sm-10">
                                <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                <input type="file" name="foto_kk" id="image-source" accept=".jpg,.jpeg,.png,.bmp" onchange="previewImage();" class="form-control-file">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Anggota Keluarga</h5>
                        <div class="scroll-area-sm">
                            <div class="scrollbar-container ps--active-y">
                                <table class="mb-0 table table-striped">
                                    <tbody>
                                    <?php foreach ($all_peserta as $value): ?>
                                    <tr>
                                        <th scope="row">
                                        <?php foreach ($jenis_peserta as $value2): ?>
                                        <?php if ($value2->kode_jenis_peserta == $value->kode_jenis_peserta): ?>
                                        <?php echo $value2->nama_jenis_peserta ?>
                                        <?php endif ?>
                                        <?php endforeach ?>
                                        </th>
                                        <td>
                                        <?php foreach ($jenis_peserta as $value2) {
                                            if ($value2->kode_jenis_peserta == $value->kode_jenis_peserta) {
                                                if ($value2->kode_group_status_keluarga == 1) { ?>
                                                <td><a href="<?php echo base_url('PesertaController/pesertaKK/'.$value->kode_peserta) ?>">
                                                    <?php echo $value->nikes ?>
                                                </a></td>
                                                <?php }elseif ($value2->kode_group_status_keluarga == 2) { ?>
                                                <td><a href="<?php echo base_url('PesertaController/pesertaPasangan/'.$value->kode_peserta) ?>">
                                                    <?php echo $value->nikes ?>
                                                </a></td>
                                                <?php }elseif ($value2->kode_group_status_keluarga == 3) { ?>
                                                <td><a href="<?php echo base_url('PesertaController/pesertaAnak/'.$value->kode_peserta) ?>">
                                                    <?php echo $value->nikes ?>
                                                </a></td>
                                                <?php
                                                }
                                            }
                                        } ?>
                                        </td>
                                        <td>
                                        <?php foreach ($jenis_peserta as $value2) {
                                            if ($value2->kode_jenis_peserta == $value->kode_jenis_peserta) {
                                                if ($value2->kode_group_status_keluarga == 1) { ?>
                                                <td><a href="<?php echo base_url('PesertaController/pesertaKK/'.$value->kode_peserta) ?>">
                                                    <?php echo $value->nama ?>
                                                </a></td>
                                                <?php }elseif ($value2->kode_group_status_keluarga == 2) { ?>
                                                <td><a href="<?php echo base_url('PesertaController/pesertaPasangan/'.$value->kode_peserta) ?>">
                                                    <?php echo $value->nama ?>
                                                </a></td>
                                                <?php }elseif ($value2->kode_group_status_keluarga == 3) { ?>
                                                <td><a href="<?php echo base_url('PesertaController/pesertaAnak/'.$value->kode_peserta) ?>">
                                                    <?php echo $value->nama ?>
                                                </a></td>
                                                <?php
                                                }
                                            }
                                        } ?>
                                        </td>
                                        <td>
                                            <?php if ($value->status_faskes == 1) {
                                                echo "Y";
                                            }else{
                                                echo "T";
                                            } ?>
                                        </td>
                                    </tr>     
                                    <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="main-card mb-3 card">
            <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="position-relative row form-group"><label for="no_peserta" class="col-sm-2 col-form-label">Nomor Peserta</label>
                                <div class="col-sm-8">
                                    <input name="no_peserta" disabled="" id="no_peserta" value=" <?php echo $peserta->row()->no_peserta; ?> " class="form-control">
                                </div>
                            </div>
                            <div class="position-relative row form-group"><label for="jenis_peserta_kk_edit" class="col-sm-2 col-form-label">Jenis Peserta</label>
                                <div class="col-sm-8">
                                    <select name="jenis_peserta_kk_edit" id="jenis_peserta_kk_edit" class="form-control" disabled="">
                                        <?php foreach ($jenis_peserta as $value): ?>
                                            <?php if ($value->kode_group_status_keluarga == 3): ?>
                                                <?php if ($value->kode_jenis_peserta == $peserta->row()->kode_jenis_peserta): ?>
                                                    <option value="<?php echo $value->kode_jenis_peserta ?>" selected=""><?php echo $value->nama_jenis_peserta ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo $value->kode_jenis_peserta ?>"><?php echo $value->nama_jenis_peserta ?></option>
                                                <?php endif ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>           
                        </div>
                        <?php foreach ($jenis_peserta as $value): ?>
                            <?php if ($value->kode_jenis_peserta == $peserta->row()->kode_jenis_peserta): ?>
                            <input type="text" hidden="" id="kode_group_jenis_peserta" name="kode_group_jenis_peserta" value="<?php echo $value->kode_group_jenis_peserta ?>">
                            <?php endif ?>
                        <?php endforeach ?>
                        <div class="col-md-6">
                            <div class="position-relative form-group">
                                <div class="position-relative row form-group"><label for="kelompok_transaksi_anak_edit" class="col-sm-2 col-form-label">Kelompok Transaksi</label>
                                    <div class="col-sm-8">
                                        <select name="kelompok_transaksi_anak_edit" id="kelompok_transaksi_anak_edit" class="form-control" required>
                                            <option value="" hidden="">Pilih Kelompok Transaksi</option>
                                            <?php foreach ($kelompok_transaksi as $value) { ?>
                                            <?php if ($value->kode_kelompok_transaksi == 02 || $value->kode_kelompok_transaksi == 88) { ?>
                                            <option value="<?php echo $value->kode_kelompok_transaksi; ?>"><?php echo $value->nama_kelompok_transaksi; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <div class="invalid-feedback">
                                            Kelompok Transaksi tidak boleh kosong.
                                        </div>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="jenis_transaksi_anak_edit" class="col-sm-2 col-form-label">Jenis Transaksi</label>
                                    <div class="col-sm-8">
                                        <select name="jenis_transaksi_anak_edit" id="jenis_transaksi_anak_edit" class="form-control" required>
                                            <option value="" hidden="">Pilih Jenis Transaksi</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Jenis Transaksi tidak boleh kosong.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-0" class="active nav-link">Data Pribadi</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-1" class="nav-link">Info Lain-Lain</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-2" class="nav-link">Data Pendukung</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-eg11-0" role="tabpanel">
                        <div class="form-row">
                            <div class="col-md-6"> 
                                <div class="position-relative row form-group"><label for="nik" class="col-sm-2 col-form-label">NIK</label>
                                    <div class="col-sm-8">
                                        <input name="nik_show" id="nik_show" value="<?php echo $nik->row()->nik; ?>" class="form-control" disabled>
                                        <input name="nik" id="nik" value="<?php echo $nik->row()->nik; ?>" class="form-control" hidden>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="nama" class="col-sm-2 col-form-label">Nama Anak</label>
                                    <div class="col-sm-8">
                                        <input name="nama" id="nama" value="<?php echo $peserta_data_pribadi->row()->nama; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                    <div class="col-sm-3">
                                        <input name="tempat_lahir" id="tempat_lahir" value="<?php echo $peserta_data_pribadi->row()->tempat_lahir; ?>" class="form-control">
                                    </div>
                                    <label for="tgl_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                    <div class="col-sm-3">
                                        <?php  
                                            $newDate = date("d/m/Y", strtotime($peserta_data_pribadi->row()->tgl_lahir)); 
                                        ?>
                                        <input name="tgl_lahir" autocomplete="off" id="datepicker_tgl_lahir"  placeholder="dd/mm/yyyy" value="<?php echo $newDate; ?>" class="form-control" required>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="usia" class="col-sm-2 col-form-label">Usia</label>
                                    <?php 
                                        $dob = new DateTime($peserta_data_pribadi->row()->tgl_lahir);
                                        $today = new DateTime;
                                        $age = $today->diff($dob);
                                        // echo $age->format('%y Years, %m Months and %d Days');
                                    ?>
                                    <div class="col-sm-2">
                                        <input name="tahun" disabled="" id="tahun" value="<?php echo $age->format('%y'); ?>" class="form-control">
                                    </div>
                                    <label for="tahun" class="col-sm-1.5 col-form-label">tahun</label>
                                    <div class="col-sm-2">
                                        <input name="bulan" disabled="" id="bulan" value="<?php echo $age->format('%m'); ?>" class="form-control">
                                    </div>
                                    <label for="bulan" class="col-sm-1.5 col-form-label">bulan</label>
                                    <div class="col-sm-2">
                                        <input name="hari" disabled="" id="hari" value="<?php echo $age->format('%d'); ?>" class="form-control">
                                    </div>
                                    <label for="hari" class="col-sm-1.5 col-form-label">hari</label>
                                </div>
                                <div class="position-relative row form-group"><label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                    <div class="col-sm-2">
                                        <div class="position-relative form-check">
                                            <label class="form-check-label"><input name="jenis_kelamin" type="radio" class="form-check-input" value="Laki-laki"
                                            <?php if ($peserta_data_pribadi->row()->jenis_kelamin == "Laki-laki") {
                                                echo "checked";
                                            } ?>
                                            >Laki-laki</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="position-relative form-check">
                                            <label class="form-check-label"><input name="jenis_kelamin" type="radio" class="form-check-input" value="Perempuan"
                                            <?php if ($peserta_data_pribadi->row()->jenis_kelamin == "Perempuan") {
                                                echo "checked";
                                            } ?>
                                            
                                            >Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="agama" class="col-sm-2 col-form-label">Agama</label>
                                    <div class="col-sm-8">
                                        <select name="agama" id="agama" class="form-control">
                                        <?php foreach ($agama as $value): ?>
                                        <?php if ($value->kode_agama == $peserta_data_pribadi->row()->kode_agama): ?>
                                            <option value="<?php echo $value->kode_agama; ?>" selected><?php echo $value->nama_agama; ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo $value->kode_agama; ?>"><?php echo $value->nama_agama; ?></option>
                                        <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="gol_darah" class="col-sm-2 col-form-label">Golongan Darah</label>
                                    <div class="col-sm-2">
                                        <select name="gol_darah" id="gol_darah" class="form-control">
                                            <option value="A"
                                            <?php if ($peserta_data_pribadi->row()->gol_darah == "A") {
                                                echo "selected";
                                            } ?>
                                            >A</option>
                                            <option value="B"
                                            <?php if ($peserta_data_pribadi->row()->gol_darah == "B") {
                                                echo "selected";
                                            } ?>
                                            >B</option>
                                            <option value="AB"
                                            <?php if ($peserta_data_pribadi->row()->gol_darah == "AB") {
                                                echo "selected";
                                            } ?>
                                            >AB</option>
                                            <option value="O"
                                            <?php if ($peserta_data_pribadi->row()->gol_darah == "O") {
                                                echo "selected";
                                            } ?>
                                            >O</option>
                                        </select>
                                    </div>
                                    <label for="gol_darah" class="col-sm-2 col-form-label">Rhesus</label>
                                    <div class="col-sm-2">
                                        <select name="rhesus" id="rhesus" class="form-control">
                                            <option value="1"
                                            <?php if ($peserta_data_pribadi->row()->rhesus == "1") {
                                                echo "selected";
                                            } ?>
                                            >+</option>
                                            <option value="0"
                                            <?php if ($peserta_data_pribadi->row()->rhesus == "0") {
                                                echo "selected";
                                            } ?>
                                            >-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="nik" class="col-sm-2 col-form-label">Nomor KTP</label>
                                    <div class="col-sm-8">
                                        <input name="no_ktp" id="no_ktp" value="<?php echo $peserta_data_pribadi->row()->no_ktp; ?>" class="form-control">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative row form-group"><label for="nikes" class="col-sm-2 col-form-label">NIKES</label>
                                    <div class="col-sm-8">
                                        <input name="nikes_show" disabled="" id="nikes_show" value="<?php echo $peserta->row()->nikes; ?>" class="form-control">
                                        <input name="nikes" id="nikes" value="<?php echo $peserta->row()->nikes; ?>" class="form-control" hidden>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="anak_ke" class="col-sm-2 col-form-label">Anak Ke-</label>
                                    <div class="col-sm-3">
                                        <input name="anak_ke" disabled="" id="anak_ke" value="<?php echo $peserta_data_info_lain->row()->no_anggota_keluarga_2; ?>" class="form-control">
                                    </div>
                                    <label for="pasangan_ke" class="col-sm-2 col-form-label">Dari Pasangan Ke-</label>
                                    <div class="col-sm-3">
                                        <input name="pasangan_ke" disabled="" id="pasangan_ke" value="<?php echo $peserta_data_info_lain->row()->no_anggota_keluarga; ?>" class="form-control">
                                    </div>
                                </div>
                                
                                
                                <div class="position-relative row form-group"><label for="no_bpjs" class="col-sm-2 col-form-label">Nomor BPJS</label>
                                    <div class="col-sm-8">
                                        <input name="no_bpjs" id="no_bpjs" value="<?php echo $peserta_data_pribadi->row()->no_bpjs; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="area_pelayanan_peserta" class="col-sm-2 col-form-label">Area Pelayanan</label>
                                    <div class="col-sm-8">
                                        <select name="area_pelayanan" id="area_pelayanan" class="form-control">
                                        <?php foreach ($area as $value): ?>
                                            <?php if ($value->kode_area != 0): ?>
                                                <?php if ($value->kode_area == $peserta_data_kepegawaian->row()->kode_area): ?>
                                                <option value="<?php echo $value->kode_area; ?>" selected><?php echo $value->nama_area; ?></option>
                                                <?php else: ?>
                                                <option value="<?php echo $value->kode_area; ?>"><?php echo $value->nama_area; ?></option>
                                                <?php endif ?>
                                                
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="tpk" class="col-sm-2 col-form-label" onchange="myFunction(event)">TPK</label>
                                    <div class="col-sm-8">
                                        <select name="tpk" id="tpk" class="form-control">
                                        <?php foreach ($tpk as $value): ?>
                                            <?php if ($value->kode_area == $peserta_data_kepegawaian->row()->kode_area): ?>
                                                <?php if ($peserta_data_tpk->row()->kode_tpk == $value->kode_tpk): ?>
                                                <option value="<?php echo $value->kode_tpk; ?>" selected><?php echo $value->nama_tpk; ?></option>
                                                <?php else: ?>    
                                                <option value="<?php echo $value->kode_tpk; ?>"><?php echo $value->nama_tpk; ?></option>
                                                <?php endif ?>
                                            <?php endif ?>
                                        <?php endforeach ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="kota_tpk" class="col-sm-2 col-form-label">Kota TPK</label>
                                    <div class="col-sm-8">
                                        <select name="kota_tpk" id="kota_tpk" class="form-control" disabled="">
                                        <?php foreach ($tpk as $key => $value): ?>
                                            <?php if ($peserta_data_tpk->row()->kode_tpk == $value->kode_tpk): ?>
                                                <?php foreach ($kota_kantor as $value_2): ?>
                                                    <?php if ($value->kode_kota_kantor == $value_2->kode_kota_kantor): ?>
                                                    <option value="<?php echo $value_2->kode_kota_kantor ?>"><?php echo $value_2->nama_kota_kantor ?></option>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="alamat_tpk" class="col-sm-2 col-form-label">Alamat TPK</label>
                                    <div class="col-sm-8">
                                        <select name="alamat_tpk" id="alamat_tpk" class="form-control" disabled="">
                                        <?php foreach ($tpk as $value): ?>
                                            <?php if ($value->kode_area == $peserta_data_kepegawaian->row()->kode_area): ?>
                                                <?php if ($peserta_data_tpk->row()->kode_tpk == $value->kode_tpk): ?>
                                                <option value="<?php echo $value_2->kode_kota_kantor ?>"><?php echo $value->alamat ?></option>
                                                <?php endif ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="status_faskes" class="col-sm-2 col-form-label">Status Faskes</label>
                                    <div class="col-sm-2">
                                        <div class="position-relative form-check">
                                            <label class="form-check-label"><input name="status_faskes" type="radio" class="form-check-input" value=1
                                            <?php if ($peserta->row()->status_faskes == "1") {
                                                echo "checked ";
                                            } if ($peserta_data_info_lain->row()->status_meninggal == "1") {
                                                echo "disabled";
                                            }?>
                                            >Ya</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="position-relative form-check">
                                            <label class="form-check-label"><input name="status_faskes" type="radio" class="form-check-input" value=0
                                            <?php if ($peserta->row()->status_faskes == "0") {
                                                echo "checked ";
                                            } if ($peserta_data_info_lain->row()->status_meninggal == "1") {
                                                echo "disabled";
                                            }?>
                                            >Tidak</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="tgl_faskes" class="col-sm-2 col-form-label">Tanggal Faskes</label>
                                    <div class="col-sm-8">
                                        <?php  
                                            $newDate = date("d/m/Y", strtotime($peserta->row()->tgl_faskes)); 
                                        ?>
                                        <?php if (empty($peserta->row()->tgl_faskes)): ?>
                                        <input name="tgl_faskes" autocomplete="off" id="datepicker_tgl_faskes" placeholder="dd/mm/yyyy" class="form-control">                                        
                                        <?php else: ?>
                                        <input name="tgl_faskes" autocomplete="off" id="datepicker_tgl_faskes" placeholder="dd/mm/yyyy" value="<?php echo $newDate; ?>" class="form-control">
                                        <?php endif ?>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-eg11-1" role="tabpanel">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="position-relative row form-group"><label for="alamat_ktp" class="col-sm-2 col-form-label">Alamat KTP</label>
                                    <div class="col-sm-8">
                                        <textarea name="alamat_ktp" id="alamat_ktp" class="form-control"><?php echo $peserta_data_alamat_ktp->row()->alamat; ?></textarea>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="rt_ktp" class="col-sm-2 col-form-label">RT</label>
                                    <div class="col-sm-3">
                                        <input name="rt_ktp" id="rt_ktp" value="<?php echo $peserta_data_alamat_ktp->row()->rt; ?>" class="form-control">
                                    </div>
                                    <label for="rw_ktp" class="col-sm-2 col-form-label">RW</label>
                                    <div class="col-sm-3">
                                        <input name="rw_ktp" id="rw_ktp" value="<?php echo $peserta_data_alamat_ktp->row()->rw; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="provinsi_ktp" class="col-sm-2 col-form-label">Provinsi</label>
                                    <div class="col-sm-3">
                                        <select name="provinsi_ktp" id="provinsi_ktp" class="form-control">
                                        <?php foreach ($provinsi as $value): ?>
                                            <?php if ($peserta_data_alamat_ktp->row()->kode_provinsi == $value->id): ?>
                                            <option value="<?php echo $value->id ?>" selected><?php echo $value->name ?></option>
                                            <?php else: ?>
                                            <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                    <label for="kabupaten_ktp" class="col-sm-2 col-form-label">Kota</label>
                                    <div class="col-sm-3">
                                        <select name="kabupaten_ktp" id="kabupaten_ktp" class="form-control">
                                        <?php foreach ($kabupaten as $value): ?>
                                            <?php if ($peserta_data_alamat_ktp->row()->kode_provinsi == $value->province_id): ?>
                                                <?php if ($peserta_data_alamat_ktp->row()->kode_kabupaten == $value->id): ?>
                                                <option value="<?php echo $value->id ?>" selected><?php echo $value->name ?></option>
                                                <?php else: ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                <?php endif ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="kecamatan_ktp" class="col-sm-2 col-form-label">Kecamatan</label>
                                    <div class="col-sm-3">
                                        <select name="kecamatan_ktp" id="kecamatan_ktp" class="form-control">
                                        <?php foreach ($kecamatan as $value): ?>
                                            <?php if ($peserta_data_alamat_ktp->row()->kode_kabupaten == $value->regency_id): ?>
                                                <?php if ($peserta_data_alamat_ktp->row()->kode_kecamatan == $value->id): ?>
                                                <option value="<?php echo $value->id ?>" selected><?php echo $value->name ?></option>
                                                <?php else: ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                <?php endif ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                    <label for="kelurahan_ktp" class="col-sm-2 col-form-label">Kelurahan</label>
                                    <div class="col-sm-3">
                                        <select name="kelurahan_ktp" id="kelurahan_ktp" class="form-control">
                                        <?php foreach ($kelurahan as $value): ?>
                                            <?php if ($peserta_data_alamat_ktp->row()->kode_kecamatan == $value->district_id): ?>
                                                <?php if ($peserta_data_alamat_ktp->row()->kode_kelurahan == $value->id): ?>
                                                <option value="<?php echo $value->id ?>" selected><?php echo $value->name ?></option>
                                                <?php else: ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                <?php endif ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="kode_pos_ktp" class="col-sm-2 col-form-label">Kode POS</label>
                                    <div class="col-sm-2">
                                        <input name="kode_pos_ktp" id="kode_pos_ktp" value="<?php echo $peserta_data_alamat_ktp->row()->kode_pos; ?>" class="form-control">
                                    </div>
                                    <label for="no_telp_ktp" class="col-sm-2 col-form-label">Nomor Telepon</label>
                                    <div class="col-sm-4">
                                        <input name="no_telp_ktp" id="no_telp_ktp" value="<?php echo $peserta_data_alamat_ktp->row()->no_telp; ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative row form-group"><label for="alamat_domisili" class="col-sm-2 col-form-label">Alamat Domisili</label>
                                    <div class="col-sm-8">
                                        <textarea name="alamat_domisili" id="alamat_domisili" class="form-control"><?php echo $peserta_data_alamat_domisili->row()->alamat; ?></textarea>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="rt_domisili" class="col-sm-2 col-form-label">RT</label>
                                    <div class="col-sm-3">
                                        <input name="rt_domisili" id="rt_domisili" value="<?php echo $peserta_data_alamat_domisili->row()->rt; ?>" class="form-control">
                                    </div>
                                    <label for="rw_domisili" class="col-sm-2 col-form-label">RW</label>
                                    <div class="col-sm-3">
                                        <input name="rw_domisili" id="rw_domisili" value="<?php echo $peserta_data_alamat_domisili->row()->rw; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="provinsi_domisili" class="col-sm-2 col-form-label">Provinsi</label>
                                    <div class="col-sm-3">
                                        <select name="provinsi_domisili" id="provinsi_domisili" class="form-control">
                                        <?php foreach ($provinsi as $value): ?>
                                            <?php if ($peserta_data_alamat_domisili->row()->kode_provinsi == $value->id): ?>
                                            <option value="<?php echo $value->id ?>" selected><?php echo $value->name ?></option>
                                            <?php else: ?>
                                            <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                    <label for="kabupaten_domisili" class="col-sm-2 col-form-label">Kota</label>
                                    <div class="col-sm-3">
                                        <select name="kabupaten_domisili" id="kabupaten_domisili" class="form-control">
                                        <?php foreach ($kabupaten as $value): ?>
                                            <?php if ($peserta_data_alamat_domisili->row()->kode_provinsi == $value->province_id): ?>
                                                <?php if ($peserta_data_alamat_domisili->row()->kode_kabupaten == $value->id): ?>
                                                <option value="<?php echo $value->id ?>" selected><?php echo $value->name ?></option>
                                                <?php else: ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                <?php endif ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="kecamatan_domisili" class="col-sm-2 col-form-label">Kecamatan</label>
                                    <div class="col-sm-3">
                                        <select name="kecamatan_domisili" id="kecamatan_domisili" class="form-control">
                                        <?php foreach ($kecamatan as $value): ?>
                                            <?php if ($peserta_data_alamat_domisili->row()->kode_kabupaten == $value->regency_id): ?>
                                                <?php if ($peserta_data_alamat_domisili->row()->kode_kecamatan == $value->id): ?>
                                                <option value="<?php echo $value->id ?>" selected><?php echo $value->name ?></option>
                                                <?php else: ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                <?php endif ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                    <label for="kelurahan_domisili" class="col-sm-2 col-form-label">Kelurahan</label>
                                    <div class="col-sm-3">
                                        <select name="kelurahan_domisili" id="kelurahan_domisili" class="form-control">
                                        <?php foreach ($kelurahan as $value): ?>
                                            <?php if ($peserta_data_alamat_domisili->row()->kode_kecamatan == $value->district_id): ?>
                                                <?php if ($peserta_data_alamat_domisili->row()->kode_kelurahan == $value->id): ?>
                                                <option value="<?php echo $value->id ?>" selected><?php echo $value->name ?></option>
                                                <?php else: ?>
                                                <option value="<?php echo $value->id ?>"><?php echo $value->name ?></option>
                                                <?php endif ?>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="kode_pos_domisili" class="col-sm-2 col-form-label">Kode POS</label>
                                    <div class="col-sm-2">
                                        <input name="kode_pos_domisili" id="kode_pos_domisili" value="<?php echo $peserta_data_alamat_domisili->row()->kode_pos; ?>" class="form-control">
                                    </div>
                                    <label for="no_telp_domisili" class="col-sm-2 col-form-label">Nomor Telepon</label>
                                    <div class="col-sm-4">
                                        <input name="no_telp_domisili" id="no_telp_domisili" value="<?php echo $peserta_data_alamat_domisili->row()->no_telp; ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" align="center">
                                <div class="col-sm-10">
                                    <div class="position-relative form-check"><label class="form-check-label">
                                        <input id="domisili_ktp" type="checkbox" class="form-check-input"> Alamat Domisili sama dengan Alamat KTP</label>
                                    </div>
                                </div>
                                <div class="divider"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative row form-group"><label for="no_hp" class="col-sm-2 col-form-label">Nomor HP</label>
                                    <div class="col-sm-8">
                                        <input name="no_hp" id="no_hp" value="<?php echo $peserta_data_info_lain->row()->no_hp; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="nama_pemilik_rekening" class="col-sm-2 col-form-label">Nama Pemilik Rekening</label>
                                    <div class="col-sm-8">
                                        <input name="nama_pemilik_rekening" id="nama_pemilik_rekening" value="<?php echo $peserta_data_bank->row()->nama_pemilik_rekening; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="nama_bank" class="col-sm-2 col-form-label">Nama Bank</label>
                                    <div class="col-sm-8">
                                        <select name="nama_bank" id="nama_bank" class="form-control">
                                        <?php foreach ($bank as $value): ?>
                                        <?php if ($value->kode_bank == $peserta_data_bank->row()->kode_bank): ?>
                                            <option value="<?php echo $value->kode_bank; ?>" selected><?php echo $value->nama_bank; ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo $value->kode_bank; ?>"><?php echo $value->nama_bank; ?></option>
                                        <?php endif ?>
                                        <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="no_rekening_bank" class="col-sm-2 col-form-label">Nomor Rekening Bank</label>
                                    <div class="col-sm-4">
                                        <input name="no_rekening_bank" id="no_rekening_bank" value="<?php echo $peserta_data_bank->row()->no_rekening; ?>" class="form-control">
                                    </div>
                                    <label for="status_kartu" class="col-sm-2 col-form-label">Status Kartu</label>
                                    <div class="col-sm-2">
                                        <select name="status_kartu" id="status_kartu" class="form-control">
                                            <option value="Belum Cetak"
                                            <?php if ($peserta_data_bank->row()->status_kartu == "Belum Cetak") {
                                                echo "selected";
                                            } ?>
                                            >Belum Cetak</option>
                                            <option value="Expired"
                                            <?php if ($peserta_data_bank->row()->status_kartu == "Expired") {
                                                echo "selected";
                                            } ?>
                                            >Expired</option>
                                            <option value="Sudah Cetak"
                                            <?php if ($peserta_data_bank->row()->status_kartu == "Sudah Cetak") {
                                                echo "selected";
                                            } ?>
                                            >Sudah Cetak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="tgl_cetak" class="col-sm-2 col-form-label">Tanggal Cetak</label>
                                    <div class="col-sm-3">
                                        <?php  
                                            $newDate = date("d/m/Y", strtotime($peserta_data_bank->row()->tgl_cetak)); 
                                        ?>
                                        <?php if (empty($peserta_data_bank->row()->tgl_cetak)): ?>
                                        <input name="tgl_cetak" autocomplete="off" id="datepicker_tgl_cetak" placeholder="dd/mm/yyyy" class="form-control">                                        
                                        <?php else: ?>
                                        <input name="tgl_cetak" autocomplete="off" id="datepicker_tgl_cetak"  placeholder="dd/mm/yyyy" value="<?php echo $newDate; ?>" class="form-control">
                                        <?php endif ?>
                                        
                                    </div>
                                    <label for="tgl_akhir_kartu" class="col-sm-2 col-form-label">Tanggal Akhir Kartu</label>
                                    <div class="col-sm-3">
                                        <?php  
                                            $newDate = date("d/m/Y", strtotime($peserta_data_bank->row()->tgl_akhir_kartu)); 
                                        ?>
                                        <?php if (empty($peserta_data_bank->row()->tgl_akhir_kartu)): ?>
                                        <input name="tgl_akhir_kartu" autocomplete="off" id="datepicker_tgl_akhir_kartu" placeholder="dd/mm/yyyy" class="form-control">                                        
                                        <?php else: ?>
                                        <input name="tgl_akhir_kartu" autocomplete="off" id="datepicker_tgl_akhir_kartu"  placeholder="dd/mm/yyyy" value="<?php echo $newDate; ?>" class="form-control">
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="position-relative row form-group"><label for="tgl_meninggal" class="col-sm-2 col-form-label">Tanggal Meninggal</label>
                                    <div class="col-sm-8">
                                        <?php  
                                            $newDate = date("d/m/Y", strtotime($peserta_data_info_lain->row()->tgl_meninggal)); 
                                        ?>
                                        <?php if (empty($peserta_data_info_lain->row()->tgl_meninggal)): ?>
                                        <input name="tgl_meninggal" autocomplete="off" id="datepicker_tgl_meninggal" placeholder="dd/mm/yyyy" class="form-control">                                        
                                        <?php else: ?>
                                        <input name="tgl_meninggal" autocomplete="off" id="datepicker_tgl_meninggal"  placeholder="dd/mm/yyyy" value="<?php echo $newDate; ?>" class="form-control">
                                        <?php endif ?>
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="alasan_berhenti_peserta" class="col-sm-2 col-form-label">Nomor HP</label>
                                    <div class="col-sm-8">
                                        <input name="alasan_berhenti_peserta" id="alasan_berhenti_peserta" value="<?php echo $peserta_data_info_lain->row()->alasan_berhenti_peserta; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="position-relative row form-group"><label for="catatan" class="col-sm-2 col-form-label">Catatan</label>
                                    <div class="col-sm-8">
                                        <textarea name="catatan" id="catatan" class="form-control"><?php echo $peserta_data_info_lain->row()->catatan;?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-eg11-2" role="tabpanel">
                        <h5 class="card-title">Dokumen Pendukung</h5>
                        <div class="form-row">
                            <table class="mb-0 table table-borderless">
                                <thead>
                                <tr>
                                    <th>Nomor Dokumen</th>
                                    <th>Perihal</th>
                                    <th>Tanggal Dokumen</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $num = 0; ?>
                                <?php foreach ($peserta_data_pendukung as $value): ?>
                                <?php $num = $num + 1; ?>
                                <tr>
                                    <th scope="row">
                                        <div class="position-relative row form-group">
                                            <div class="col-sm-12">
                                                <input name="no_dokumen_edit[]" id="no_dokumen" value="<?php echo $value->no_dokumen; ?>" class="form-control">
                                            </div>
                                        </div>
                                    </th>
                                    <td>
                                        <div class="position-relative row form-group">
                                            <div class="col-sm-12">
                                                <input name="perihal_edit[]" id="perihal" value="<?php echo $value->perihal; ?>" class="form-control">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <?php  
                                            $newDate = date("d/m/Y", strtotime($value->tgl_dokumen)); 
                                        ?>
                                        <div class="position-relative row form-group">
                                            <div class="col-sm-12">
                                                <?php if (empty($value->tgl_dokumen)): ?>
                                                <input autocomplete="off" name="tgl_dokumen_edit[]" id="<?php echo 'tgl_dokumen_edit_'.$num; ?>" placeholder="dd/mm/yyyy" class="form-control">
                                                <?php else: ?>
                                                <input autocomplete="off" name="tgl_dokumen_edit[]" id="<?php echo 'tgl_dokumen_edit_'.$num; ?>" placeholder="dd/mm/yyyy" value="<?php echo $newDate; ?>" class="form-control">
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <script>
                                            $(<?php echo "tgl_dokumen_edit_".$num; ?>).datepicker({
                                                format: 'dd/mm/yyyy',
                                                uiLibrary: 'bootstrap'
                                            });
                                        </script>
                                    </td>
                                    <td>
                                        <div class="position-relative row form-group">
                                            <div class="col-sm-12">
                                                <button type="button" aria-haspopup="true" aria-expanded="false" class="btn btn-primary" title="Lihat dokumen"
                                                onclick=" window.open('<?php echo base_url('../uploads/data_pendukung/'.$value->file) ?>','_blank')">
                                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                                        <i class="fa fa-eye fa-w-20"></i>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                        
                                </tr>
                                <?php endforeach ?>

                                </tbody>
                            </table>
                        </div>
                        <div class="divider"></div>
                        <h5 class="card-title">Upload Dokumen Pendukung Baru</h5>
                        <div class="form-row">
                                <table class="mb-0 table table-borderless">
                                    <thead>
                                    <tr>
                                        <th>Nomor Dokumen</th>
                                        <th>Perihal</th>
                                        <th>Tanggal Dokumen</th>
                                        <th>File</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="no_dokumen_1" id="no_dokumen_1" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="perihal_1" id="perihal_1" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="tgl_dokumen_1" autocomplete="off" id="datepicker_tgl_dokumen_1" placeholder="dd/mm/yyyy" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>

                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                                    <input type="file" name="file_1" id="file_1" accept=".pdf" class="form-control-file">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="no_dokumen_2" id="no_dokumen_2" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="perihal_2" id="perihal_2" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="tgl_dokumen_2" autocomplete="off" id="datepicker_tgl_dokumen_2" placeholder="dd/mm/yyyy" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>

                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                                    <input type="file" name="file_2" id="file_2" accept=".pdf" class="form-control-file">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="no_dokumen_3" id="no_dokumen_3" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="perihal_3" id="perihal_3" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="tgl_dokumen_3" autocomplete="off" id="datepicker_tgl_dokumen_3" placeholder="dd/mm/yyyy" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>

                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                                    <input type="file" name="file_3" id="file_3" accept=".pdf" class="form-control-file">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="col-md-12" align="center">
                    <button class="mt-1 btn btn-primary" 
                        <?php if ($peserta_data_info_lain->row()->status_meninggal == 1) {
                            echo "disabled";
                        } ?>
                    >Simpan</button>
                </div>
            </div>
        </div>
    </div>

</form>
<script type="text/javascript">
    

    $(document).ready(function(){
       

        $('#kelompok_transaksi_peserta').change(function(){ 
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url('Sika/getJenisTransaksi');?>",
                method : "POST",
                data : {id: id},
                async : true,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].kode_jenis_transaksi+'> Karyawan - ' + data[i].nama_jenis_transaksi+'</option>';            
                    }
                    $('#jenis_transaksi_peserta').html(html);
                }
            });
            return false;
        });

        $('#area_pelayanan_peserta').change(function(){ 
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url('Sika/getTPK');?>",
                method : "POST",
                data : {id: id},
                async : true,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].kode_tpk+'>'+data[i].nama_tpk+'</option>';

                    }
                    $('#tpk_peserta').html(html);
                }
            });
            return false;
        }); 

        
    }
</script>