<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-add-user icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Input Kepala Keluarga
                <div class="page-title-subheading">Penambahan data kepala keluarga baru.
                </div>
            </div>
        </div>
        <div class="page-title-actions">
        </div>    
    </div>
</div>
<?php  if ($this->session->input_fail == 1) { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>NIK sudah tersedia!</strong> Silakan masukan NIK lain.
        </div>
    </div>
</div>
<script type="text/javascript">
    window.onload = function() {
        
        

        // var nik = localStorage.getItem("nik");
        // if (nik !== null) $('#nik').val(nik);


        var nama = localStorage.getItem("nama");
        if (nama !== null) $('#nama').val(nama);

        var tempat_lahir = localStorage.getItem("tempat_lahir");
        if (tempat_lahir !== null) $('#tempat_lahir').val(tempat_lahir);

        var tgl_lahir = localStorage.getItem("tgl_lahir");
        if (tgl_lahir !== null) $('#datepicker_tgl_lahir').val(tgl_lahir);

        var jenis_kelamin = localStorage.getItem("jenis_kelamin");
        if (jenis_kelamin !== null) $('#jenis_kelamin').val(jenis_kelamin);

        var agama = localStorage.getItem("agama");
        if (agama !== null) $('#agama').val(agama);

        var no_ktp = localStorage.getItem("no_ktp");
        if (no_ktp !== null) $('#no_ktp').val(no_ktp);

        var gol_darah = localStorage.getItem("gol_darah");
        if (gol_darah !== null) $('#gol_darah').val(gol_darah);

        var rhesus = localStorage.getItem("rhesus");
        if (rhesus !== null) $('#rhesus').val(rhesus);

        var no_bpjs = localStorage.getItem("no_bpjs");
        if (no_bpjs !== null) $('#no_bpjs').val(no_bpjs);

        // var area_pelayanan = localStorage.getItem("area_pelayanan");
        // if (area_pelayanan !== null) $('#area_pelayanan').val(area_pelayanan);

        // var tpk = localStorage.getItem("tpk");
        // if (tpk !== null) $('#tpk').val(tpk);

        var status_faskes = localStorage.getItem("status_faskes");
        if (status_faskes !== null) $('#status_faskes').val(status_faskes);

        var tgl_faskes = localStorage.getItem("tgl_faskes");
        if (tgl_faskes !== null) $('#datepicker_tgl_faskes').val(tgl_faskes);

        var kelompok_peserta = localStorage.getItem("kelompok_peserta");
        if (kelompok_peserta !== null) $('#kelompok_peserta').val(kelompok_peserta);

        var tgl_capeg = localStorage.getItem("tgl_capeg");
        if (tgl_capeg !== null) $('#datepicker_tgl_capeg').val(tgl_capeg);

        var tgl_mulai_kerja = localStorage.getItem("tgl_mulai_kerja");
        if (tgl_mulai_kerja !== null) $('#datepicker_tgl_mulai_kerja').val(tgl_mulai_kerja);

        var tgl_pensiun = localStorage.getItem("tgl_pensiun");
        if (tgl_pensiun !== null) $('#datepicker_tgl_pensiun').val(tgl_pensiun);

        var tgl_berhenti_kerja = localStorage.getItem("tgl_berhenti_kerja");
        if (tgl_berhenti_kerja !== null) $('#datepicker_tgl_berhenti_kerja').val(tgl_berhenti_kerja);

        var alasan_berhenti_kerja = localStorage.getItem("alasan_berhenti_kerja");
        if (alasan_berhenti_kerja !== null) $('#alasan_berhenti_kerja').val(alasan_berhenti_kerja);

        // var pendidikan = localStorage.getItem("pendidikan");
        // if (pendidikan !== null) $('#pendidikan').val(pendidikan);

        // var instansi = localStorage.getItem("instansi");
        // if (instansi !== null) $('instansi').val(instansi);

        var jabatan = localStorage.getItem("jabatan");
        if (jabatan !== null) $('jabatan').val(jabatan);

        // var divisi = localStorage.getItem("divisi");
        // if (divisi !== null) $('divisi').val(divisi);

        // var bagian = localStorage.getItem("bagian");
        // if (bagian !== null) $('bagian').val(bagian);

        // var band_posisi = localStorage.getItem("band_posisi");
        // if (band_posisi !== null) $('band_posisi').val(band_posisi);

        // var klas_posisi = localStorage.getItem("klas_posisi");
        // if (klas_posisi !== null) $('klas_posisi').val(klas_posisi);

        var kelas_perawatan = localStorage.getItem("kelas_perawatan");
        if (kelas_perawatan !== null) $('kelas_perawatan').val(kelas_perawatan);

        // var personal_area = localStorage.getItem("personal_area");
        // if (personal_area !== null) $('personal_area').val(personal_area);

        // var personal_sub_area = localStorage.getItem("personal_sub_area");
        // if (personal_sub_area !== null) $('personal_sub_area').val(personal_sub_area);

        var alamat_ktp = localStorage.getItem("alamat_ktp");
        if (alamat_ktp !== null) $('alamat_ktp').val(alamat_ktp);

        var rt_ktp = localStorage.getItem("rt_ktp");
        if (rt_ktp !== null) $('rt_ktp').val(rt_ktp);

        var rw_ktp = localStorage.getItem("rw_ktp");
        if (rw_ktp !== null) $('rw_ktp').val(rw_ktp);

        // var provinsi_ktp = localStorage.getItem("provinsi_ktp");
        // if (provinsi_ktp !== null) $('provinsi_ktp').val(provinsi_ktp);

        // var kabupaten_ktp = localStorage.getItem("kabupaten_ktp");
        // if (kabupaten_ktp !== null) $('kabupaten_ktp').val(kabupaten_ktp);

        // var kecamatan_ktp = localStorage.getItem("kecamatan_ktp");
        // if (kecamatan_ktp !== null) $('kecamatan_ktp').val(kecamatan_ktp);

        // var kelurahan_ktp = localStorage.getItem("kelurahan_ktp");
        // if (kelurahan_ktp !== null) $('kelurahan_ktp').val(kelurahan_ktp);

        var kode_pos_ktp = localStorage.getItem("kode_pos_ktp");
        if (kode_pos_ktp !== null) $('kode_pos_ktp').val(kode_pos_ktp);

        var no_telp_ktp = localStorage.getItem("no_telp_ktp");
        if (no_telp_ktp !== null) $('no_telp_ktp').val(no_telp_ktp);

        var alamat_domisili = localStorage.getItem("alamat_domisili");
        if (alamat_domisili !== null) $('alamat_domisili').val(alamat_domisili);

        var rt_domisili = localStorage.getItem("rt_domisili");
        if (rt_domisili !== null) $('rt_domisili').val(rt_domisili);

        var rw_domisili = localStorage.getItem("rw_domisili");
        if (rw_domisili !== null) $('rw_domisili').val(rw_domisili);

        // var provinsi_domisili = localStorage.getItem("provinsi_domisili");
        // if (provinsi_domisili !== null) $('provinsi_domisili').val(provinsi_domisili);

        // var kabupaten_domisili = localStorage.getItem("kabupaten_domisili");
        // if (kabupaten_domisili !== null) $('kabupaten_domisili').val(kabupaten_domisili);

        // var kecamatan_domisili = localStorage.getItem("kecamatan_domisili");
        // if (kecamatan_domisili !== null) $('kecamatan_domisili').val(kecamatan_domisili);

        // var kelurahan_domisili = localStorage.getItem("kelurahan_domisili");
        // if (kelurahan_domisili !== null) $('kelurahan_domisili').val(kelurahan_domisili);

        var kode_pos_domisili = localStorage.getItem("kode_pos_domisili");
        if (kode_pos_domisili !== null) $('kode_pos_domisili').val(kode_pos_domisili);

        var no_telp_domisili = localStorage.getItem("no_telp_domisili");
        if (no_telp_domisili !== null) $('no_telp_domisili').val(no_telp_domisili);

        var nama_pemilik_rekening = localStorage.getItem("nama_pemilik_rekening");
        if (nama_pemilik_rekening !== null) $('nama_pemilik_rekening').val(nama_pemilik_rekening);

        var nama_bank = localStorage.getItem("nama_bank");
        if (nama_bank !== null) $('nama_bank').val(nama_bank);

        // var no_rekening_bank = localStorage.getItem("no_rekening_bank");
        // if (no_rekening_bank !== null) $('no_rekening_bank').val(no_rekening_bank);

        var status_kartu = localStorage.getItem("status_kartu");
        if (status_kartu !== null) $('status_kartu').val(status_kartu);

        var tgl_cetak = localStorage.getItem("tgl_cetak");
        if (tgl_cetak !== null) $('datepicker_tgl_cetak').val(tgl_cetak);

        var tgl_akhir_kartu = localStorage.getItem("tgl_akhir_kartu");
        if (tgl_akhir_kartu !== null) $('datepicker_tgl_akhir_kartu').val(tgl_akhir_kartu);

        var status_pernikahan = localStorage.getItem("status_pernikahan");
        if (status_pernikahan !== null) $('status_pernikahan').val(status_pernikahan);

        var tgl_nikah = localStorage.getItem("tgl_nikah");
        if (tgl_nikah !== null) $('datepicker_tgl_nikah').val(tgl_nikah);

        var tgl_cerai = localStorage.getItem("tgl_cerai");
        if (tgl_cerai !== null) $('datepicker_tgl_cerai').val(tgl_cerai);

        var tgl_meninggal = localStorage.getItem("tgl_meninggal");
        if (tgl_meninggal !== null) $('datepicker_tgl_meninggal').val(tgl_meninggal);

        var catatan = localStorage.getItem("catatan");
        if (catatan !== null) $('catatan').val(catatan);

    }
</script>

<?php $this->session->input_fail = 0; } ?>




<form action="<?php echo base_url('PesertaController/inputKKProses'); ?>" enctype="multipart/form-data"  method="POST" class="needs-validation" novalidate>
    <div class="row">
        <div class="col-md-6">
            <div class="tab-content">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-12">
                                <h5 class="card-title">Upload Foto Kepala Keluarga</h5>
                                <div class="position-relative row form-group">
                                    <img class="col-sm-3 col-form-label" name="foto_kk_preview" id="image-preview" src="<?php echo base_url() . '../assets/images/user-placeholder.jpg'; ?>">
                                </div>
                                <div class="position-relative row form-group">

                                    <div class="col-sm-10">
                                        <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                        <input type="file" name="foto_kk" id="image-source" accept=".jpg,.jpeg,.png,.bmp" onchange="previewImage();" class="form-control-file">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tab-content">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-12">
                                <h5 class="card-title">Pilih Transaksi</h5>
                                <div class="position-relative form-group">
                                    <div class="position-relative row form-group"><label for="kelompok_transaksi_kk" class="col-sm-2 col-form-label">Kelompok Transaksi</label>
                                        <div class="col-sm-8">
                                            <select name="kelompok_transaksi_kk" id="kelompok_transaksi_kk" class="form-control" required>
                                                <option value="" selected>Pilih Kelompok Transaksi</option>
                                                <?php  
                                                    foreach ($kelompok_transaksi as $value) {
                                                        if ($value->kode_kelompok_transaksi == "01") {
                                                            echo "<option value=".$value->kode_kelompok_transaksi.">".$value->nama_kelompok_transaksi."</option>";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Kelompok Transaksi tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="jenis_peserta_kk" class="col-sm-2 col-form-label">Jenis Peserta</label>
                                        <div class="col-sm-8">
                                            <select name="jenis_peserta_kk" id="jenis_peserta_kk" class="form-control" required>
                                                <option value="" hidden="" selected>Pilih Jenis Peserta</option>
                                                <?php foreach ($jenis_peserta as $value): ?>
                                                <?php if ($value->kode_group_status_keluarga == "1"): ?>
                                                <option value="<?php echo $value->kode_group_jenis_peserta ?>"><?php echo $value->nama_jenis_peserta ?></option>
                                                <?php endif ?>
                                                <?php endforeach ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Jenis Peserta tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="position-relative row form-group"><label for="jenis_transaksi_kk" class="col-sm-2 col-form-label">Jenis Transaksi</label>
                                    <div class="col-sm-8">
                                        <select name="jenis_transaksi_kk" id="jenis_transaksi_kk" class="form-control" required>
                                            <option value="" hidden="" selected>Pilih Jenis Transaksi</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Jenis Transaksi tidak boleh kosong.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-0" class="active nav-link">Data Pribadi</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-1" class="nav-link">Data Kepegawaian</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-2" class="nav-link">Info Lain-Lain</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-3" class="nav-link">Data Pendukung</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-eg11-0" role="tabpanel">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="nik" class="col-sm-2 col-form-label">NIK</label>
                                        <div class="col-sm-8">
                                            <input name="nik" id="nik" placeholder="" class="form-control" required>
                                            <div class="invalid-feedback">
                                                NIK tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="nama" class="col-sm-2 col-form-label">Nama</label>
                                        <div class="col-sm-8">
                                            <input name="nama" id="nama" placeholder="" class="form-control" required>
                                            <div class="invalid-feedback">
                                                Nama tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                        <div class="col-sm-3">
                                            <input name="tempat_lahir" id="tempat_lahir" placeholder="" class="form-control" required>
                                            <div class="invalid-feedback">
                                                Tempat Lahir tidak boleh kosong.
                                            </div>
                                        </div>
                                        <label for="tgl_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                        <div class="col-sm-3">

                                            <input name="tgl_lahir" autocomplete="off" id="datepicker_tgl_lahir" placeholder="dd/mm/yyyy" class="form-control" required>

                                            <div class="invalid-feedback">
                                                Tanggal Lahir tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                        <div class="col-sm-2">
                                            <div class="position-relative form-check">
                                                <label class="form-check-label"><input name="jenis_kelamin" id="jenis_kelamin" value="Laki-laki" type="radio" class="form-check-input" checked>Laki-laki</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="position-relative form-check">
                                                <label class="form-check-label"><input name="jenis_kelamin" id="jenis_kelamin" value="Perempuan" type="radio" class="form-check-input">Perempuan</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="agama" class="col-sm-2 col-form-label">Agama</label>
                                        <div class="col-sm-8">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="" hidden="">Pilih Agama</option>
                                            <?php foreach ($agama as $value): ?>
                                                <option value="<?php echo $value->kode_agama; ?>"><?php echo $value->nama_agama; ?></option>
                                            <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="gol_darah" class="col-sm-2 col-form-label">Golongan Darah</label>
                                        <div class="col-sm-3">
                                            <select name="gol_darah" id="gol_darah" class="form-control">
                                                <option value="" hidden="" selected></option>
                                                <option value="A">A</option>
                                                <option value="A">B</option>
                                                <option value="A">AB</option>
                                                <option value="A">O</option>
                                            </select>
                                        </div>
                                        <label for="gol_darah" class="col-sm-2 col-form-label">Rhesus</label>
                                        <div class="col-sm-3">
                                            <select name="rhesus" id="rhesus" class="form-control">
                                                <option value="" hidden="" selected></option>
                                                <option value=1>+</option>
                                                <option value=0>-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">Nomor KTP</label>
                                        <div class="col-sm-8">
                                            <input name="no_ktp" id="no_ktp" placeholder="" class="form-control">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    
                                    <div class="position-relative row form-group"><label for="no_bpjs" class="col-sm-2 col-form-label">Nomor BPJS</label>
                                        <div class="col-sm-8">
                                            <input name="no_bpjs" id="no_bpjs" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="area_pelayanan" class="col-sm-2 col-form-label">Area Pelayanan</label>
                                        <div class="col-sm-8">
                                            <select name="area_pelayanan" id="area_pelayanan" class="form-control" required> 
                                                <option value="" hidden="" selected>Pilih area</option>
                                                <?php  
                                                    foreach ($area as $value) {
                                                        if ($value->kode_area != 0) {
                                                            # code...
                                                            echo "<option value=".$value->kode_area.">".$value->nama_area."</option>";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Area Pelayanan tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tpk" class="col-sm-2 col-form-label">TPK</label>
                                        <div class="col-sm-8">
                                            <select name="tpk" id="tpk" class="form-control" required>
                                                <option value="" hidden="" selected>Pilih TPK</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                TPK tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">Kota TPK</label>
                                        <div class="col-sm-8">
                                            <select name="kota_tpk" id="kota_tpk" disabled="" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">Alamat TPK</label>
                                        <div class="col-sm-8">
                                            <select name="alamat_tpk" id="alamat_tpk" disabled="" placeholder="" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="status_faskes" class="col-sm-2 col-form-label">Status Faskes</label>
                                        <div class="col-sm-2">
                                            <div class="position-relative form-check">
                                                <label class="form-check-label"><input name="status_faskes" id="status_faskes" value=1 type="radio" class="form-check-input" checked>Ya</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="position-relative form-check">
                                                <label class="form-check-label"><input name="status_faskes" id="status_faskes" value=0 type="radio" class="form-check-input">Tidak</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tgl_faskes" class="col-sm-2 col-form-label">Tanggal Faskes</label>
                                        <div class="col-sm-8">
                                            <input name="tgl_faskes" autocomplete="off" id="datepicker_tgl_faskes" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                    <div class="tab-pane" id="tab-eg11-1" role="tabpanel">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="kelompok_peserta" class="col-sm-2 col-form-label">Kelompok Peserta</label>
                                        <div class="col-sm-8">
                                            <select name="kelompok_peserta" id="kelompok_peserta" class="form-control" required>
                                                <option value="" hidden="" selected>Pilih kelompok peserta</option>
                                                <?php  
                                                    foreach ($kelompok_peserta as $value) {
                                                        echo "<option value='".$value->kode_kelompok_peserta."'>".$value->nama_kelompok_peserta."</option>";
                                                    }
                                                ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Kelompok Peserta tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tgl_capeg" class="col-sm-2 col-form-label">Tanggal Capeg</label>
                                        <div class="col-sm-3">
                                            <input name="tgl_capeg" autocomplete="off" id="datepicker_tgl_capeg" placeholder="dd/mm/yyyy" class="form-control" required>
                                            <div class="invalid-feedback">
                                                Tanggal Capeg tidak boleh kosong.
                                            </div>
                                        </div>
                                        <label for="tgl_mulai_kerja" class="col-sm-2 col-form-label">Tanggal Mulai Kerja</label>
                                        <div class="col-sm-3">
                                            <input name="tgl_mulai_kerja" autocomplete="off" id="datepicker_tgl_mulai_kerja" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tgl_pensiun" class="col-sm-2 col-form-label">Tanggal Pensiun</label>
                                        <div class="col-sm-3">
                                            <input name="tgl_pensiun" autocomplete="off" id="datepicker_tgl_pensiun" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                        <label for="tgl_berhenti_kerja" class="col-sm-2 col-form-label">Tanggal Berhenti Kerja</label>
                                        <div class="col-sm-3">
                                            <input name="tgl_berhenti_kerja" autocomplete="off" id="datepicker_tgl_berhenti_kerja" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="alasan_berhenti_kerja" class="col-sm-2 col-form-label">Alasan Berhenti Kerja</label>
                                        <div class="col-sm-8">
                                            <input name="alasan_berhenti_kerja" id="alasan_berhenti_kerja" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="pendidikan" class="col-sm-2 col-form-label">Pendidikan</label>
                                        <div class="col-sm-8">
                                            <select name="pendidikan" id="pendidikan" class="form-control" required>
                                                <option value="" hidden="" selected>Pilih pendidikan</option>
                                                <?php  
                                                    foreach ($pendidikan as $value) {
                                                        echo "<option value=".$value->kode_pendidikan.">".$value->nama_pendidikan."</option>";
                                                    }
                                                ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Pendidikan tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="instansi" class="col-sm-2 col-form-label">Instansi</label>
                                        <div class="col-sm-8">
                                            <select name="instansi" id="instansi" class="form-control" required>
                                            </select>
                                            <div class="invalid-feedback">
                                                Instansi tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="jabatan" class="col-sm-2 col-form-label">Jabatan</label>
                                        <div class="col-sm-8">
                                            <input name="jabatan" id="jabatan" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="divisi" class="col-sm-2 col-form-label">Divisi / Loker</label>
                                        <div class="col-sm-3">
                                            <select name="divisi" id="divisi" class="form-control" required>
                                            </select>
                                            <div class="invalid-feedback">
                                                Divisi tidak boleh kosong.
                                            </div>
                                        </div>
                                        <label for="bagian" class="col-sm-2 col-form-label">Bagian</label>
                                        <div class="col-sm-3">
                                            <select name="bagian" id="bagian" class="form-control" required>
                                                <option value="" hidden="" selected></option>
                                                <?php  
                                                    foreach ($bagian as $value) {
                                                        echo "<option value='".$value->kode_bagian."'>".$value->nama_bagian."</option>";
                                                    }
                                                ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Bagian tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="band_posisi" class="col-sm-2 col-form-label">Band Posisi</label>
                                        <div class="col-sm-3">
                                            <select name="band_posisi" id="band_posisi" class="form-control" required>
                                                <option value="" hidden="" selected></option>
                                                <option value="I">I</option>
                                                <option value="II">II</option>
                                                <option value="III">III</option>
                                                <option value="IV">IV</option>
                                                <option value="V">V</option>
                                                <option value="VI">VI</option>
                                                <option value="VII">VII</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                Band Posisi tidak boleh kosong.
                                            </div>
                                        </div>
                                        <label for="klas_posisi" class="col-sm-2 col-form-label">Klas Posisi</label>
                                        <div class="col-sm-3">
                                            <select name="klas_posisi" id="klas_posisi" class="form-control" required>
                                            </select>
                                            <div class="invalid-feedback">
                                                Klas Posisi tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="grade" class="col-sm-2 col-form-label">Grade</label>
                                        <div class="col-sm-3">
                                            <select name="grade" disabled="" id="grade" class="form-control">
                                                <option value="" hidden="" selected></option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <label for="kelas_perawatan" class="col-sm-2 col-form-label">Kelas Perawatan</label>
                                        <div class="col-sm-3">
                                            <select name="kelas_perawatan" id="kelas_perawatan" class="form-control" required>
                                                <option value="" hidden="" selected></option>
                                                <option value="VVIP">VVIP</option>
                                                <option value="VIP">VIP</option>
                                                <option value="UTAMA">UTAMA</option>
                                                <option value="I">I</option>
                                                <option value="II">II</option>
                                                <option value="III">III</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                Kelas Perawatan tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="personal_area" class="col-sm-2 col-form-label">Personal Area</label>
                                        <div class="col-sm-3">
                                            <select name="personal_area" id="personal_area" class="form-control" required>
                                                <option value="" hidden="" selected></option>
                                                <?php  
                                                    foreach ($personal_area as $value) {
                                                        echo "<option value='".$value->kode_personal_area."'>".$value->nama_personal_area."</option>";
                                                    }
                                                ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Personal Area tidak boleh kosong.
                                            </div>
                                        </div>
                                        <label for="psa" class="col-sm-2 col-form-label">Personal Sub Area</label>
                                        <div class="col-sm-3">
                                            <select name="personal_sub_area" id="personal_sub_area" class="form-control" required>
                                            </select>
                                            <div class="invalid-feedback">
                                                Personal Sub Area tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="tab-pane" id="tab-eg11-2" role="tabpanel">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="alamat_ktp" class="col-sm-2 col-form-label">Alamat KTP</label>
                                        <div class="col-sm-8">
                                            <textarea name="alamat_ktp" id="alamat_ktp" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="rt_ktp" class="col-sm-2 col-form-label">RT</label>
                                        <div class="col-sm-3">
                                            <input name="rt_ktp" id="rt_ktp" placeholder="" class="form-control">
                                        </div>
                                        <label for="rw_ktp" class="col-sm-2 col-form-label">RW</label>
                                        <div class="col-sm-3">
                                            <input name="rw_ktp" id="rw_ktp" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="provinsi_ktp" class="col-sm-2 col-form-label">Provinsi</label>
                                        <div class="col-sm-3">
                                            <select name="provinsi_ktp" id="provinsi_ktp" class="form-control">
                                                <option value="" hidden="" selected></option>
                                                <?php  
                                                    foreach ($provinsi as $value) {
                                                        echo "<option value=".$value->id.">".$value->name."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <label for="kabupaten_ktp" class="col-sm-2 col-form-label">Kota</label>
                                        <div class="col-sm-3">
                                            <select name="kabupaten_ktp" id="kabupaten_ktp" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="kecamatan_ktp" class="col-sm-2 col-form-label">Kecamatan</label>
                                        <div class="col-sm-3">
                                            <select name="kecamatan_ktp" id="kecamatan_ktp" class="form-control">
                                            </select>
                                        </div>
                                        <label for="kelurahan_ktp" class="col-sm-2 col-form-label">Kelurahan</label>
                                        <div class="col-sm-3">
                                            <select name="kelurahan_ktp" id="kelurahan_ktp" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="kode_pos_ktp" class="col-sm-2 col-form-label">Kode POS</label>
                                        <div class="col-sm-2">
                                            <input name="kode_pos_ktp" id="kode_pos_ktp" placeholder="" class="form-control">
                                        </div>
                                        <label for="no_telp_ktp" class="col-sm-2 col-form-label">Nomor Telepon</label>
                                        <div class="col-sm-4">
                                            <input name="no_telp_ktp" id="no_telp_ktp" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="alamat_domisili" class="col-sm-2 col-form-label">Alamat Domisili</label>
                                        <div class="col-sm-8">
                                            <textarea name="alamat_domisili" id="alamat_domisili" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="rt_domisili" class="col-sm-2 col-form-label">RT</label>
                                        <div class="col-sm-3">
                                            <input name="rt_domisili" id="rt_domisili" placeholder="" class="form-control">
                                        </div>
                                        <label for="rw_domisili" class="col-sm-2 col-form-label">RW</label>
                                        <div class="col-sm-3">
                                            <input name="rw_domisili" id="rw_domisili" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="provinsi_domisili" class="col-sm-2 col-form-label">Provinsi</label>
                                        <div class="col-sm-3">
                                            <select name="provinsi_domisili" id="provinsi_domisili" class="form-control">
                                                <option value="" hidden="" selected></option>
                                                <?php  
                                                    foreach ($provinsi as $value) {
                                                        echo "<option value='".$value->id."'>".$value->name."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <label for="kabupaten_domisili" class="col-sm-2 col-form-label">Kota</label>
                                        <div class="col-sm-3">
                                            <select name="kabupaten_domisili" id="kabupaten_domisili" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="kecamatan_domisili" class="col-sm-2 col-form-label">Kecamatan</label>
                                        <div class="col-sm-3">
                                            <select name="kecamatan_domisili" id="kecamatan_domisili" class="form-control">
                                            </select>
                                        </div>
                                        <label for="kelurahan_domisili" class="col-sm-2 col-form-label">Kelurahan</label>
                                        <div class="col-sm-3">
                                            <select name="kelurahan_domisili" id="kelurahan_domisili" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="kode_pos_domisili" class="col-sm-2 col-form-label">Kode POS</label>
                                        <div class="col-sm-2">
                                            <input name="kode_pos_domisili" id="kode_pos_domisili" placeholder="" class="form-control">
                                        </div>
                                        <label for="no_telp_domisili" class="col-sm-2 col-form-label">Nomor Telepon</label>
                                        <div class="col-sm-4">
                                            <input name="no_telp_domisili" id="no_telp_domisili" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" align="center">
                                    <div class="col-sm-10">
                                        <div class="position-relative form-check"><label class="form-check-label">
                                            <input name="ktp_domisili" id="ktp_domisili" type="checkbox" onclick="FillBilling(this.form)" class="form-check-input"> Alamat Domisili sama dengan Alamat KTP</label>
                                        </div>
                                    </div>
                                    <div class="divider"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="no_hp" class="col-sm-2 col-form-label">Nomor HP</label>
                                        <div class="col-sm-8">
                                            <input name="no_hp" id="no_hp" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="nama_pemilik_rekening" class="col-sm-2 col-form-label">Nama Pemilik Rekening</label>
                                        <div class="col-sm-8">
                                            <input name="nama_pemilik_rekening" id="nama_pemilik_rekening" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="nama_bank" class="col-sm-2 col-form-label">Nama Bank</label>
                                        <div class="col-sm-8">
                                            <select name="nama_bank" id="nama_bank" class="form-control">
                                                <option value="" hidden="" selected>Pilih bank</option>
                                                <?php  
                                                    foreach ($bank as $value) {
                                                        echo "<option value=".$value->kode_bank.">".$value->nama_bank."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="no_rekening_bank" class="col-sm-2 col-form-label">Nomor Rekening Bank</label>
                                        <div class="col-sm-4">
                                            <input name="no_rekening_bank" id="no_rekening_bank" placeholder="" class="form-control">
                                        </div>
                                        <label for="status_kartu" class="col-sm-1.5 col-form-label">Status Kartu</label>
                                        <div class="col-sm-3">
                                            <select name="status_kartu" id="status_kartu" class="form-control">
                                                <option value="" hidden="" selected>Pilih status</option>
                                                <option value="Belum Cetak">Belum Cetak</option>
                                                <option value="Expired">Expired</option>
                                                <option value="Sudah Cetak">Sudah Cetak</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tgl_cetak" class="col-sm-2 col-form-label">Tanggal Cetak</label>
                                        <div class="col-sm-3">
                                            <input name="tgl_cetak" autocomplete="off" id="datepicker_tgl_cetak" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                        <label for="tgl_akhir_kartu" class="col-sm-2 col-form-label">Tanggal Akhir Kartu</label>
                                        <div class="col-sm-3">
                                            <input name="tgl_akhir_kartu" autocomplete="off" id="datepicker_tgl_akhir_kartu" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="status_pernikahan" class="col-sm-2 col-form-label">Status Pernikahan</label>
                                        <div class="col-sm-3">
                                            <select name="status_pernikahan" id="status_pernikahan" class="form-control">
                                                <option value="" hidden="" selected>Pilih status</option>
                                                <option value="Singe">Single</option>
                                                <option value="Menikah">Menikah</option>
                                                <option value="Janda">Janda</option>
                                                <option value="Duda">Duda</option>
                                            </select>
                                        </div>
                                        <label for="tgl_nikah" class="col-sm-2 col-form-label">Tanggal Pernikahan</label>
                                        <div class="col-sm-3">
                                            <input name="tgl_nikah" autocomplete="off" id="datepicker_tgl_nikah" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tgl_cerai" class="col-sm-2 col-form-label">Tanggal Perceraian</label>
                                        <div class="col-sm-8">
                                            <input name="tgl_cerai" autocomplete="off" id="datepicker_tgl_cerai" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tgl_meninggal" class="col-sm-2 col-form-label">Tanggal Meninggal</label>
                                        <div class="col-sm-8">
                                            <input name="tgl_meninggal" autocomplete="off" id="datepicker_tgl_meninggal" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="catatan" class="col-sm-2 col-form-label">Catatan</label>
                                        <div class="col-sm-8">
                                            <textarea name="catatan" id="catatan" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    
                    
                    
                    <div class="tab-pane" id="tab-eg11-3" role="tabpanel">
                            <h5 class="card-title">Upload Dokumen Pendukung</h5>
                            <div class="form-row">
                                <table class="mb-0 table table-borderless">
                                    <thead>
                                    <tr>
                                        <th>Nomor Dokumen</th>
                                        <th>Perihal</th>
                                        <th>Tanggal Dokumen</th>
                                        <th>File</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="no_dokumen_1" id="no_dokumen_1" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="perihal_1" id="perihal_1" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="tgl_dokumen_1" autocomplete="off" id="datepicker_tgl_dokumen_1" placeholder="dd/mm/yyyy" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>

                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                                    <input type="file" name="file_1" id="file_1" accept=".pdf" class="form-control-file">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="no_dokumen_2" id="no_dokumen_2" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="perihal_2" id="perihal_2" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="tgl_dokumen_2" autocomplete="off" id="datepicker_tgl_dokumen_2" placeholder="dd/mm/yyyy" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>

                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                                    <input type="file" name="file_2" id="file_2" accept=".pdf" class="form-control-file">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="no_dokumen_3" id="no_dokumen_3" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="perihal_3" id="perihal_3" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="tgl_dokumen_3" autocomplete="off" id="datepicker_tgl_dokumen_3" placeholder="dd/mm/yyyy" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>

                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                                    <input type="file" name="file_3" id="file_3" accept=".pdf" class="form-control-file">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="col-md-12" align="center">
                    <button class="mt-1 btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>