<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-edit icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Edit Peserta
                <div class="page-title-subheading">Cari peserta untuk melakukan penyuntingan data peserta.
                </div>
            </div>
        </div>
        <div class="page-title-actions">
        </div>    
    </div>
</div>
<div class="tab-content">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <form action="<?php echo base_url('../edit_peserta_filter'); ?>" method="POST">
                <div class="position-relative row form-group"><label for="jenis_kepesertaan" class="col-sm-2 col-form-label">Jenis Kepesertaan</label>
                    <div class="col-sm-6">
                        <select name="select" id="jenis_kepesertaan" class="form-control" >
                            <option hidden="">Pilih Jenis Peserta</option>
                            <?php foreach ($jenis_peserta as $value) {
                                echo "<option value=".$value->kode_jenis_peserta.">".$value->nama_jenis_peserta."</option>";
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nik" class="col-sm-2 col-form-label">NIK</label>
                    <div class="col-sm-6"><input name="nik" id="nik" placeholder="" class="form-control"></div>
                </div>
                <div class="position-relative row form-group"><label for="no_peserta" class="col-sm-2 col-form-label">Nomor Peserta</label>
                    <div class="col-sm-6"><input name="no_peserta" id="no_peserta" placeholder="" class="form-control"></div>
                </div>
                <div class="position-relative row form-group"><label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-6"><input name="nama" id="nama" placeholder="" class="form-control"></div>
                </div>
                <div class="position-relative row form-group"><label for="nikep" class="col-sm-2 col-form-label">NIKEP (Nomor KTP)</label>
                    <div class="col-sm-6"><input name="nikep" id="nikep" placeholder="" class="form-control"></div>
                </div>
                <div class="position-relative row form-group"><label for="no_bpjs" class="col-sm-2 col-form-label">Nomor BPJS</label>
                    <div class="col-sm-6"><input name="no_bpjs" id="no_bpjs" placeholder="" class="form-control"></div>
                </div>
                <div class="position-relative row form-group"><label for="area_pelayanan_cari_peserta" class="col-sm-2 col-form-label">Area Pelayanan</label>
                    <div class="col-sm-6">
                        <select name="area_pelayanan_cari_peserta" id="area_pelayanan_cari_peserta" class="form-control">
                            <option hidden="">Pilih Area Pelayanan</option>
                            <?php foreach ($area_pelayanan as $value) {
                                echo "<option value=".$value->kode_area.">".$value->nama_area."</option>";
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="tpk_cari_peserta" class="col-sm-2 col-form-label">TPK</label>
                    <div class="col-sm-6">
                        <select name="tpk_cari_peserta" id="tpk_cari_peserta" class="form-control">
                            <option hidden="">Pilih TPK</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-primary">Cari Peserta</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- <div class="tab-content">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <table id="cari_peserta_tabel" class="mb-0 table table-striped">
                        <thead>
                            <tr>
                                <th>NIKES</th>
                                <th>Nama</th>
                                <th>Area</th>
                                <th>Jenis Peserta</th>
                                <th>Nomor KTP</th>
                                <th>Nomor BPJS</th>
                                <th>BAND</th>
                                <th>Status Faskes</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
 -->



