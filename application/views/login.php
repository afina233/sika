<!DOCTYPE html>
<html lang="en">
<head>
	<title>SIKEP | Login </title>
    <link rel="icon" type="image/gif" href="<?php echo base_url('../assets/images/icon.png') ?>" />
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css'; ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css'; ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/vendor/animate/animate.css'; ?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/vendor/css-hamburgers/hamburgers.min.css'; ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/vendor/animsition/css/animsition.min.css'; ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/vendor/select2/select2.min.css'; ?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/vendor/daterangepicker/daterangepicker.css'; ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/css/util.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/css/main.css'; ?>">
<!--===============================================================================================-->
    <link href="<?php echo base_url().'../main.css'; ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url().'../assets/scripts/main.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'../assets/scripts/jquery-3.3.1.js'; ?>"></script>

</head>

<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
				<form class="login100-form validate-form flex-sb flex-w needs-validation" action="<?php echo base_url('AdminController/loginAdmin'); ?>" method="POST">
					<span class="login100-form-title p-b-32" style="text-align: center;">
						SIKEP YAKES TELKOM
						<p>Sistem Informasi Kepesertaan</p>
					</span>				
					<div class="form-row">
						<?php if ($this->session->gagal_login == 1){ ?>
							
						<div class="col-md-12">
					        <div class="alert alert-danger alert-dismissible">
					            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					            <strong>Username / password tidak sesuai!</strong>
					        </div>
					    </div>  
						<?php } $this->session->gagal_login = 0; ?>
						
						<h5 class="card-title">Username</h5>
						<div class="col-md-12">
							<input type="text" name="username" class="form-control">
							<br>
						</div>
						<h5 class="card-title">Password</h5>
						<div class="col-md-12">
							<span class="btn-show-pass">
								<i class="fa fa-eye"></i>
							</span>
							<input type="password" name="password" class="form-control">
						</div>
						
					</div>
					<!-- <div class="wrap-input100 validate-input m-b-36" data-validate = "Username harus diisi">
						<input class="input100" type="text" name="username" >
					</div> -->
					
					<!-- <div class="wrap-input100 validate-input m-b-12" data-validate = "Password harus diisi">
						<span class="btn-show-pass">
							<i class="fa fa-eye"></i>
						</span>
						<input class="input100" type="password" name="password" >
					</div> -->
					
					<div class="flex-sb-m w-full p-b-48">
						<!-- <div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>

						<div>
							<a href="#" class="txt3">
								Forgot Password?
							</a>
						</div> -->
					</div>

					<!-- <div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Masuk
						</button>
					</div> -->
					<div class="col-md-12" align="center">
                	    <button class="mb-2 mr-2 btn btn-primary btn-lg btn-block">Masuk</button>
                	</div>

				</form>
			</div>
		</div>
	</div>
	
	
<!--===============================================================================================-->
	<!-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/vendor/jquery/jquery-3.2.1.min.js'; ?>"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="<?php echo base_url().'../assets/login/vendor/animsition/js/animsition.min.js'; ?>"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url().'../assets/login/vendor/bootstrap/js/popper.js'; ?>"></script>
	<!-- <script src="vendor/bootstrap/js/bootstrap.min.js"></script> -->
<!--===============================================================================================-->
	<!-- <script src="vendor/select2/select2.min.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/vendor/select2/select2.min.js'; ?>"></script>
<!--===============================================================================================-->
	<!-- <script src="vendor/daterangepicker/moment.min.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/vendor/daterangepicker/moment.min.js'; ?>"></script>
	<!-- <script src="vendor/daterangepicker/daterangepicker.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/vendor/daterangepicker/daterangepicker.js'; ?>"></script>
<!--===============================================================================================-->
	<!-- <script src="vendor/countdowntime/countdowntime.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/vendor/countdowntime/countdowntime.js'; ?>"></script>
<!--===============================================================================================-->
	<!-- <script src="js/main.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/js/main.js'; ?>"></script>

</body>
</html>