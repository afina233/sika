<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-edit icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Edit Peserta
                <div class="page-title-subheading">Cari peserta untuk melakukan penyuntingan data peserta.
                </div>
            </div>
        </div>
        <div class="page-title-actions">
        </div>    
    </div>
</div>

<?php if ($this->session->cari_simpan == 1){ ?>
<script type="text/javascript">
    window.onload = function() {
        var nik = localStorage.getItem("nik");
        if (nik !== null) $('#nik').val(nik);

        var no_peserta = localStorage.getItem("no_peserta");
        if (no_peserta !== null) $('#no_peserta').val(no_peserta);

        var nama = localStorage.getItem("nama");
        if (nama !== null) $('#nama').val(nama);

        var nikep = localStorage.getItem("nikep");
        if (nikep !== null) $('#nikep').val(nikep);

        var no_bpjs = localStorage.getItem("no_bpjs");
        if (no_bpjs !== null) $('#no_bpjs').val(no_bpjs);

        // doubt
        var jenis_kepesertaan = localStorage.getItem("jenis_kepesertaan");
        if (jenis_kepesertaan !== null) $('#jenis_kepesertaan').val(jenis_kepesertaan);

        var area_pelayanan_cari_peserta = localStorage.getItem("area_pelayanan_cari_peserta");
        if (area_pelayanan_cari_peserta !== null) $('#area_pelayanan_cari_peserta').val(area_pelayanan_cari_peserta);

        // var tpk_cari_peserta = localStorage.getItem("tpk_cari_peserta");
        // if (tpk_cari_peserta !== null) $('#tpk_cari_peserta').val(tpk_cari_peserta);

    }
</script>
<?php $this->session->cari_simpan == 0; } ?>

<?php if ($this->session->hapus_keluarga) { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Keluarga dengan NIK <?php echo $this->session->hapus_keluarga ?> berhasil dihapus!</strong>
        </div>
    </div>  
</div>
<?php } $this->session->hapus_keluarga = 0; ?>


<div class="tab-content">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <!-- <form action="<?php echo base_url('../edit_peserta'); ?>" method="POST" name="Form" onsubmit="return validateForm()"> -->
            <form action="<?php echo base_url('PesertaController/editPeserta'); ?>" method="POST" name="Form">
                <div class="position-relative row form-group"><label for="jenis_kepesertaan" class="col-sm-2 col-form-label">Jenis Kepesertaan</label>
                    <div class="col-sm-6">
                        <select name="jenis_kepesertaan" id="jenis_kepesertaan" class="form-control" >
                            <option value="">Pilih Jenis Peserta</option>
                            <?php foreach ($jenis_peserta as $value) {
                                echo "<option value=".$value->kode_jenis_peserta.">".$value->nama_jenis_peserta."</option>";
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nik" class="col-sm-2 col-form-label">NIK</label>
                    <div class="col-sm-6"><input name="nik" id="nik" placeholder="" class="form-control"></div>
                </div>
                <div class="position-relative row form-group"><label for="no_peserta" class="col-sm-2 col-form-label">Nomor Peserta</label>
                    <div class="col-sm-6"><input name="no_peserta" id="no_peserta" placeholder="" class="form-control"></div>
                </div>
                <div class="position-relative row form-group"><label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-6"><input name="nama" id="nama" placeholder="" class="form-control"></div>
                </div>
                <div class="position-relative row form-group"><label for="nikep" class="col-sm-2 col-form-label">NIKEP (Nomor KTP)</label>
                    <div class="col-sm-6"><input name="nikep" id="nikep" placeholder="" class="form-control"></div>
                </div>
                <div class="position-relative row form-group"><label for="no_bpjs" class="col-sm-2 col-form-label">Nomor BPJS</label>
                    <div class="col-sm-6"><input name="no_bpjs" id="no_bpjs" placeholder="" class="form-control"></div>
                </div>
                <div class="position-relative row form-group"><label for="area_pelayanan_cari_peserta" class="col-sm-2 col-form-label">Area Pelayanan</label>
                    <div class="col-sm-6">
                        <select name="area_pelayanan_cari_peserta" id="area_pelayanan_cari_peserta" class="form-control">
                            <option value="">Pilih Area Pelayanan</option>
                            <?php foreach ($area_pelayanan as $value) {
                                echo "<option value=".$value->kode_area.">".$value->nama_area."</option>";
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="tpk_cari_peserta" class="col-sm-2 col-form-label">TPK</label>
                    <div class="col-sm-6">
                        <select name="tpk_cari_peserta" id="tpk_cari_peserta" class="form-control">
                            <option hidden="" value="">Pilih TPK</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-primary" id="checkBtn">Cari Peserta</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="tab-content">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <table id="cari_peserta_tabel" class="mb-0 table table-striped">
                        <thead>
                            <tr>
                                <th>NIKES</th>
                                <th>Nama</th>
                                <th>Area</th>
                                <th>Jenis Peserta</th>
                                <th>Nomor KTP</th>
                                <th>Nomor BPJS</th>
                                <th>BAND</th>
                                <th>Status Faskes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($this->session->cari == 1){ ?>
                            <?php foreach ($peserta as $value) { ?>
                                
                            <tr>
                                <td>  
                                <?php foreach ($jenis_peserta as $value2) {
                                    if ($value->kode_jenis_peserta == $value2->kode_jenis_peserta) {
                                        if ($value2->kode_group_status_keluarga == 1) { ?>
                                        <a href="<?php echo base_url('PesertaController/pesertaKK/'.$value->kode_peserta) ?>"><?php echo $value->nikes ?></a>

                                        <?php
                                        }elseif ($value2->kode_group_status_keluarga == 2) { ?>
                                        <a href="<?php echo base_url('PesertaController/pesertaPasangan/'.$value->kode_peserta) ?>"><?php echo $value->nikes ?></a>

                                        <?php
                                        }elseif ($value2->kode_group_status_keluarga == 3) { ?>
                                        <a href="<?php echo base_url('PesertaController/pesertaAnak/'.$value->kode_peserta) ?>"><?php echo $value->nikes ?></a>

                                        <?php
                                        }
                                    }
                                } ?>
                                </td>
                                <td>
                                    <?php foreach ($jenis_peserta as $value2) {
                                    if ($value->kode_jenis_peserta == $value2->kode_jenis_peserta) {
                                        if ($value2->kode_group_status_keluarga == 1) { ?>
                                        <a href="<?php echo base_url('PesertaController/pesertaKK/'.$value->kode_peserta) ?>"><?php echo $value->nama ?></a>

                                        <?php
                                        }elseif ($value2->kode_group_status_keluarga == 2) { ?>
                                        <a href="<?php echo base_url('PesertaController/pesertaPasangan/'.$value->kode_peserta) ?>"><?php echo $value->nama ?></a>

                                        <?php
                                        }elseif ($value2->kode_group_status_keluarga == 3) { ?>
                                        <a href="<?php echo base_url('PesertaController/pesertaAnak/'.$value->kode_peserta) ?>"><?php echo $value->nama ?></a>

                                        <?php
                                        }
                                    }
                                } ?>
                                </td>
                                <td>
                                    <?php foreach ($area as $value2) {
                                        if ($value2->kode_area == $value->kode_area) {
                                            echo $value2->nama_area;
                                        }
                                    } ?>
                                </td>
                                <td>
                                    <?php foreach ($jenis_peserta as $value2) {
                                        if ($value2->kode_jenis_peserta == $value->kode_jenis_peserta) {
                                            echo $value2->nama_jenis_peserta;
                                        }
                                    } ?>
                                </td>
                                <td><?php echo $value->no_ktp ?></td>
                                <td><?php echo $value->no_bpjs ?></td>
                                <td>
                                    <?php foreach ($band_posisi as $value2) {
                                        if ($value2->kode_band_posisi == $value->kode_band_posisi) {
                                            echo $value2->nama_band;
                                        }
                                    } ?>
                                </td>
                                <td>
                                    <?php if ($value->status_faskes == 1) {
                                        echo "Ya";
                                    }else{
                                        echo "Tidak";
                                    } ?>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php } $this->session->cari = 0; ?>
                                
                        </tbody>
                    </table>
                </div>
            </div>
</div>



