<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-photo-gallery icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Rekapitulasi Transaksi
                <div class="page-title-subheading">Gunakan filter untuk memperoleh data transaksi.
                </div>
            </div>
        </div>
        <div class="page-title-actions">
        </div>    
    </div>
</div>
<?php if ($this->session->cari_rekapitulasi_transaksi == 1){ ?>
<script type="text/javascript">
    window.onload = function() {

        var kelompok_transaksi = localStorage.getItem("kelompok_transaksi");
        if (kelompok_transaksi !== null) $('#kelompok_transaksi').val(kelompok_transaksi);

        var group_jenis_peserta = localStorage.getItem("group_jenis_peserta");
        if (group_jenis_peserta !== null) $('#group_jenis_peserta').val(group_jenis_peserta);

        var jenis_transaksi_rekap = localStorage.getItem("jenis_transaksi_rekap");
        if (jenis_transaksi_rekap !== null) $('#jenis_transaksi_rekap').val(jenis_transaksi_rekap);

        
        var periode_1 = localStorage.getItem("periode_1");
        if (periode_1 !== null) $('#periode_1').val(periode_1);

        var periode_2 = localStorage.getItem("periode_2");
        if (periode_2 !== null) $('#periode_2').val(periode_2);
    }
</script>
<?php } ?>
<form action="<?php echo base_url('TransaksiController/rekapitulasiTransaksi') ?>" method="POST">
    <div class="tab-content">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative row form-group"><label for="kelompok_transaksi" class="col-sm-2 col-form-label">Kelompok Transaksi</label>
                            <div class="col-sm-8">
                                <select name="kelompok_transaksi" id="kelompok_transaksi" class="form-control">
                                    <option value="" hidden="">Pilih Kelompok Transaksi</option>
                                    <?php foreach ($kelompok_transaksi as $value): ?>
                                    <option value="<?php echo $value->kode_kelompok_transaksi ?>"><?php echo $value->nama_kelompok_transaksi ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="position-relative row form-group"><label for="group_jenis_peserta" class="col-sm-2 col-form-label">Group Jenis Peserta</label>
                            <div class="col-sm-8">
                                <select name="group_jenis_peserta" id="group_jenis_peserta" class="form-control">
                                    <option value="" hidden="">Pilih Group Jenis Peserta</option>
                                    <?php foreach ($group_jenis_peserta as $value): ?>
                                    <option value="<?php echo $value->kode_group_jenis_peserta ?>"><?php echo $value->nama_group_jenis_peserta ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        
                        
                    </div>
                    <div class="col-md-6">
                        
                        <div class="position-relative row form-group"><label for="jenis_transaksi_rekap" class="col-sm-2 col-form-label">Jenis Transaksi</label>
                            <div class="col-sm-8">
                                <select name="jenis_transaksi_rekap" id="jenis_transaksi_rekap" class="form-control">
                                    <option value="" hidden="">Pilih Jenis Transaksi</option>
                                </select>
                            </div>
                        </div>
                        <div class="position-relative row form-group"><label for="periode_1" class="col-sm-2 col-form-label">Periode</label>
                            <div class="col-sm-3">
                                <input name="periode_1" autocomplete="off" placeholder="dd/mm/yyyy" id="datepicker_periode_1" placeholder="" class="form-control">
                            </div>
                            <label for="periode_2" class="col-sm-2 col-form-label">Sampai</label>
                            <div class="col-sm-3">
                                <input name="periode_2" autocomplete="off" placeholder="dd/mm/yyyy" id="datepicker_periode_2" placeholder="" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="col-md-12" align="center">
                    <button class="mt-1 btn btn-primary">Cari Data Transaksi</button>
                </div>
            </div>
        </div>
    </div>
    
</form>
<div class="tab-content">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <table id="rekapitulasi_transaksi_tabel" class="mb-0 table table-striped">
                <thead>
                <tr>
                    <th>Waktu Transaksi</th>
                    <th>Kelompok Transaksi</th>
                    <th>Jenis Transaksi</th>
                    <th>NIK</th>
                    <th>NIKES</th>
                    <th>Admin</th>
                
                </tr>
                </thead>
                <tbody>
                    <?php if ($this->session->cari_rekapitulasi_transaksi == 1) {
                        foreach ($transaksi as $value) { ?>
                    <tr>
                        <td><?php echo $value->waktu ?></td>
                        <td><?php echo $value->nama_kelompok_transaksi ?></td>
                        <td><?php echo $value->nama_jenis_transaksi ?></td>
                        <td><?php echo $value->nik ?></td>
                        <td><?php echo $value->nikes ?></td>
                        <td><?php echo $value->username ?></td>
                    </tr>
                        <?php
                        }
                    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>