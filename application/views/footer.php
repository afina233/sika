                    </div>
                    <div class="app-wrapper-footer">
                        <div class="app-footer">
                            <div class="app-footer__inner">
                                <div class="app-footer-left">
                                    <ul class="nav">
                                    </ul>
                                </div>
                                <div class="app-footer-right">
                                    <ul class="nav">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        </div>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            // Initialize Datatables
            $.fn.dataTable.ext.classes.sPageButton = 'btn btn-light'; // Change Pagination Button Class
            

        } );
        $(document).ready(function() {
            $('#data_admin').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'copy',
                        text: 'Copy isi tabel',
                        className: 'btn btn-light'
                    }, 
                    {
                        extend: 'csv',
                        text: 'Simpan sebagai CSV',
                        className: 'btn btn-light'
                    }, 
                    {
                        extend: 'excel',
                        text: 'Simpan sebagai Excel',
                        className: 'btn btn-light'
                    }, 
                    {
                        extend: 'pdf',
                        text: 'Simpan sebagai PDF',
                        className: 'btn btn-light'
                    },
                    {
                        extend: 'print',
                        text: 'Print',
                        className: 'btn btn-light'
                    }
                ],
                
                "language": {
                    "lengthMenu": "Menampilkan _MENU_ baris per halaman",
                    "zeroRecords": "Silakan cari peserta terlebih dahulu.",
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Tidak ada data peserta.",
                    "search": "",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                      "previous": "Sebelumnya",
                      "next": "Berikutnya"
                    }
                }

            });
            $('.dataTables_filter input').addClass('form-control-sm form-control');
            $('.dataTables_filter input').attr('placeholder', 'Cari...');
        } );

        $(document).ready(function() {
            $('#cari_peserta_tabel').DataTable({
                "language": {
                    "lengthMenu": "Menampilkan _MENU_ baris per halaman",
                    "zeroRecords": "Silakan cari peserta terlebih dahulu.",
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Tidak ada data peserta.",
                    "search": "Cari peserta",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                      "previous": "Sebelumnya",
                      "next": "Berikutnya"
                    }
                }


            });

        } );

        


        $(document).ready(function() {
            $('#cari_kepesertaan_tabel').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'copy',
                        text: 'Copy isi tabel',
                        className: 'btn btn-light'
                    }, 
                    {
                        extend: 'csv',
                        text: 'Simpan sebagai CSV',
                        className: 'btn btn-light'
                    }, 
                    {
                        extend: 'excel',
                        text: 'Simpan sebagai Excel',
                        className: 'btn btn-light'
                    }, 
                    {
                        extend: 'pdf',
                        text: 'Simpan sebagai PDF',
                        className: 'btn btn-light'
                    },
                    {
                        extend: 'print',
                        text: 'Print',
                        className: 'btn btn-light'
                    }
                ],

                "language": {
                    "lengthMenu": "Menampilkan _MENU_ baris per halaman",
                    "zeroRecords": "Silakan cari peserta terlebih dahulu.",
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Tidak ada data peserta.",
                    "search": "",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                      "previous": "Sebelumnya",
                      "next": "Berikutnya"
                    }
                }

            });

            $('.dataTables_filter input').addClass('form-control-sm form-control');
            $('.dataTables_filter input').attr('placeholder', 'Cari...');

        } );


        $(document).ready(function() {
            $('#rekapitulasi_transaksi_tabel').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'copy',
                        text: 'Copy isi tabel',
                        className: 'btn btn-light'

                    }, 
                    {
                        extend: 'csv',
                        text: 'Simpan sebagai CSV',
                        className: 'btn btn-light'
                    }, 
                    {
                        extend: 'excel',
                        text: 'Simpan sebagai Excel',
                        className: 'btn btn-light'
                    }, 
                    {
                        extend: 'pdf',
                        text: 'Simpan sebagai PDF',
                        className: 'btn btn-light'
                    },
                    {
                        extend: 'print',
                        text: 'Print',
                        className: 'btn btn-light'
                    }
                ],

                "language": {
                    "lengthMenu": "Menampilkan _MENU_ baris per halaman",
                    "zeroRecords": "Silakan cari transaksi terlebih dahulu.",
                    "info": "Menampilkan halaman _PAGE_ dari _PAGES_",
                    "infoEmpty": "Tidak ada data transaksi.",
                    "search": "",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                      "previous": "Sebelumnya",
                      "next": "Berikutnya"
                    }
                },
                "order": [[ 0, "desc" ]]


            });

            $('.dataTables_filter input').addClass('form-control-sm form-control');
            $('.dataTables_filter input').attr('placeholder', 'Cari...');

        } );

        $('#area_pelayanan_cari_peserta').change(function(){ 
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url('PesertaController/getTPK');?>",
                method : "POST",
                data : {id: id},
                async : true,
                dataType : 'json',
                success: function(data){
                    var html = '<option value="" hidden>Pilih TPK</option>';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].kode_tpk+'>'+data[i].nama_tpk+'</option>';
                    }
                    $('#tpk_cari_peserta').html(html);
                }
            });
            return false;
        }); 

        $('#group_jenis_peserta').change(function(){ 
            var id=$(this).val();
            $.ajax({
                url : "<?php echo base_url('PesertaController/getJenisPesertaByGroup');?>",
                method : "POST",
                data : {id: id},
                async : true,
                dataType : 'json',
                success: function(data){
                    var html = '<option value="" hidden>Pilih TPK</option>';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].kode_jenis_peserta+'>'+data[i].nama_jenis_peserta+'</option>';
                    }
                    $('#jenis_peserta_rekap').html(html);
                }
            });
            var id_2=$('#kelompok_transaksi').val();
                $.ajax({
                    url : "<?php echo base_url('TransaksiController/getJenisTransaksiRekap');?>",
                    method : "POST",
                    data : {id: id, id_2: id_2},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '<option value="" hidden>Pilih Jenis Transaksi</option>';
                        
                        var i;
                        for(i=0; i<data.length; i++){
                            if (data[i].kode_group_status_keluarga == 1) {
                                html += '<option value='+data[i].kode_jenis_transaksi+'> Kepala Keluarga - '+data[i].nama_jenis_transaksi+'</option>';  
                            } else if (data[i].kode_group_status_keluarga == 2){
                                html += '<option value='+data[i].kode_jenis_transaksi+'> Pasangan - '+data[i].nama_jenis_transaksi+'</option>';  

                            }else if (data[i].kode_group_status_keluarga == 3){
                                html += '<option value='+data[i].kode_jenis_transaksi+'> Anak - '+data[i].nama_jenis_transaksi+'</option>';  
                            }
                        }
                        $('#jenis_transaksi_rekap').html(html);
 
                    }
                });
            return false;
        }); 
    </script>
    <script type="text/javascript">
        window.onbeforeunload = function() {
            // edit peserta
            localStorage.setItem("nikep", $('#nikep').val());
            localStorage.setItem("no_peserta", $('#no_peserta').val());
            localStorage.setItem("jenis_kepesertaan", $('#jenis_kepesertaan').val());
            localStorage.setItem("area_pelayanan_cari_peserta", $('#area_pelayanan_cari_peserta').val());
            // localStorage.setItem("tpk_cari_peserta", $('#tpk_cari_peserta').val());

            // data kepesertaan
            localStorage.setItem("area_pelayanan", $('#area_pelayanan').val());
            localStorage.setItem("tpk_kepesertaan", $('#tpk_kepesertaan').val());
            localStorage.setItem("group_status_keluarga", $('#group_status_keluarga').val());
            localStorage.setItem("group_jenis_peserta", $('#group_jenis_peserta').val());

            // rekapitulasi transaksi
            localStorage.setItem("kelompok_transaksi", $('#kelompok_transaksi').val());
            localStorage.setItem("jenis_transaksi_rekap", $('#jenis_transaksi_rekap').val());
            localStorage.setItem("periode_1", $('#periode_1').val());
            localStorage.setItem("periode_2", $('#periode_2').val());

            // input admin
            localStorage.setItem("nama_admin", $('#nama_admin').val());
            localStorage.setItem("username", $('#username').val());
            



            // localStorage.setItem("kelompok_transaksi_kk", $('#kelompok_transaksi_kk').val());
            // localStorage.setItem("jenis_transaksi_kk", $('#jenis_transaksi_kk').val());

            // localStorage.setItem("foto_kk_preview", $('#image-preview').val());
            // localStorage.setItem("foto_kk", $('#image-source').val());
            
            localStorage.setItem("nik", $('#nik').val());

            localStorage.setItem("nama", $('#nama').val());
            localStorage.setItem("tempat_lahir", $('#tempat_lahir').val());
            localStorage.setItem("tgl_lahir", $('#datepicker_tgl_lahir').val());
            localStorage.setItem("jenis_kelamin", $('#jenis_kelamin').val());
            localStorage.setItem("agama", $('#agama').val());
            localStorage.setItem("no_ktp", $('#no_ktp').val());
            localStorage.setItem("gol_darah", $('#gol_darah').val());
            localStorage.setItem("rhesus", $('#rhesus').val());

            localStorage.setItem("no_bpjs", $('#no_bpjs').val());
            // localStorage.setItem("area_pelayanan", $('#area_pelayanan').val());
            // localStorage.setItem("tpk", $('#tpk').val());
            localStorage.setItem("status_faskes", $('#status_faskes').val());

            localStorage.setItem("tgl_faskes", $('#datepicker_tgl_faskes').val());
            localStorage.setItem("kelompok_peserta", $('#kelompok_peserta').val());
            localStorage.setItem("tgl_capeg", $('#datepicker_tgl_capeg').val());
            localStorage.setItem("tgl_mulai_kerja", $('#datepicker_tgl_mulai_kerja').val());
            localStorage.setItem("tgl_pensiun", $('#datepicker_tgl_pensiun').val());
            localStorage.setItem("tgl_berhenti_kerja", $('#datepicker_tgl_berhenti_kerja').val());
            localStorage.setItem("alasan_berhenti_kerja", $('#alasan_berhenti_kerja').val());

            // localStorage.setItem("pendidikan", $('#pendidikan').val());
            // localStorage.setItem("instansi", $('#instansi').val());
            localStorage.setItem("jabatan", $('#jabatan').val()); 
            // localStorage.setItem("divisi", $('#divisi').val());
            // localStorage.setItem("bagian", $('#bagian').val());
            // localStorage.setItem("band_posisi", $('#band_posisi').val());
            // localStorage.setItem("klas_posisi", $('#klas_posisi').val());
            localStorage.setItem("kelas_perawatan", $('#kelas_perawatan').val());
            // localStorage.setItem("personal_area", $('#personal_area').val());
            // localStorage.setItem("personal_sub_area", $('#personal_sub_area').val());
            localStorage.setItem("alamat_ktp", $('#alamat_ktp').val());
            localStorage.setItem("rt_ktp", $('#rt_ktp').val());
            localStorage.setItem("rw_ktp", $('#rw_ktp').val());
            // localStorage.setItem("provinsi_ktp", $('#provinsi_ktp').val());
            // localStorage.setItem("kabupaten_ktp", $('#kabupaten_ktp').val());
            // localStorage.setItem("kecamatan_ktp", $('#kecamatan_ktp').val());
            // localStorage.setItem("kelurahan_ktp", $('#kelurahan_ktp').val());
            localStorage.setItem("kode_pos_ktp", $('#kode_pos_ktp').val());
            localStorage.setItem("no_telp_ktp", $('#no_telp_ktp').val());
            localStorage.setItem("alamat_domisili", $('#alamat_domisili').val());
            localStorage.setItem("rt_domisili", $('#rt_domisili').val());
            localStorage.setItem("rw_domisili", $('#rw_domisili').val());
            // localStorage.setItem("provinsi_domisili", $('#provinsi_domisili').val());
            // localStorage.setItem("kabupaten_domisili", $('#kabupaten_domisili').val());
            // localStorage.setItem("kecamatan_domisili", $('#kecamatan_domisili').val());
            // localStorage.setItem("kelurahan_domisili", $('#kelurahan_domisili').val());
            localStorage.setItem("kode_pos_domisili", $('#kode_pos_domisili').val());
            localStorage.setItem("no_telp_domisili", $('#no_telp_domisili').val());
            localStorage.setItem("no_hp", $('#no_hp').val());
            localStorage.setItem("nama_pemilik_rekening", $('#nama_pemilik_rekening').val());
            // localStorage.setItem("nama_bank", $('#nama_bank').val());
            localStorage.setItem("no_rekening_bank", $('#no_rekening_bank').val());
            localStorage.setItem("status_kartu", $('#status_kartu').val());
            localStorage.setItem("tgl_cetak", $('#datepicker_tgl_cetak').val());
            localStorage.setItem("tgl_akhir_kartu", $('#datepicker_tgl_akhir_kartu').val());
            localStorage.setItem("status_pernikahan", $('#status_pernikahan').val());
            localStorage.setItem("tgl_nikah", $('#datepicker_tgl_nikah').val());
            localStorage.setItem("tgl_cerai", $('#datepicker_tgl_cerai').val());
            localStorage.setItem("tgl_meninggal", $('#datepicker_tgl_meninggal').val());
            localStorage.setItem("catatan", $('#catatan').val());
            
            

        } 
    </script>
    <script>
        $('#datepicker_tgl_lahir').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_faskes').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_capeg').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_mulai_kerja').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_pensiun').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_berhenti_kerja').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_cetak').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_akhir_kartu').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_nikah').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_cerai').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_meninggal').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_dokumen_1').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_dokumen_2').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
        $('#datepicker_tgl_dokumen_3').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });

        $('#datepicker_tgl_dokumen').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });

        $('#datepicker_periode_1').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });

        $('#datepicker_periode_2').datepicker({
            format: 'dd/mm/yyyy',
            uiLibrary: 'bootstrap'
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){

            $('#provinsi_ktp').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getKabupaten');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'</option>';
                        }
                        $('#kabupaten_ktp').html(html);
 
                    }
                });
                return false;
            }); 

            $('#kabupaten_ktp').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getKecamatan');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'</option>';
                        }
                        $('#kecamatan_ktp').html(html);
 
                    }
                });
                return false;
            }); 

            $('#kecamatan_ktp').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getKelurahan');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'</option>';
                        }
                        $('#kelurahan_ktp').html(html);
 
                    }
                });
                return false;
            });

            $('#provinsi_domisili').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getKabupaten');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'</option>';
                        }
                        $('#kabupaten_domisili').html(html);
 
                    }
                });
                return false;
            }); 

            $('#kabupaten_domisili').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getKecamatan');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'</option>';
                        }
                        $('#kecamatan_domisili').html(html);
 
                    }
                });
                return false;
            }); 

            $('#kecamatan_domisili').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getKelurahan');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'</option>';
                        }
                        $('#kelurahan_domisili').html(html);
 
                    }
                });
                return false;
            });

            $('#kelompok_peserta').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getInstansi');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].kode_instansi+'>'+data[i].nama_instansi+'</option>';
                        }
                        $('#instansi').html(html);
 
                    }
                });
                return false;
            });

            $('#instansi').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getDivisi');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].kode_divisi+'>'+data[i].nama_divisi+'</option>';
                        }
                        $('#divisi').html(html);
 
                    }
                });
                return false;
            });

            $('#band_posisi').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getKlasPosisi');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].kode_band_posisi+'>'+data[i].klas_posisi+'</option>';
                        }
                        $('#klas_posisi').html(html);
 
                    }
                });
                return false;
            });

            $('#personal_area').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getPersonalSubArea');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].kode+'>'+data[i].nama_personal_sub_area+'</option>';
                        }
                        $('#personal_sub_area').html(html);
 
                    }
                });
                return false;
            });

    

            $('#jenis_peserta_kk').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('TransaksiController/getJenisTransaksiByJenisPeserta');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            if (data[i].kode_kelompok_transaksi == "01" && data[i].kode_group_status_keluarga == "1") {
                                html += '<option value='+data[i].kode_jenis_transaksi+'>' + data[i].nama_jenis_transaksi+'</option>';         
                                
                            }
                        }
                        $('#jenis_transaksi_kk').html(html);
                    }
                });

                $.ajax({
                    url : "<?php echo base_url('TransaksiController/getJenisTransaksiByJenisPeserta');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            if (data[i].kode_kelompok_transaksi == "01" && data[i].kode_group_status_keluarga == "2") {
                                html += '<option value='+data[i].kode_jenis_transaksi+'>' + data[i].nama_jenis_transaksi+'</option>';         
                                
                            }
                        }
                        $('#jenis_transaksi_pasangan').html(html);
                    }
                });

                $.ajax({
                    url : "<?php echo base_url('TransaksiController/getJenisTransaksiByJenisPeserta');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            if (data[i].kode_kelompok_transaksi == "01" && data[i].kode_group_status_keluarga == "3") {
                                html += '<option value='+data[i].kode_jenis_transaksi+'>' + data[i].nama_jenis_transaksi+'</option>';         
                                
                            }
                        }
                        $('#jenis_transaksi_anak').html(html);
                    }
                });

                return false;
            });

            // edit ni
            $('#kelompok_transaksi_kk_edit').change(function(){ 
                var id=$(this).val();
                var id_2=$('#kode_group_jenis_peserta').val();
                $.ajax({
                    url : "<?php echo base_url('TransaksiController/getJenisTransaksiKKEdit');?>",
                    method : "POST",
                    data : {id: id, id_2: id_2},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            if (data[i].kode_group_status_keluarga == 1) {

                                html += '<option value='+data[i].kode_jenis_transaksi+'>' + data[i].nama_jenis_transaksi+'</option>';         
                            }    
                                
                        }
                        $('#jenis_transaksi_kk_edit').html(html);
                    }
                });
                return false;
            });

            $('#kelompok_transaksi_pasangan_edit').change(function(){ 
                var id=$(this).val();
                var id_2=$('#kode_group_jenis_peserta').val();

                $.ajax({
                    url : "<?php echo base_url('TransaksiController/getJenisTransaksiKKEdit');?>",
                    method : "POST",
                    data : {id: id, id_2: id_2},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            if (data[i].kode_group_status_keluarga == 2) {

                                html += '<option value='+data[i].kode_jenis_transaksi+'>' + data[i].nama_jenis_transaksi+'</option>';         
                            }
                                
                                
                        }
                        $('#jenis_transaksi_pasangan_edit').html(html);
                    }
                });
                return false;
            });

            $('#kelompok_transaksi_anak_edit').change(function(){ 
                var id=$(this).val();
                var id_2=$('#kode_group_jenis_peserta').val();

                $.ajax({
                    url : "<?php echo base_url('TransaksiController/getJenisTransaksiKKEdit');?>",
                    method : "POST",
                    data : {id: id, id_2: id_2},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            if (data[i].kode_group_status_keluarga == 3) {

                                html += '<option value='+data[i].kode_jenis_transaksi+'>' + data[i].nama_jenis_transaksi+'</option>';         
                            }
                                
                                
                        }
                        $('#jenis_transaksi_anak_edit').html(html);
                    }
                });
                return false;
            });

          

            $('#tpk').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getKotaKantorByTPK');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option>' + data[i].nama_kota_kantor+'</option>';            
                                
                        }
                        $('#kota_tpk').html(html);
                    }
                });

                $.ajax({
                    url : "<?php echo base_url('PesertaController/getTPK2');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option>' + data[i].alamat+'</option>';            
                                
                        }
                        $('#alamat_tpk').html(html);
                    }
                });
            });

            $('#tpk_asal').change(function(){ 
                var id=$(this).val();


                $.ajax({
                    url : "<?php echo base_url('PesertaController/getKotaKantorByTPK');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option>' + data[i].nama_kota_kantor+'</option>';            
                                
                        }
                        $('#kota_tpk_asal').html(html);
                    }
                });

                $.ajax({
                    url : "<?php echo base_url('PesertaController/getTPK2');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option>' + data[i].alamat+'</option>';            
                                
                        }
                        $('#alamat_tpk_asal').html(html);
                    }
                });
                return false;

            });

            $('#tpk_tujuan').change(function(){ 
                var id=$(this).val();


                $.ajax({
                    url : "<?php echo base_url('PesertaController/getKotaKantorByTPK');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option>' + data[i].nama_kota_kantor+'</option>';            
                                
                        }
                        $('#kota_tpk_tujuan').html(html);
                    }
                });

                $.ajax({
                    url : "<?php echo base_url('PesertaController/getTPK2');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option>' + data[i].alamat+'</option>';            
                                
                        }
                        $('#alamat_tpk_tujuan').html(html);
                    }
                });
                return false;

            });

            // $('#tpk').change(function(){ 
            //     var id=$(this).val();


                
            // });


            $('#kelompok_transaksi').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('TransaksiController/getJenisTransaksi');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            if (data[i].kode_jenis_peserta == 12 || data[i].kode_jenis_peserta == 22 || data[i].kode_jenis_peserta == 32 || data[i].kode_jenis_peserta == 52) {
                                if (data[i].kode_jenis_peserta == 12) {
                                    html += '<option value='+data[i].kode_jenis_transaksi+'> Istri/ Suami Karyawan - ' + data[i].nama_jenis_transaksi+'</option>';         
                                }else if (data[i].kode_jenis_peserta == 22) {
                                    html += '<option value='+data[i].kode_jenis_transaksi+'> Istri/ Suami Pensiunan - ' + data[i].nama_jenis_transaksi+'</option>';         
                                }else if (data[i].kode_jenis_peserta == 32) {
                                    html += '<option value='+data[i].kode_jenis_transaksi+'> Istri/ Suami Karyawan Mitra - ' + data[i].nama_jenis_transaksi+'</option>';         
                                }else if (data[i].kode_jenis_peserta == 52) {
                                    html += '<option value='+data[i].kode_jenis_transaksi+'> Istri/ Suami Karyawan YAKES - ' + data[i].nama_jenis_transaksi+'</option>';         
                                }
                            }
                        }
                        $('#jenis_transaksi_pasangan').html(html);
 
                    }
                });
                return false;
            });

            $('#kelompok_transaksi').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('TransaksiController/getJenisTransaksi');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            if (data[i].kode_jenis_peserta == 13 || data[i].kode_jenis_peserta == 23 || data[i].kode_jenis_peserta == 33 || data[i].kode_jenis_peserta == 53) {
                                if (data[i].kode_jenis_peserta == 13) {
                                    html += '<option value='+data[i].kode_jenis_transaksi+'> Anak Karyawan - ' + data[i].nama_jenis_transaksi+'</option>';         
                                }else if (data[i].kode_jenis_peserta == 23) {
                                    html += '<option value='+data[i].kode_jenis_transaksi+'> Anak Pensiunan - ' + data[i].nama_jenis_transaksi+'</option>';         
                                }else if (data[i].kode_jenis_peserta == 33) {
                                    html += '<option value='+data[i].kode_jenis_transaksi+'> Anak Karyawan Mitra - ' + data[i].nama_jenis_transaksi+'</option>';         
                                }else if (data[i].kode_jenis_peserta == 53) {
                                    html += '<option value='+data[i].kode_jenis_transaksi+'> Anak Karyawan YAKES - ' + data[i].nama_jenis_transaksi+'</option>';         
                                }
                            }
                        }
                        $('#jenis_transaksi_anak').html(html);
 
                    }
                });
                return false;
            });

            $('#kelompok_transaksi').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('TransaksiController/getJenisTransaksi');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].kode_jenis_transaksi+'>'+data[i].nama_jenis_transaksi+'</option>';  
                        }
                        $('#jenis_transaksi').html(html);
 
                    }
                });
                return false;
            });


            $('#area_pelayanan').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getTPK');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].kode_tpk+'>'+data[i].nama_tpk+'</option>';
                        }
                        $('#tpk').html(html);
 
                    }
                });

                $.ajax({
                    url : "<?php echo base_url('PesertaController/getTPK');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '<option value="" hidden>Pilih TPK</option>';

                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].kode_tpk+'>'+data[i].nama_tpk+'</option>';
                        }
                        $('#tpk_asal').html(html);
 
                    }
                });

                $.ajax({
                    url : "<?php echo base_url('PesertaController/getTPK');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '<option value="" hidden>Pilih TPK</option>';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].kode_tpk+'>'+data[i].nama_tpk+'</option>';
                        }
                        $('#tpk_tujuan').html(html);
 
                    }
                });

                $.ajax({
                    url : "<?php echo base_url('PesertaController/getTPK');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '<option value="">Pilih TPK</option>';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].kode_tpk+'>'+data[i].nama_tpk+'</option>';
                        }
                        $('#tpk_kepesertaan').html(html);
 
                    }
                });
                return false;
            }); 


            $('#area_cari_nik').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo base_url('PesertaController/getTPK');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].kode_tpk+'>'+data[i].nama_tpk+'</option>';
                        }
                        $('#tpk_cari_nik').html(html);
 
                    }
                });
                return false;
            });






            
        });
    </script>
    <script type="text/javascript">
        function FillBilling(f) {
            if(f.ktp_domisili.checked == true) {
                f.alamat_domisili.value = f.alamat_ktp.value;
                f.rt_domisili.value = f.rt_ktp.value;
                f.rw_domisili.value = f.rw_ktp.value;
                f.provinsi_domisili.value = f.provinsi_ktp.value;
                f.kabupaten_domisili.value = f.kabupaten_ktp.value;
                f.kecamatan_domisili.value = f.kecamatan_ktp.value;
                f.kelurahan_domisili.value = f.kelurahan_ktp.value;
                f.kode_pos_domisili.value = f.kode_pos_ktp.value;
                f.no_telp_domisili.value = f.no_telp_ktp.value;
            }else{
                f.alamat_domisili.value = "";
                f.rt_domisili.value = "";
                f.rw_domisili.value = "";
                f.provinsi_domisili.value = "";
                f.kabupaten_domisili.value = "";
                f.kecamatan_domisili.value = "";
                f.kelurahan_domisili.value = "";
                f.kode_pos_domisili.value = "";
                f.no_telp_domisili.value = "";
              }
            }
    </script>
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    <script type="text/javascript">
        function previewImage() {
            document.getElementById("image-preview").style.display = "block";
            var oFReader = new FileReader();
             oFReader.readAsDataURL(document.getElementById("image-source").files[0]);
         
            oFReader.onload = function(oFREvent) {
              document.getElementById("image-preview").src = oFREvent.target.result;
            };
          };
    </script>
</body>
</html>

<!-- Modal edit data users -->

<!-- Modal hapus data user -->

<div class="modal fade bd-modal-hapus-datausers" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus user ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus dokumen -->

<div class="modal fade bd-modal-hapus-dokumen" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Dokumen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus dokumen ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah area -->

<div class="modal fade bd-modal-tambah-area" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_area" class="col-sm-3 col-form-label">Kode Area</label>
                    <div class="col-sm-6">
                        <input name="kode_area" id="kode_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_area" class="col-sm-3 col-form-label">Nama Area</label>
                    <div class="col-sm-6">
                        <input name="nama_area" id="nama_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="manager_area" class="col-sm-3 col-form-label">Manager Area</label>
                    <div class="col-sm-6">
                        <input name="manager_area" id="manager_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="singkatan_area" class="col-sm-3 col-form-label">Singkatan Area</label>
                    <div class="col-sm-6">
                        <input name="singkatan_area" id="singkatan_area" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus area -->

<div class="modal fade bd-modal-hapus-area" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus area ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit area -->

<div class="modal fade bd-modal-edit-area" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_area" class="col-sm-3 col-form-label">Kode Area</label>
                    <div class="col-sm-6">
                        <input name="kode_area" disabled="" id="kode_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_area" class="col-sm-3 col-form-label">Nama Area</label>
                    <div class="col-sm-6">
                        <input name="nama_area" id="nama_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="manager_area" class="col-sm-3 col-form-label">Manager Area</label>
                    <div class="col-sm-6">
                        <input name="manager_area" id="manager_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="singkatan_area" class="col-sm-3 col-form-label">SingkatanArea</label>
                    <div class="col-sm-6">
                        <input name="singkatan_area" id="singkatan_area" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah personal area -->

<div class="modal fade bd-modal-tambah-personal-area" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Personal Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_personal_area" class="col-sm-3 col-form-label">Kode Personal Area</label>
                    <div class="col-sm-6">
                        <input name="kode_personal_area" id="kode_personal_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_personal_area" class="col-sm-3 col-form-label">Nama Personal Area</label>
                    <div class="col-sm-6">
                        <input name="nama_personal_area" id="nama_personal_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="status_aktif" class="col-sm-3 col-form-label">Status Aktif</label>
                    <div class="col-sm-2">
                        <div class="position-relative form-check">
                            <label class="form-check-label"><input name="radio2" type="radio" class="form-check-input">Ya</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="position-relative form-check">
                            <label class="form-check-label"><input name="radio2" type="radio" class="form-check-input">Tidak</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus personal area -->

<div class="modal fade bd-modal-hapus-personal-area" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Personal Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus personal area ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit personal area -->

<div class="modal fade bd-modal-edit-personal-area" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Personal Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_personal_area" class="col-sm-3 col-form-label">Kode Personal Area</label>
                    <div class="col-sm-6">
                        <input name="kode_personal_area" disabled="" id="kode_personal_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_personal_area" class="col-sm-3 col-form-label">Nama Personal Area</label>
                    <div class="col-sm-6">
                        <input name="nama_personal_area" id="nama_personal_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="status_aktif" class="col-sm-3 col-form-label">Status Aktif</label>
                    <div class="col-sm-2">
                        <div class="position-relative form-check">
                            <label class="form-check-label"><input name="radio2" type="radio" class="form-check-input">Ya</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="position-relative form-check">
                            <label class="form-check-label"><input name="radio2" type="radio" class="form-check-input">Tidak</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah personal sub area -->

<div class="modal fade bd-modal-tambah-personal-sub-area" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Personal Sub Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="personal_area" class="col-sm-3 col-form-label">Personal Area</label>
                    <div class="col-sm-6">
                        <select name="personal_area" id="personal_area" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kode_personal_sub_area" class="col-sm-3 col-form-label">Kode Personal Sub Area</label>
                    <div class="col-sm-6">
                        <input name="kode_personal_sub_area" id="kode_personal_sub_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_personal_sub_area" class="col-sm-3 col-form-label">Nama Personal Sub Area</label>
                    <div class="col-sm-6">
                        <input name="nama_personal_sub_area" id="nama_personal_sub_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="status_aktif" class="col-sm-3 col-form-label">Status Aktif</label>
                    <div class="col-sm-2">
                        <div class="position-relative form-check">
                            <label class="form-check-label"><input name="radio2" type="radio" class="form-check-input">Ya</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="position-relative form-check">
                            <label class="form-check-label"><input name="radio2" type="radio" class="form-check-input">Tidak</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus personal sub area -->

<div class="modal fade bd-modal-hapus-personal-sub-area" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Personal Sub Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus personal sub area ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit personal sub area -->

<div class="modal fade bd-modal-edit-personal-sub-area" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Personal Sub Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="personal_area" class="col-sm-3 col-form-label">Personal Area</label>
                    <div class="col-sm-6">
                        <select name="personal_area" id="personal_area" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kode_personal_sub_area" class="col-sm-3 col-form-label">Kode Personal Sub Area</label>
                    <div class="col-sm-6">
                        <input name="kode_personal_sub_area" disabled="" id="kode_personal_sub_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_personal_sub_area" class="col-sm-3 col-form-label">Nama Personal Sub Area</label>
                    <div class="col-sm-6">
                        <input name="nama_personal_sub_area" id="nama_personal_sub_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="status_aktif" class="col-sm-3 col-form-label">Status Aktif</label>
                    <div class="col-sm-2">
                        <div class="position-relative form-check">
                            <label class="form-check-label"><input name="radio2" type="radio" class="form-check-input">Ya</label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="position-relative form-check">
                            <label class="form-check-label"><input name="radio2" type="radio" class="form-check-input">Tidak</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah kelompok peserta -->

<div class="modal fade bd-modal-tambah-kelompok-peserta" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Kelompok Peserta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_kelompok_peserta" class="col-sm-3 col-form-label">Kode Kelompok Peserta</label>
                    <div class="col-sm-6">
                        <input name="kode_kelompok_peserta" id="kode_kelompok_peserta" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kelompok_peserta" class="col-sm-3 col-form-label">Nama Kelompok Peserta</label>
                    <div class="col-sm-6">
                        <input name="nama_kelompok_peserta" id="nama_kelompok_peserta" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus kelompok peserta -->

<div class="modal fade bd-modal-hapus-kelompok-peserta" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Kelompok Peserta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus kelompok peserta ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit kelompok peserta -->

<div class="modal fade bd-modal-edit-kelompok-peserta" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Kelompok Peserta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_kelompok_peserta" class="col-sm-3 col-form-label">Kode Kelompok Peserta</label>
                    <div class="col-sm-6">
                        <input name="kode_kelompok_peserta" disabled="" id="kode_kelompok_peserta" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kelompok_peserta" class="col-sm-3 col-form-label">Nama Kelompok Peserta</label>
                    <div class="col-sm-6">
                        <input name="nama_kelompok_peserta" id="nama_kelompok_peserta" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah group jenis peserta -->

<div class="modal fade bd-modal-tambah-group-jenis-peserta" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Group Jenis Peserta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_group_jenis_peserta" class="col-sm-3 col-form-label">Kode Group Jenis Peserta</label>
                    <div class="col-sm-6">
                        <input name="kode_group_jenis_peserta" id="kode_group_jenis_peserta" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_group_jenis_peserta" class="col-sm-3 col-form-label">Nama Group Jenis Peserta</label>
                    <div class="col-sm-6">
                        <input name="nama_group_jenis_peserta" id="nama_group_jenis_peserta" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal hapus group jenis peserta -->

<div class="modal fade bd-modal-hapus-group-jenis-peserta" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Group Jenis Peserta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus group jenis peserta ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit group jenis peserta -->

<div class="modal fade bd-modal-edit-group-jenis-peserta" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Group Jenis Peserta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_group_jenis_peserta" class="col-sm-3 col-form-label">Kode Group Jenis Peserta</label>
                    <div class="col-sm-6">
                        <input name="kode_group_jenis_peserta" disabled="" id="kode_group_jenis_peserta" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_group_jenis_peserta" class="col-sm-3 col-form-label">Nama Group Jenis Peserta</label>
                    <div class="col-sm-6">
                        <input name="nama_group_jenis_peserta" id="nama_group_jenis_peserta" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah jenis peserta -->

<div class="modal fade bd-modal-tambah-jenis-peserta" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Jenis Peserta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_group_jenis_peserta" class="col-sm-3 col-form-label">Kode Jenis Peserta</label>
                    <div class="col-sm-6">
                        <input name="kode_group_jenis_peserta" id="kode_group_jenis_peserta" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_group_jenis_peserta" class="col-sm-3 col-form-label">Nama Jenis Peserta</label>
                    <div class="col-sm-6">
                        <input name="nama_group_jenis_peserta" id="nama_group_jenis_peserta" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="x" class="col-sm-3 col-form-label">Group Jenis Peserta</label>
                    <div class="col-sm-6">
                        <select name="select" id="x" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="group_status_peserta" class="col-sm-3 col-form-label">Group Status Keluarga</label>
                    <div class="col-sm-6">
                        <select name="select" id="group_status_peserta" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus jenis peserta -->

<div class="modal fade bd-modal-hapus-jenis-peserta" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Jenis Peserta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus jenis peserta ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit jenis peserta -->

<div class="modal fade bd-modal-edit-jenis-peserta" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Jenis Peserta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_group_jenis_peserta" class="col-sm-3 col-form-label">Kode Jenis Peserta</label>
                    <div class="col-sm-6">
                        <input name="kode_group_jenis_peserta" disabled="" id="kode_group_jenis_peserta" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_group_jenis_peserta" class="col-sm-3 col-form-label">Nama Jenis Peserta</label>
                    <div class="col-sm-6">
                        <input name="nama_group_jenis_peserta" id="nama_group_jenis_peserta" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="x" class="col-sm-3 col-form-label">Group Jenis Peserta</label>
                    <div class="col-sm-6">
                        <select name="select" id="x" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="group_status_peserta" class="col-sm-3 col-form-label">Group Status Keluarga</label>
                    <div class="col-sm-6">
                        <select name="select" id="group_status_peserta" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah instansi -->

<div class="modal fade bd-modal-tambah-instansi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Instansi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_instansi" class="col-sm-3 col-form-label">Kode Instansi</label>
                    <div class="col-sm-6">
                        <input name="kode_instansi" id="kode_instansi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_instansi" class="col-sm-3 col-form-label">Nama Instansi</label>
                    <div class="col-sm-6">
                        <input name="nama_instansi" id="nama_instansi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kelompok_peserta" class="col-sm-3 col-form-label">Kelompok Peserta</label>
                    <div class="col-sm-6">
                        <select name="select" id="kelompok_peserta" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus instansi -->

<div class="modal fade bd-modal-hapus-instansi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Instansi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus instansi ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit instansi -->

<div class="modal fade bd-modal-edit-instansi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Instansi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_instansi" class="col-sm-3 col-form-label">Kode Instansi</label>
                    <div class="col-sm-6">
                        <input name="kode_instansi" disabled="" id="kode_instansi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_instansi" class="col-sm-3 col-form-label">Nama Instansi</label>
                    <div class="col-sm-6">
                        <input name="nama_instansi" id="nama_instansi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kelompok_peserta" class="col-sm-3 col-form-label">Kelompok Peserta</label>
                    <div class="col-sm-6">
                        <select name="select" id="kelompok_peserta" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah pekerjaan -->

<div class="modal fade bd-modal-tambah-pekerjaan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Pekerjaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_pekerjaan" class="col-sm-3 col-form-label">Kode Pekerjaan</label>
                    <div class="col-sm-6">
                        <input name="kode_pekerjaan" id="kode_pekerjaan" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_pekerjaan" class="col-sm-3 col-form-label">Nama Pekerjaan</label>
                    <div class="col-sm-6">
                        <input name="nama_pekerjaan" id="nama_pekerjaan" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="perusahaan" class="col-sm-3 col-form-label">Perusahaan</label>
                    <div class="col-sm-6">
                        <select name="select" id="perusahaan" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus pekerjaan -->

<div class="modal fade bd-modal-hapus-pekerjaan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Pekerjaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus pekerjaan ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit pekerjaan -->

<div class="modal fade bd-modal-edit-pekerjaan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Pekerjaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_pekerjaan" class="col-sm-3 col-form-label">Kode Pekerjaan</label>
                    <div class="col-sm-6">
                        <input name="kode_pekerjaan" disabled="" id="kode_pekerjaan" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_pekerjaan" class="col-sm-3 col-form-label">Nama Pekerjaan</label>
                    <div class="col-sm-6">
                        <input name="nama_pekerjaan" id="nama_pekerjaan" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="perusahaan" class="col-sm-3 col-form-label">Perusahaan</label>
                    <div class="col-sm-6">
                        <select name="select" id="perusahaan" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah bank -->

<div class="modal fade bd-modal-tambah-bank" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Bank</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_bank" class="col-sm-3 col-form-label">Kode Bank</label>
                    <div class="col-sm-6">
                        <input name="kode_bank" id="kode_bank" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_bank" class="col-sm-3 col-form-label">Nama Bank</label>
                    <div class="col-sm-6">
                        <input name="nama_bank" id="nama_bank" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus bank -->

<div class="modal fade bd-modal-hapus-bank" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Bank</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus bank ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit bank -->

<div class="modal fade bd-modal-edit-bank" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Bank</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_bank" class="col-sm-3 col-form-label">Kode Bank</label>
                    <div class="col-sm-6">
                        <input name="kode_bank" disabled="" id="kode_bank" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_bank" class="col-sm-3 col-form-label">Nama Bank</label>
                    <div class="col-sm-6">
                        <input name="nama_bank" id="nama_bank" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah kelompok transaksi -->

<div class="modal fade bd-modal-tambah-kelompok-transaksi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah kelompok Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_kelompok_transaksi" class="col-sm-3 col-form-label">Kode kelompok Transaksi</label>
                    <div class="col-sm-6">
                        <input name="kode_kelompok_transaksi" id="kode_kelompok_transaksi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kelompok_transaksi" class="col-sm-3 col-form-label">Nama kelompok Transaksi</label>
                    <div class="col-sm-6">
                        <input name="nama_kelompok_transaksi" id="nama_kelompok_transaksi" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus kelompok transaksi -->

<div class="modal fade bd-modal-hapus-kelompok-transaksi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Kelompok Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus kelompok transaksi ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit kelompok transaksi -->

<div class="modal fade bd-modal-edit-kelompok-transaksi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit kelompok Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_kelompok_transaksi" class="col-sm-3 col-form-label">Kode kelompok Transaksi</label>
                    <div class="col-sm-6">
                        <input name="kode_kelompok_transaksi" disabled="" id="kode_kelompok_transaksi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kelompok_transaksi" class="col-sm-3 col-form-label">Nama kelompok Transaksi</label>
                    <div class="col-sm-6">
                        <input name="nama_kelompok_transaksi" id="nama_kelompok_transaksi" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah jenis transaksi -->

<div class="modal fade bd-modal-tambah-jenis-transaksi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Jenis Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_jenis_transaksi" class="col-sm-3 col-form-label">Kode Jenis Transaksi</label>
                    <div class="col-sm-6">
                        <input name="kode_jenis_transaksi" id="kode_jenis_transaksi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_jenis_transaksi" class="col-sm-3 col-form-label">Nama Jenis Transaksi</label>
                    <div class="col-sm-6">
                        <input name="nama_jenis_transaksi" id="nama_jenis_transaksi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kelompok_transaksi" class="col-sm-3 col-form-label">Kelompok Transaksi</label>
                    <div class="col-sm-6">
                        <select name="select" id="kelompok_transaksi" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus jenis transaksi -->

<div class="modal fade bd-modal-hapus-jenis-transaksi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Jenis Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus jenis transaksi ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit jenis transaksi -->

<div class="modal fade bd-modal-edit-jenis-transaksi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Jenis Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_jenis_transaksi" class="col-sm-3 col-form-label">Kode Jenis Transaksi</label>
                    <div class="col-sm-6">
                        <input name="kode_jenis_transaksi" disabled="" id="kode_jenis_transaksi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_jenis_transaksi" class="col-sm-3 col-form-label">Nama Jenis Transaksi</label>
                    <div class="col-sm-6">
                        <input name="nama_jenis_transaksi" id="nama_jenis_transaksi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kelompok_transaksi" class="col-sm-3 col-form-label">Kelompok Transaksi</label>
                    <div class="col-sm-6">
                        <select name="select" id="kelompok_transaksi" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah band posisi -->

<div class="modal fade bd-modal-tambah-band-posisi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Band Posisi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_band_posisi" class="col-sm-3 col-form-label">Kode Band Posisi</label>
                    <div class="col-sm-6">
                        <input name="kode_band_posisi" id="kode_band_posisi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_band_posisi" class="col-sm-3 col-form-label">Nama Band Posisi</label>
                    <div class="col-sm-6">
                        <input name="nama_band_posisi" id="nama_band_posisi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="klas_posisi" class="col-sm-3 col-form-label">Klas Posisi</label>
                    <div class="col-sm-6">
                        <input name="klas_posisi" id="klas_posisi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kelas_perawatan" class="col-sm-3 col-form-label">Kelas Perawatan</label>
                    <div class="col-sm-6">
                        <select name="select" id="kelas_perawatan" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus band posisi -->

<div class="modal fade bd-modal-hapus-band-posisi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Band Posisi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus band posisi ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit band posisi -->

<div class="modal fade bd-modal-edit-band-posisi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Band Posisi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_band_posisi" class="col-sm-3 col-form-label">Kode Band Posisi</label>
                    <div class="col-sm-6">
                        <input name="kode_band_posisi" disabled="" id="kode_band_posisi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_band_posisi" class="col-sm-3 col-form-label">Nama Band Posisi</label>
                    <div class="col-sm-6">
                        <input name="nama_band_posisi" id="nama_band_posisi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="klas_posisi" class="col-sm-3 col-form-label">Klas Posisi</label>
                    <div class="col-sm-6">
                        <input name="klas_posisi" id="klas_posisi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kelas_perawatan" class="col-sm-3 col-form-label">Kelas Perawatan</label>
                    <div class="col-sm-6">
                        <select name="select" id="kelas_perawatan" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah kota kantor -->

<div class="modal fade bd-modal-tambah-kota-kantor" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Kota Kantor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_kota_kantor" class="col-sm-3 col-form-label">Kode Kota Kantor</label>
                    <div class="col-sm-6">
                        <input name="kode_kota_kantor" id="kode_kota_kantor" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kota_kantor" class="col-sm-3 col-form-label">Nama Kota Kantor</label>
                    <div class="col-sm-6">
                        <input name="nama_kota_kantor" id="nama_kota_kantor" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="area" class="col-sm-3 col-form-label">Area</label>
                    <div class="col-sm-6">
                        <select name="area" id="area" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus kota kantor -->

<div class="modal fade bd-modal-hapus-kota-kantor" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Kota Kantor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus kota kantor ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit kota kantor -->

<div class="modal fade bd-modal-edit-kota-kantor" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Kota Kantor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_kota_kantor" class="col-sm-3 col-form-label">Kode Kota Kantor</label>
                    <div class="col-sm-6">
                        <input name="kode_kota_kantor" disabled="" id="kode_kota_kantor" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kota_kantor" class="col-sm-3 col-form-label">Nama Kota Kantor</label>
                    <div class="col-sm-6">
                        <input name="nama_kota_kantor" id="nama_kota_kantor" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="area" class="col-sm-3 col-form-label">Area</label>
                    <div class="col-sm-6">
                        <select name="area" id="area" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah HR HOST -->

<div class="modal fade bd-modal-tambah-hr-host" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah HR HOST</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_hr_host" class="col-sm-3 col-form-label">Kode HR HOST</label>
                    <div class="col-sm-6">
                        <input name="kode_hr_host" id="kode_hr_host" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_hr_host" class="col-sm-3 col-form-label">Nama HR HOST</label>
                    <div class="col-sm-6">
                        <input name="nama_hr_host" id="nama_hr_host" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus HR HOST -->

<div class="modal fade bd-modal-hapus-hr-host" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus HR HOST</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus HR HOST ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit HR HOST -->

<div class="modal fade bd-modal-edit-hr-host" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit HR HOST</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_hr_host" class="col-sm-3 col-form-label">Kode HR HOST</label>
                    <div class="col-sm-6">
                        <input name="kode_hr_host" disabled="" id="kode_hr_host" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_hr_host" class="col-sm-3 col-form-label">Nama HR HOST</label>
                    <div class="col-sm-6">
                        <input name="nama_hr_host" id="nama_hr_host" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit TPK -->

<div class="modal fade bd-modal-edit-tpk" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit TPK</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_tpk" class="col-sm-3 col-form-label">Kode TPK</label>
                    <div class="col-sm-6">
                        <input name="kode_tpk" disabled="" id="kode_tpk" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_tpk" class="col-sm-3 col-form-label">Nama TPK</label>
                    <div class="col-sm-6">
                        <input name="nama_tpk" disabled="" id="nama_tpk" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="area" class="col-sm-3 col-form-label">Area</label>
                    <div class="col-sm-6">
                        <input name="area" disabled="" id="area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="status_tpk" class="col-sm-3 col-form-label">Status TPK</label>
                    <div class="col-sm-6">
                        <select name="status_tpk" disabled="" id="status_tpk" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="alamat_tpk" class="col-sm-3 col-form-label">Alamat TPK</label>
                    <div class="col-sm-6">
                        <textarea name="alamat_tpk" disabled="" id="alamat_tpk" placeholder="" class="form-control"></textarea>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kota_tpk" class="col-sm-3 col-form-label">Kota TPK</label>
                    <div class="col-sm-6">
                        <input name="kota_tpk" disabled="" id="kota_tpk" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nomor_telepon" class="col-sm-3 col-form-label">Nomor Telepon</label>
                    <div class="col-sm-6">
                        <input name="nomor_telepon" disabled="" id="nomor_telepon" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="singkatan_tpk" class="col-sm-3 col-form-label">Singkatan TPK</label>
                    <div class="col-sm-6">
                        <input name="singkatan_tpk" disabled="" id="singkatan_tpk" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="lat_long" class="col-sm-3 col-form-label">Latitude Longitude</label>
                    <div class="col-sm-3">
                        <input name="lat_long" id="lat_long" placeholder="" class="form-control">
                    </div>
                    <div class="col-sm-3">
                        <input name="lat_long" id="lat_long" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah provinsi -->

<div class="modal fade bd-modal-tambah-provinsi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Provinsi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_provinsi" class="col-sm-3 col-form-label">Kode Provinsi</label>
                    <div class="col-sm-6">
                        <input name="kode_provinsi" id="kode_provinsi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_provinsi" class="col-sm-3 col-form-label">Nama Provinsi</label>
                    <div class="col-sm-6">
                        <input name="nama_provinsi" id="nama_provinsi" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus provinsi -->

<div class="modal fade bd-modal-hapus-provinsi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Provinsi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus provinsi ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit provinsi -->

<div class="modal fade bd-modal-edit-provinsi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Provinsi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_provinsi" class="col-sm-3 col-form-label">Kode Provinsi</label>
                    <div class="col-sm-6">
                        <input name="kode_provinsi" disabled="" id="kode_provinsi" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_provinsi" class="col-sm-3 col-form-label">Nama Provinsi</label>
                    <div class="col-sm-6">
                        <input name="nama_provinsi" id="nama_provinsi" placeholder="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah kabupaten -->

<div class="modal fade bd-modal-tambah-kabupaten" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Kabupaten</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_kabupaten" class="col-sm-3 col-form-label">Kode Kabupaten</label>
                    <div class="col-sm-6">
                        <input name="kode_kabupaten" id="kode_kabupaten" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kabupaten" class="col-sm-3 col-form-label">Nama Kabupaten</label>
                    <div class="col-sm-6">
                        <input name="nama_kabupaten" id="nama_kabupaten" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="provinsi" class="col-sm-3 col-form-label">Provinsi</label>
                    <div class="col-sm-6">
                        <select name="provinsi" id="provinsi" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus kabupaten -->

<div class="modal fade bd-modal-hapus-kabupaten" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Kabupaten</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus kabupaten ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit kabupaten -->

<div class="modal fade bd-modal-edit-kabupaten" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Kabupaten</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_kabupaten" class="col-sm-3 col-form-label">Kode Kabupaten</label>
                    <div class="col-sm-6">
                        <input name="kode_kabupaten" disabled="" id="kode_kabupaten" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kabupaten" class="col-sm-3 col-form-label">Nama Kabupaten</label>
                    <div class="col-sm-6">
                        <input name="nama_kabupaten" id="nama_kabupaten" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="provinsi" class="col-sm-3 col-form-label">Provinsi</label>
                    <div class="col-sm-6">
                        <select name="provinsi" id="provinsi" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal tambah kecamatan -->

<div class="modal fade bd-modal-tambah-kecamatan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Tambah Kecamatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_kecamatan" class="col-sm-3 col-form-label">Kode Kecamatan</label>
                    <div class="col-sm-6">
                        <input name="kode_kecamatan" id="kode_kecamatan" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kecamatan" class="col-sm-3 col-form-label">Nama Kecamatan</label>
                    <div class="col-sm-6">
                        <input name="nama_kecamatan" id="nama_kecamatan" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kabupaten" class="col-sm-3 col-form-label">Kabupaten</label>
                    <div class="col-sm-6">
                        <select name="kabupaten" id="kabupaten" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus kecamatan -->

<div class="modal fade bd-modal-hapus-kecamatan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Hapus Kecamatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin untuk menghapus kecamatan ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Hapus</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit kecamatan -->

<div class="modal fade bd-modal-edit-kecamatan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Kecamatan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_kecamatan" class="col-sm-3 col-form-label">Kode Kecamatan</label>
                    <div class="col-sm-6">
                        <input name="kode_kecamatan" disabled="" id="kode_kecamatan" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kecamatan" class="col-sm-3 col-form-label">Nama Kecamatan</label>
                    <div class="col-sm-6">
                        <input name="nama_kecamatan" id="nama_kecamatan" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="kabupaten" class="col-sm-3 col-form-label">Kabupaten</label>
                    <div class="col-sm-6">
                        <select name="kabupaten" id="kabupaten" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal cari nik -->

<div class="modal fade bd-modal-cari-nik" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Cari NIK (Kepala Keluarga)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="nik_cari" class="col-sm-3 col-form-label">NIK</label>
                    <div class="col-sm-6">
                        <input name="nik_cari" id="nik_cari" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_kepala_keluarga_cari" class="col-sm-3 col-form-label">Nama Kepala Keluarga</label>
                    <div class="col-sm-6">
                        <input name="nama_kepala_keluarga_cari" id="nama_kepala_keluarga_cari" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="area_cari_nik" class="col-sm-3 col-form-label">Area Pelayanan</label>
                    <div class="col-sm-6">
                        <select name="area_cari_nik" id="area_cari_nik" class="form-control">
                            <option value="" hidden="" selected>Pilih area</option>
                            <?php  
                                foreach ($area as $value) {
                                    echo "<option value=".$value->kode_area.">".$value->nama_area."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="tpk_cari_nik" class="col-sm-3 col-form-label">TPK</label>
                    <div class="col-sm-6">
                        <select name="tpk_cari_nik" id="tpk_cari_nik" class="form-control">
                            <option value="" hidden="" selected>Pilih TPK</option>            
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="jenis_peserta" class="col-sm-3 col-form-label">Jenis Peserta</label>
                    <div class="col-sm-6">
                        <select name="jenis_peserta" id="jenis_peserta" class="form-control">
                            <option value="" hidden="" selected>Pilih jenis peserta</option>
                            <?php  
                                foreach ($jenis_peserta as $value) {
                                    if ($value->kode_group_status_keluarga == 1) {
                                        echo "<option value=".$value->kode_jenis_peserta.">".$value->nama_jenis_peserta."</option>";
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-12" align="center">
                    <button class="mt-1 btn btn-primary">Cari Data Kepala Keluarga</button>
                </div>
                <div class="card-body">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <table class="mb-0 table table-striped">
                                <thead>
                                <tr>
                                    <th>NIK</th>
                                    <th>Nama KK</th>
                                    <th>Area</th>
                                    <th>Jenis Peserta</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>NIKES</td>
                                    <td>Nama Peserta</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>
                                        <button class="mt-1 btn btn-success" data-dismiss="modal">Pilih</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NIKES</td>
                                    <td>Nama Peserta</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>
                                        <button class="mt-1 btn btn-success" data-dismiss="modal">Pilih</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NIKES</td>
                                    <td>Nama Peserta</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>
                                        <button class="mt-1 btn btn-success" data-dismiss="modal">Pilih</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NIKES</td>
                                    <td>Nama Peserta</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>
                                        <button class="mt-1 btn btn-success" data-dismiss="modal">Pilih</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>NIKES</td>
                                    <td>Nama Peserta</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>
                                        <button class="mt-1 btn btn-success" data-dismiss="modal">Pilih</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal lihat dokumen -->

<div class="modal fade bd-modal-lihat-dokumen" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Area</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group"><label for="kode_area" class="col-sm-3 col-form-label">Nomor Dokumen</label>
                    <div class="col-sm-6">
                        <input name="kode_area" disabled="" id="kode_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="nama_area" class="col-sm-3 col-form-label">Perihal Dokumen</label>
                    <div class="col-sm-6">
                        <input name="nama_area" disabled="" id="nama_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group"><label for="manager_area" class="col-sm-3 col-form-label">Tanggal Dokumen</label>
                    <div class="col-sm-6">
                        <input name="manager_area" disabled="" id="manager_area" placeholder="" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <div class="col-sm-9">
                        <embed src="https://sumanbogati.github.io/tiny.pdf" width="600px" height="500px" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal mutasi TPK -->

<div class="modal fade bd-modal-mutasi-tpk" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Mutasi TPK</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Anda yakin akan melakukan mutasi TPK?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-danger">Mutasi</button>
            </div>
        </div>
    </div>
</div>