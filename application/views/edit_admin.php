<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-edit icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Edit Admin
                <div class="page-title-subheading">Penyuntingan data admin SIKEP.
                </div>
            </div>
        </div>
        <div class="page-title-actions">
        </div>    
    </div>
</div>
<?php if ($this->session->kode_admin != 1) { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Anda tidak memiliki akses ke halaman ini.</strong>
        </div>
    </div>  
</div>
<?php } else{ ?>
    

<form action="<?php echo base_url('AdminController/editAdminProses/'.$admin->row()->kode_admin) ?>" method="POST" class="needs-validation" novalidate>
    <div class="tab-content">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative row form-group"><label for="nama_admin" class="col-sm-2 col-form-label">Nama Admin</label>
                            <div class="col-sm-8">
                                <input type="text" name="nama_admin" id="nama_admin" class="form-control" value="<?php echo $admin->row()->nama_admin ?>" required>
                                <div class="invalid-feedback">
                                    Nama Admin tidak boleh kosong.
                                </div>
                            </div>
                        </div>
                        <div class="position-relative row form-group"><label for="username" class="col-sm-2 col-form-label">Username</label>
                            <div class="col-sm-8">
                                <input type="text" name="username_show" id="username_show" class="form-control" value="<?php echo $admin->row()->username ?>" disabled>
                                <input type="text" name="username" id="username" class="form-control" value="<?php echo $admin->row()->username ?>" hidden>
                            </div>
                        </div>
                        <div class="position-relative row form-group"><label for="password" class="col-sm-2 col-form-label">Password</label>
                            <div class="col-sm-8">
                                <input type="password" name="password" id="password" class="form-control" value="<?php echo $admin->row()->password ?>" required>
                                <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                <div class="invalid-feedback">
                                    Password tidak boleh kosong.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="col-md-12" align="center">
                    <?php if ($admin->row()->kode_admin != 1): ?>
                    <input type="button" name="" class="btn btn-danger" value="Hapus Admin" onclick="window.location='<?php echo base_url('AdminController/hapusAdminProses/'.$admin->row()->kode_admin) ?>';">
                    <?php endif ?>      
                    <button class="mt-1 btn btn-primary">Simpan</button>      
                </div>
            </div>
        </div>
    </div>
</form>
<style type="text/css">
    .field-icon {
      float: right;
      margin-left: -25px;
      margin-top: -25px;
      position: relative;
      z-index: 2;
    }
</style>
<script type="text/javascript">
    $(".toggle-password").click(function() {

      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
</script>
<?php } ?>
