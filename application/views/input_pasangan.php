<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-add-user icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Input Pasangan
                <div class="page-title-subheading">Penambahan data pasangan baru.
                </div>
            </div>
        </div>
        <div class="page-title-actions">
        </div>    
    </div>
</div>
<?php  if ($this->session->input_fail == 1) { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>NIK belum tersedia!</strong> Silakan masukan NIK lain.
        </div>
    </div>
</div>


<?php $this->session->input_fail = 0; } ?>

<form action="<?php echo base_url('PesertaController/inputPasanganProses'); ?>" enctype="multipart/form-data" method="POST" class="needs-validation" novalidate>
    <div class="row">
        <div class="col-md-6">
            <div class="tab-content">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-12">
                                <h5 class="card-title">Upload Foto Pasangan</h5>
                                <div class="position-relative row form-group">
                                    <img class="col-sm-3 col-form-label" name="foto_kk_preview" id="image-preview" src="<?php echo base_url() . '../assets/images/user-placeholder.jpg'; ?>">
                                </div>
                                <div class="position-relative row form-group">

                                    <div class="col-sm-10">
                                        <?php echo form_open_multipart(base_url('PesertaController/inputPasangan'));?>
                                        <input type="file" name="foto_kk" id="image-source" accept=".jpg,.jpeg,.png,.bmp" onchange="previewImage();" class="form-control-file">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="tab-content">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-md-12">
                                <h5 class="card-title">Pilih Transaksi</h5>
                                <div class="position-relative form-group">
                                    <div class="position-relative row form-group"><label for="kelompok_transaksi_kk" class="col-sm-2 col-form-label">Kelompok Transaksi</label>
                                        <div class="col-sm-8">
                                            <select name="kelompok_transaksi_kk" id="kelompok_transaksi_kk" class="form-control" required>
                                                <option value="" selected>Pilih Kelompok Transaksi</option>
                                                <?php  
                                                    foreach ($kelompok_transaksi as $value) {
                                                        if ($value->kode_kelompok_transaksi == "01") {
                                                            echo "<option value=".$value->kode_kelompok_transaksi.">".$value->nama_kelompok_transaksi."</option>";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Kelompok Transaksi tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="jenis_peserta_kk" class="col-sm-2 col-form-label">Jenis Peserta</label>
                                        <div class="col-sm-8">
                                            <select name="jenis_peserta_kk" id="jenis_peserta_kk" class="form-control" required>
                                                <option value="" hidden="" selected>Pilih Jenis Peserta</option>
                                                <?php foreach ($jenis_peserta as $value): ?>
                                                <?php if ($value->kode_group_status_keluarga == "2"): ?>
                                                <option value="<?php echo $value->kode_group_jenis_peserta ?>"><?php echo $value->nama_jenis_peserta ?></option>
                                                <?php endif ?>
                                                <?php endforeach ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Jenis Peserta tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="position-relative row form-group"><label for="jenis_transaksi_pasangan" class="col-sm-2 col-form-label">Jenis Transaksi</label>
                                    <div class="col-sm-8">
                                        <select name="jenis_transaksi_pasangan" id="jenis_transaksi_pasangan" class="form-control" required>
                                            <option value="" hidden="" selected>Pilih Jenis Transaksi</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Jenis Transaksi tidak boleh kosong.
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-0" class="active nav-link">Data Pribadi</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-1" class="nav-link">Data Kepegawaian</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-2" class="nav-link">Info Lain-Lain</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg11-3" class="nav-link">Data Pendukung</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-eg11-0" role="tabpanel">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="nik" class="col-sm-2 col-form-label">NIK</label>
                                        <div class="col-sm-8">
                                            <!-- <input name="nik" id="nik" placeholder="" class="form-control" data-toggle="modal" data-target=".bd-modal-cari-nik"> -->
                                            <input name="nik" id="nik" placeholder="" class="form-control" required>

                                            <div class="invalid-feedback">
                                                NIK tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="nama" class="col-sm-2 col-form-label">Nama Pasangan</label>
                                        <div class="col-sm-8">
                                            <input name="nama" id="nama" placeholder="" class="form-control" required>
                                            <div class="invalid-feedback">
                                                Nama tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                                        <div class="col-sm-3">
                                            <input name="tempat_lahir" id="tempat_lahir" placeholder="" class="form-control" required>
                                            <div class="invalid-feedback">
                                                Tempat Lahir tidak boleh kosong.
                                            </div>
                                        </div>
                                        <label for="tgl_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                                        <div class="col-sm-3">

                                            <input name="tgl_lahir" autocomplete="off" id="datepicker_tgl_lahir" placeholder="dd/mm/yyyy" class="form-control" required>

                                            <div class="invalid-feedback">
                                                Tanggal Lahir tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                                        <div class="col-sm-2">
                                            <div class="position-relative form-check">
                                                <label class="form-check-label"><input name="jenis_kelamin" id="jenis_kelamin" value="Laki-laki" type="radio" class="form-check-input" checked>Laki-laki</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="position-relative form-check">
                                                <label class="form-check-label"><input name="jenis_kelamin" id="jenis_kelamin" value="Perempuan" type="radio" class="form-check-input">Perempuan</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="agama" class="col-sm-2 col-form-label">Agama</label>
                                        <div class="col-sm-8">
                                            <select name="agama" id="agama" class="form-control">
                                                <option value="" hidden="">Pilih Agama</option>
                                            <?php foreach ($agama as $value): ?>
                                                <option value="<?php echo $value->kode_agama; ?>"><?php echo $value->nama_agama; ?></option>
                                            <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="gol_darah" class="col-sm-2 col-form-label">Golongan Darah</label>
                                        <div class="col-sm-3">
                                            <select name="gol_darah" id="gol_darah" class="form-control">
                                                <option value="" hidden="" selected></option>
                                                <option value="A">A</option>
                                                <option value="A">B</option>
                                                <option value="A">AB</option>
                                                <option value="A">O</option>
                                            </select>
                                        </div>
                                        <label for="gol_darah" class="col-sm-2 col-form-label">Rhesus</label>
                                        <div class="col-sm-3">
                                            <select name="rhesus" id="rhesus" class="form-control">
                                                <option value="" hidden="" selected></option>
                                                <option value=1>+</option>
                                                <option value=0>-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">Nomor KTP</label>
                                        <div class="col-sm-8">
                                            <input name="no_ktp" id="no_ktp" placeholder="" class="form-control">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="pasangan_ke" class="col-sm-2 col-form-label">Pasangan Ke-</label>
                                        <div class="col-sm-8">
                                            <input type="number" name="pasangan_ke" id="pasangan_ke" placeholder="" class="form-control" min="1">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="no_bpjs" class="col-sm-2 col-form-label">Nomor BPJS</label>
                                        <div class="col-sm-8">
                                            <input name="no_bpjs" id="no_bpjs" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="area_pelayanan" class="col-sm-2 col-form-label">Area Pelayanan</label>
                                        <div class="col-sm-8">
                                            <select name="area_pelayanan" id="area_pelayanan" class="form-control" required> 
                                                <option value="" hidden="" selected>Pilih area</option>
                                                <?php  
                                                    foreach ($area as $value) {
                                                        if ($value->kode_area != 0) {
                                                            # code...
                                                            echo "<option value=".$value->kode_area.">".$value->nama_area."</option>";
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Area Pelayanan tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tpk" class="col-sm-2 col-form-label">TPK</label>
                                        <div class="col-sm-8">
                                            <select name="tpk" id="tpk" class="form-control" required>
                                                <option value="" hidden="" selected>Pilih TPK</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                TPK tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">Kota TPK</label>
                                        <div class="col-sm-8">
                                            <select name="kota_tpk" id="kota_tpk" disabled="" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">Alamat TPK</label>
                                        <div class="col-sm-8">
                                            <select name="alamat_tpk" id="alamat_tpk" disabled="" placeholder="" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="status_faskes" class="col-sm-2 col-form-label">Status Faskes</label>
                                        <div class="col-sm-2">
                                            <div class="position-relative form-check">
                                                <label class="form-check-label"><input name="status_faskes" id="status_faskes" value=1 type="radio" class="form-check-input" checked>Ya</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="position-relative form-check">
                                                <label class="form-check-label"><input name="status_faskes" id="status_faskes" value=0 type="radio" class="form-check-input">Tidak</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tgl_faskes" class="col-sm-2 col-form-label">Tanggal Faskes</label>
                                        <div class="col-sm-8">
                                            <input name="tgl_faskes" autocomplete="off" id="datepicker_tgl_faskes" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                    </div>
                    <div class="tab-pane" id="tab-eg11-1" role="tabpanel">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="nip_pasangan" class="col-sm-2 col-form-label">Nomor Induk Pegawai (NIP)</label>
                                        <div class="col-sm-8">
                                            <input name="nip_pasangan" id="nip_pasangan" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="pendidikan" class="col-sm-2 col-form-label">Pendidikan</label>
                                        <div class="col-sm-8">
                                            <select name="pendidikan" id="pendidikan" class="form-control" required>
                                                <option value="" hidden="" selected>Pilih pendidikan</option>
                                                <?php  
                                                    foreach ($pendidikan as $value) {
                                                        echo "<option value=".$value->kode_pendidikan.">".$value->nama_pendidikan."</option>";
                                                    }
                                                ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Pendidikan tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="position-relative row form-group"><label for="pekerjaan" class="col-sm-2 col-form-label">Pekerjaan</label>
                                        <div class="col-sm-8">
                                            <select name="pekerjaan" id="pekerjaan" class="form-control" required>
                                                <option value="" hidden="" selected>Pilih pekerjaan</option>
                                                <?php  
                                                    foreach ($pekerjaan as $value) {
                                                        echo "<option value=".$value->kode_pekerjaan.">".$value->nama_pekerjaan."</option>";
                                                    }
                                                ?>
                                            </select>
                                            <div class="invalid-feedback">
                                                Pendidikan tidak boleh kosong.
                                            </div>
                                        </div>
                                    </div>                   
                                </div>
                                <div class="col-md-6">
                                     

                                </div>
                            </div>
                    </div>
                    <div class="tab-pane" id="tab-eg11-2" role="tabpanel">
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="alamat_ktp" class="col-sm-2 col-form-label">Alamat KTP</label>
                                        <div class="col-sm-8">
                                            <textarea name="alamat_ktp" id="alamat_ktp" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="rt_ktp" class="col-sm-2 col-form-label">RT</label>
                                        <div class="col-sm-3">
                                            <input name="rt_ktp" id="rt_ktp" placeholder="" class="form-control">
                                        </div>
                                        <label for="rw_ktp" class="col-sm-2 col-form-label">RW</label>
                                        <div class="col-sm-3">
                                            <input name="rw_ktp" id="rw_ktp" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="provinsi_ktp" class="col-sm-2 col-form-label">Provinsi</label>
                                        <div class="col-sm-3">
                                            <select name="provinsi_ktp" id="provinsi_ktp" class="form-control">
                                                <option value="" hidden="" selected></option>
                                                <?php  
                                                    foreach ($provinsi as $value) {
                                                        echo "<option value=".$value->id.">".$value->name."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <label for="kabupaten_ktp" class="col-sm-2 col-form-label">Kota</label>
                                        <div class="col-sm-3">
                                            <select name="kabupaten_ktp" id="kabupaten_ktp" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="kecamatan_ktp" class="col-sm-2 col-form-label">Kecamatan</label>
                                        <div class="col-sm-3">
                                            <select name="kecamatan_ktp" id="kecamatan_ktp" class="form-control">
                                            </select>
                                        </div>
                                        <label for="kelurahan_ktp" class="col-sm-2 col-form-label">Kelurahan</label>
                                        <div class="col-sm-3">
                                            <select name="kelurahan_ktp" id="kelurahan_ktp" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="kode_pos_ktp" class="col-sm-2 col-form-label">Kode POS</label>
                                        <div class="col-sm-2">
                                            <input name="kode_pos_ktp" id="kode_pos_ktp" placeholder="" class="form-control">
                                        </div>
                                        <label for="no_telp_ktp" class="col-sm-2 col-form-label">Nomor Telepon</label>
                                        <div class="col-sm-4">
                                            <input name="no_telp_ktp" id="no_telp_ktp" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="alamat_domisili" class="col-sm-2 col-form-label">Alamat Domisili</label>
                                        <div class="col-sm-8">
                                            <textarea name="alamat_domisili" id="alamat_domisili" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="rt_domisili" class="col-sm-2 col-form-label">RT</label>
                                        <div class="col-sm-3">
                                            <input name="rt_domisili" id="rt_domisili" placeholder="" class="form-control">
                                        </div>
                                        <label for="rw_domisili" class="col-sm-2 col-form-label">RW</label>
                                        <div class="col-sm-3">
                                            <input name="rw_domisili" id="rw_domisili" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="provinsi_domisili" class="col-sm-2 col-form-label">Provinsi</label>
                                        <div class="col-sm-3">
                                            <select name="provinsi_domisili" id="provinsi_domisili" class="form-control">
                                                <option value="" hidden="" selected></option>
                                                <?php  
                                                    foreach ($provinsi as $value) {
                                                        echo "<option value='".$value->id."'>".$value->name."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <label for="kabupaten_domisili" class="col-sm-2 col-form-label">Kota</label>
                                        <div class="col-sm-3">
                                            <select name="kabupaten_domisili" id="kabupaten_domisili" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="kecamatan_domisili" class="col-sm-2 col-form-label">Kecamatan</label>
                                        <div class="col-sm-3">
                                            <select name="kecamatan_domisili" id="kecamatan_domisili" class="form-control">
                                            </select>
                                        </div>
                                        <label for="kelurahan_domisili" class="col-sm-2 col-form-label">Kelurahan</label>
                                        <div class="col-sm-3">
                                            <select name="kelurahan_domisili" id="kelurahan_domisili" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="kode_pos_domisili" class="col-sm-2 col-form-label">Kode POS</label>
                                        <div class="col-sm-2">
                                            <input name="kode_pos_domisili" id="kode_pos_domisili" placeholder="" class="form-control">
                                        </div>
                                        <label for="no_telp_domisili" class="col-sm-2 col-form-label">Nomor Telepon</label>
                                        <div class="col-sm-4">
                                            <input name="no_telp_domisili" id="no_telp_domisili" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" align="center">
                                    <div class="col-sm-10">
                                        <div class="position-relative form-check"><label class="form-check-label">
                                            <input name="ktp_domisili" id="ktp_domisili" type="checkbox" onclick="FillBilling(this.form)" class="form-check-input"> Alamat Domisili sama dengan Alamat KTP</label>
                                        </div>
                                    </div>
                                    <div class="divider"></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="no_hp" class="col-sm-2 col-form-label">Nomor HP</label>
                                        <div class="col-sm-8">
                                            <input name="no_hp" id="no_hp" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="nama_pemilik_rekening" class="col-sm-2 col-form-label">Nama Pemilik Rekening</label>
                                        <div class="col-sm-8">
                                            <input name="nama_pemilik_rekening" id="nama_pemilik_rekening" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="nama_bank" class="col-sm-2 col-form-label">Nama Bank</label>
                                        <div class="col-sm-8">
                                            <select name="nama_bank" id="nama_bank" class="form-control">
                                                <option value="" hidden="" selected>Pilih bank</option>
                                                <?php  
                                                    foreach ($bank as $value) {
                                                        echo "<option value=".$value->kode_bank.">".$value->nama_bank."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="no_rekening_bank" class="col-sm-2 col-form-label">Nomor Rekening Bank</label>
                                        <div class="col-sm-4">
                                            <input name="no_rekening_bank" id="no_rekening_bank" placeholder="" class="form-control">
                                        </div>
                                        <label for="status_kartu" class="col-sm-1.5 col-form-label">Status Kartu</label>
                                        <div class="col-sm-3">
                                            <select name="status_kartu" id="status_kartu" class="form-control">
                                                <option value="" hidden="" selected>Pilih status</option>
                                                <option value="Belum Cetak">Belum Cetak</option>
                                                <option value="Expired">Expired</option>
                                                <option value="Sudah Cetak">Sudah Cetak</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tgl_cetak" class="col-sm-2 col-form-label">Tanggal Cetak</label>
                                        <div class="col-sm-3">
                                            <input name="tgl_cetak" autocomplete="off" id="datepicker_tgl_cetak" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                        <label for="tgl_akhir_kartu" class="col-sm-2 col-form-label">Tanggal Akhir Kartu</label>
                                        <div class="col-sm-3">
                                            <input name="tgl_akhir_kartu" autocomplete="off" id="datepicker_tgl_akhir_kartu" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative row form-group"><label for="status_pernikahan" class="col-sm-2 col-form-label">Status Pernikahan</label>
                                        <div class="col-sm-3">
                                            <select name="status_pernikahan" id="status_pernikahan" class="form-control">
                                                <option value="" hidden="" selected>Pilih status</option>
                                                <option value="Singe">Single</option>
                                                <option value="Menikah">Menikah</option>
                                                <option value="Janda">Janda</option>
                                                <option value="Duda">Duda</option>
                                            </select>
                                        </div>
                                        <label for="tgl_nikah" class="col-sm-2 col-form-label">Tanggal Pernikahan</label>
                                        <div class="col-sm-3">
                                            <input name="tgl_nikah" autocomplete="off" id="datepicker_tgl_nikah" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tgl_cerai" class="col-sm-2 col-form-label">Tanggal Perceraian</label>
                                        <div class="col-sm-8">
                                            <input name="tgl_cerai" autocomplete="off" id="datepicker_tgl_cerai" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="tgl_meninggal" class="col-sm-2 col-form-label">Tanggal Meninggal</label>
                                        <div class="col-sm-8">
                                            <input name="tgl_meninggal" autocomplete="off" id="datepicker_tgl_meninggal" placeholder="dd/mm/yyyy" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="alasan_berhenti_peserta" class="col-sm-2 col-form-label">Alasan Berhenti Peserta</label>
                                        <div class="col-sm-8">
                                            <input name="alasan_berhenti_peserta" id="alasan_berhenti_peserta" placeholder="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group"><label for="catatan" class="col-sm-2 col-form-label">Catatan</label>
                                        <div class="col-sm-8">
                                            <textarea name="catatan" id="catatan" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    
                    
                    
                    <div class="tab-pane" id="tab-eg11-3" role="tabpanel">
                            <h5 class="card-title">Upload Dokumen Pendukung</h5>
                            <div class="form-row">
                                <table class="mb-0 table table-borderless">
                                    <thead>
                                    <tr>
                                        <th>Nomor Dokumen</th>
                                        <th>Perihal</th>
                                        <th>Tanggal Dokumen</th>
                                        <th>File</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="no_dokumen_1" id="no_dokumen_1" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="perihal_1" id="perihal_1" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="tgl_dokumen_1" autocomplete="off" id="datepicker_tgl_dokumen_1" placeholder="dd/mm/yyyy" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>

                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                                    <input type="file" name="file_1" id="file_1" accept=".pdf" class="form-control-file">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="no_dokumen_2" id="no_dokumen_2" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="perihal_2" id="perihal_2" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="tgl_dokumen_2" autocomplete="off" id="datepicker_tgl_dokumen_2" placeholder="dd/mm/yyyy" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>

                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                                    <input type="file" name="file_2" id="file_2" accept=".pdf" class="form-control-file">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="no_dokumen_3" id="no_dokumen_3" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="perihal_3" id="perihal_3" placeholder="" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <input name="tgl_dokumen_3" autocomplete="off" id="datepicker_tgl_dokumen_3" placeholder="dd/mm/yyyy" class="form-control">
                                                </div>
                                            </div>
                                        </td>
                                        <td>

                                            <div class="position-relative row form-group">
                                                <div class="col-sm-12">
                                                    <?php echo form_open_multipart(base_url('PesertaController/inputKepalaKeluarga'));?>
                                                    <input type="file" name="file_3" id="file_3" accept=".pdf" class="form-control-file">
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="col-md-12" align="center">
                    <button class="mt-1 btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>