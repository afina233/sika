<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-users icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Data Kepesertaan
                <div class="page-title-subheading">Gunakan filter untuk memperoleh data peserta.
                </div>
            </div>
        </div>
        <div class="page-title-actions">
        </div>    
    </div>
</div>
<?php if ($this->session->cari_kepesertaan == 1){ ?>
<script type="text/javascript">
    window.onload = function() {

        var area_pelayanan = localStorage.getItem("area_pelayanan");
        if (area_pelayanan !== null) $('#area_pelayanan').val(area_pelayanan);

        var tpk_kepesertaan = localStorage.getItem("tpk_kepesertaan");
        if (tpk_kepesertaan !== null) $('#tpk_kepesertaan').val(tpk_kepesertaan);

        var group_status_keluarga = localStorage.getItem("group_status_keluarga");
        if (group_status_keluarga !== null) $('#group_status_keluarga').val(group_status_keluarga);

        var group_jenis_peserta = localStorage.getItem("group_jenis_peserta");
        if (group_jenis_peserta !== null) $('#group_jenis_peserta').val(group_jenis_peserta);
    }
</script>
<?php } ?>

<form action="<?php echo base_url('PesertaController/dataKepesertaan') ?>" method="POST">
    <div class="tab-content">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative row form-group"><label for="area_pelayanan" class="col-sm-2 col-form-label">Area Pelayanan</label>
                            <div class="col-sm-8">
                                <select name="area_pelayanan" id="area_pelayanan" class="form-control">
                                    <option value="" hidden="">Pilih Area Pelayanan</option>
                                    <?php foreach ($area_pelayanan as $value): ?>
                                    <option value="<?php echo $value->kode_area ?>"><?php echo $value->nama_area ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="position-relative row form-group"><label for="tpk_kepesertaan" class="col-sm-2 col-form-label">TPK</label>
                            <div class="col-sm-8">
                                <select name="tpk_kepesertaan" id="tpk_kepesertaan" class="form-control">
                                    <option value="" hidden="">Pilih TPK</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative row form-group"><label for="group_status_keluarga" class="col-sm-2 col-form-label">Group Peserta</label>
                            <div class="col-sm-8">
                                <select name="group_status_keluarga" id="group_status_keluarga" class="form-control">
                                    <option value="" hidden="">Pilih Group Peserta</option>
                                    <?php foreach ($group_status_keluarga as $value): ?>
                                    <option value="<?php echo $value->kode_group_status_keluarga ?>"><?php echo $value->nama_group_status_keluarga ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="position-relative row form-group"><label for="group_jenis_peserta" class="col-sm-2 col-form-label">Jenis Peserta</label>
                            <div class="col-sm-8">
                                <select name="group_jenis_peserta" id="group_jenis_peserta" class="form-control">
                                    <option value="" hidden="">Pilih Jenis Peserta</option>
                                    <?php foreach ($group_jenis_peserta as $value): ?>
                                    <option value="<?php echo $value->kode_group_jenis_peserta ?>"><?php echo $value->nama_group_jenis_peserta ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="col-md-12" align="center">
                    <button class="mt-1 btn btn-primary">Cari Data Kepesertaan</button>
                </div>
            </div>
        </div>
    </div>
    
</form>
<div class="tab-content">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <table id="cari_kepesertaan_tabel" class="mb-0 table table-striped">
                <thead>
                <tr>
                    <th>NIKES</th>
                    <th>Nama Peserta</th>
                    <th>NIK</th>
                    <th>Kepala Keluarga</th>
                    <th>Jenis Peserta</th>
                    <th>HR</th>
                    <th>Jenis Kelamin</th>
                    <th>Nomor HP</th>
                </tr>
                </thead>
                <tbody>
                    <?php if ($this->session->cari_kepesertaan == 1) {
                    foreach ($peserta as $value) { ?>
                    <tr>
                        <td>
                            <?php if ($value->kode_group_status_keluarga == 1): ?>
                                <a href="<?php echo base_url('Sika/peserta/'.$value->kode_peserta) ?>">
                                    <?php echo $value->nikes ?>
                                </a>
                            <?php elseif ($value->kode_group_status_keluarga == 2): ?>
                                <a href="<?php echo base_url('Sika/peserta_pasangan/'.$value->kode_peserta) ?>">
                                    <?php echo $value->nikes ?>
                                </a>
                            <?php else: ?>
                                <a href="<?php echo base_url('Sika/peserta_anak/'.$value->kode_peserta) ?>">
                                    <?php echo $value->nikes ?>
                                </a>
                            <?php endif ?> 
                        </td>
                        <td>
                            <?php if ($value->kode_group_status_keluarga == 1): ?>
                                <a href="<?php echo base_url('Sika/peserta/'.$value->kode_peserta) ?>">
                                    <?php echo $value->nama ?>
                                </a>
                            <?php elseif ($value->kode_group_status_keluarga == 2): ?>
                                <a href="<?php echo base_url('Sika/peserta_pasangan/'.$value->kode_peserta) ?>">
                                    <?php echo $value->nama ?>
                                </a>
                            <?php else: ?>
                                <a href="<?php echo base_url('Sika/peserta_anak/'.$value->kode_peserta) ?>">
                                    <?php echo $value->nama ?>
                                </a>
                            <?php endif ?> 
                        </td>
                        <td><?php echo $value->nik ?></td>
                        <td>
                            <?php foreach ($peserta_kk as $value2): ?>
                            <?php if ($value2->kode_nik == $value->kode_nik): ?>
                            <?php echo $value2->nama ?>
                            <?php endif ?>
                            <?php endforeach ?>
                        </td>
                        <td><?php echo $value->nama_jenis_peserta ?></td>
                        <td>
                            <?php foreach ($personal_sub_area as $value2): ?>
                            <?php if ($value2->kode == $value->kode_personal_sub_area): ?>
                            <?php echo $value2->kode_personal_area ?>
                            <?php endif ?>
                            <?php endforeach ?>
                        </td>
                        <td><?php echo $value->jenis_kelamin ?></td>
                        <td><?php echo $value->no_hp ?></td>
                    </tr>
                    <?php
                    }
                    } $this->session->cari_kepesertaan = 0; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>