<!DOCTYPE html>
<html lang="en">
<head>
	<title>SIKEP</title>
    <link rel="icon" type="image/gif" href="<?php echo base_url('../assets/images/icon.png') ?>" />
	
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css'; ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css'; ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/vendor/animate/animate.css'; ?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/vendor/css-hamburgers/hamburgers.min.css'; ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/vendor/animsition/css/animsition.min.css'; ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/vendor/select2/select2.min.css'; ?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/vendor/daterangepicker/daterangepicker.css'; ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/css/util.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().'../assets/login/css/main.css'; ?>">
<!--===============================================================================================-->
    <link href="<?php echo base_url().'../main.css'; ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url().'../assets/scripts/main.js'; ?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'../assets/scripts/jquery-3.3.1.js'; ?>"></script>

</head>

<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
				<h3 align="center">Halaman Tidak Ditemukan</h3>
			</div>
		</div>
	</div>
	
	
<!--===============================================================================================-->
	<!-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/vendor/jquery/jquery-3.2.1.min.js'; ?>"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="<?php echo base_url().'../assets/login/vendor/animsition/js/animsition.min.js'; ?>"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url().'../assets/login/vendor/bootstrap/js/popper.js'; ?>"></script>
	<!-- <script src="vendor/bootstrap/js/bootstrap.min.js"></script> -->
<!--===============================================================================================-->
	<!-- <script src="vendor/select2/select2.min.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/vendor/select2/select2.min.js'; ?>"></script>
<!--===============================================================================================-->
	<!-- <script src="vendor/daterangepicker/moment.min.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/vendor/daterangepicker/moment.min.js'; ?>"></script>
	<!-- <script src="vendor/daterangepicker/daterangepicker.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/vendor/daterangepicker/daterangepicker.js'; ?>"></script>
<!--===============================================================================================-->
	<!-- <script src="vendor/countdowntime/countdowntime.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/vendor/countdowntime/countdowntime.js'; ?>"></script>
<!--===============================================================================================-->
	<!-- <script src="js/main.js"></script> -->
	<script src="<?php echo base_url().'../assets/login/js/main.js'; ?>"></script>

</body>
</html>