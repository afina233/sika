<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-lock icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Data Admin
                <div class="page-title-subheading">Daftar seluruh admin SIKEP.
                </div>
            </div>
        </div>
        <?php if ($this->session->kode_admin == 1): ?>
        <div class="page-title-actions">
            <div class="d-inline-block dropdown">
                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow btn btn-primary" onclick="window.location.href = '<?php echo base_url('AdminController/inputAdmin') ?>';">
                    <span class="btn-icon-wrapper pr-2 opacity-7">
                        <i class="fa fa-plus fa-w-20"></i>
                    </span>
                    Tambah Admin Baru
                </button>
            </div>
        </div>         
        <?php endif ?>
    </div>
</div>
<?php if ($this->session->edit_admin_berhasil != "") { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Admin <?php echo $this->session->edit_admin_berhasil ?> berhasil di-edit!</strong>
        </div>
    </div>  
</div>
<?php } else if ($this->session->input_admin_berhasil != "") { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Admin <?php echo $this->session->input_admin_berhasil ?> berhasil di-input!</strong>
        </div>
    </div>  
</div>
<?php } $this->session->input_admin_berhasil = ""; $this->session->edit_admin_berhasil = ""; ?>
<?php if ($this->session->hapus_admin_berhasil != "") { ?>
<div class="tab-content">
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Admin <?php echo $this->session->hapus_admin_berhasil ?> berhasil dihapus!</strong>
        </div>
    </div>  
</div>
<?php } $this->session->hapus_admin_berhasil = ""; ?>

<div class="tab-content">
    <div class="main-card mb-3 card">
        <div class="card-body">
            <table id="data_admin" class="mb-0 table table-striped">
                <thead>
                <tr>
                    <th>Nomor</th>
                    <th>Nama Admin</th>
                    <th>Username</th>
                    <th>Transaksi Terakhir</th>
                </tr>
                </thead>
                <tbody>
                <?php $no = 0; ?>
                <?php foreach ($admin as $value): ?>
                <?php $no = $no + 1; ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td>
                        <?php if ($this->session->kode_admin == 1): ?>
                        <a href="<?php echo base_url('AdminController/editAdmin/'.$value->kode_admin) ?>">
                            <?php echo $value->nama_admin ?>
                        </a>
                        <?php else: ?>
                            <?php echo $value->nama_admin ?>
                        <?php endif ?>
                    </td>
                    <td>
                        <?php if ($this->session->kode_admin == 1): ?>
                        <a href="<?php echo base_url('AdminController/editAdmin/'.$value->kode_admin) ?>">
                            <?php echo $value->username ?>
                        </a>
                        <?php else: ?>
                            <?php echo $value->username ?>
                        <?php endif ?>
                    </td>
                    <td>
                        <?php $count = 0; ?>
                        <?php foreach ($transaksi as $value2): ?>
                            <?php if ($count == 0 ): ?>
                                <?php if ($value2->kode_admin == $value->kode_admin): ?>
                                    <?php echo $value2->waktu ?>
                                    <?php $count = 1; ?>
                                <?php endif ?>
                            <?php endif ?>
                        <?php endforeach ?>
                    </td>
                </tr>
                <?php endforeach ?>
               
                </tbody>
            </table>
        </div>
    </div>
</div>

