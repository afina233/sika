<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="pe-7s-graph2 icon-gradient bg-mean-fruit">
                </i>
            </div>
            <div>Dashboard SIKEP
                <div class="page-title-subheading">Data peserta YAKES Telkom dalam grafik.
                </div>
            </div>
        </div>
        <div class="page-title-actions">
        </div>   
    </div>
</div>            

<div class="row">
    <div class="col-md-6">
        <div class="card mb-3 widget-content">
            <div class="widget-content-wrapper">
<!--                 <div class="widget-content-left">
                    <div class="widget-heading">Area Pelayanan</div>
                    <div class="widget-subheading">Pilih area untuk melihat data peserta dalam grafik.</div>

                </div>
                <div class="widget-content-right">
                    <select name="area_pelayanan_dashboard" id="area_pelayanan_dashboard" class="form-control">
                        <?php foreach ($area_pelayanan as $value): ?>
                        <option value="<?php echo $value->kode_area ?>"><?php echo $value->nama_area ?></option>
                        <?php endforeach ?>
                    </select>
                </div> -->
                <div class="widget-content-left">
                    <div class="widget-heading">Jumlah Seluruh Peserta YAKES</div>
                    <div class="widget-subheading">Termasuk peserta yang berhak dan tidak berhak faskes</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-success"><span><?php echo $jumlah_peserta_faskes ?></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card mb-3 widget-content">
            <div class="widget-content-wrapper">
                <div class="widget-content-left">
                    <div class="widget-heading">Jumlah Peserta Berhak Faskes</div>
                    <div class="widget-subheading">Peserta berhak untuk memperoleh layanan faskes</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-success"><span><?php echo $jumlah_peserta_faskes ?></span></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Grafik Jumlah Peserta Per Group</h5>
                <canvas id="myChart"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Grafik Jumlah Peserta Per TPK</h5>
                <canvas id="oilChart"></canvas>
            </div>
        </div>
    </div>
</div>
<script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Kepala Keluarga", "Pasangan", "Anak"],
            datasets: [{
                label: 'Jumlah Peserta',
                data: [
                <?php echo $jumlah_peserta_kk ?>, 
                <?php echo $jumlah_peserta_pasangan ?>, 
                <?php echo $jumlah_peserta_anak; ?>
                ],
                backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
</script>
<script>
    var oilCanvas = document.getElementById("oilChart");

    var oilData = {
        labels: [
            "TPKU",
            "TPKK",
            "Non TPK"
        ],
        datasets: [
            {
                data: [
                <?php echo $jumlah_tpku ?>, 
                <?php echo $jumlah_tpkk ?>, 
                <?php echo $jumlah_non_tpk ?>
                ],
                backgroundColor: [
                    "#FF6384",
                    "#63FF84",
                    "#8463FF"
                ]
            }]
    };

    var pieChart = new Chart(oilCanvas, {
      type: 'pie',
      data: oilData
    });

    

        


</script>