<?php  

defined('BASEPATH') OR exit('No direct script access allowed');

class TransaksiController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url'));

		$this->load->model('Transaksi');
		$this->load->model('Peserta');
		$this->load->model('Admin');

		
	}

	public function rekapitulasiTransaksi()
	{
		if ($this->session->kode_admin) {
			$this->session->page = "rekapitulasi_transaksi";
			$this->session->cari_rekapitulasi_transaksi = 0;

			$data['transaksi'] = null;
			if (!empty($_POST)) {
				$data['kode_kelompok_transaksi'] = $this->input->post('kelompok_transaksi');
				$data['kode_group_jenis_peserta'] = $this->input->post('group_jenis_peserta');
				$data['kode_jenis_transaksi'] = $this->input->post('jenis_transaksi_rekap');

				$data['periode_1'] = date('00-00-00 00:00:00');
				if (!empty($this->input->post('periode_1'))) {
					$date = str_replace('/', '-', $this->input->post('periode_1'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['periode_1'] = $newDate;
				}

				$data['periode_2'] = date('Y-m-d H:i:s');
				if (!empty($this->input->post('periode_2'))) {
					$date = str_replace('/', '-', $this->input->post('periode_2'));
					$newDate = date('Y-m-d', strtotime($date . ' +1 day'));
					$data['periode_2'] = $newDate;
				}

				$transaksi = $this->Transaksi->getTransaksiRekap($data);
				$data['transaksi'] = $transaksi;
				
				$this->session->cari_rekapitulasi_transaksi = 1;

			}


			$data['kelompok_transaksi'] = $this->Transaksi->getAllKelompokTransaksi();
			$data['group_jenis_peserta'] = $this->Peserta->getAllGroupJenisPeserta();

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('rekapitulasi_transaksi', $data);
			$this->load->view('footer');
		} else {
			$this->load->view('error');
		}
	}

	public function getJenisTransaksi(){
		if ($this->session->kode_admin) {
			$kode_kelompok_transaksi = $this->input->post('id', TRUE);
			$data = $this->Transaksi->getJenisTransaksi($kode_kelompok_transaksi);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getJenisTransaksiKKEdit(){
		if ($this->session->kode_admin) {
			$kode_kelompok_transaksi = $this->input->post('id', TRUE);
			$kode_group_jenis_peserta = $this->input->post('id_2', TRUE);
			$data = $this->Transaksi->getJenisTransaksiKKEdit($kode_kelompok_transaksi, $kode_group_jenis_peserta);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getJenisTransaksiRekap(){
		if ($this->session->kode_admin) {
			$kode_group_jenis_peserta = $this->input->post('id', TRUE);
			$kode_kelompok_transaksi = $this->input->post('id_2', TRUE);
			$data = $this->Transaksi->getJenisTransaksiRekap($kode_kelompok_transaksi, $kode_group_jenis_peserta);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getJenisTransaksiByJenisPeserta(){
		if ($this->session->kode_admin) {
			$kode_jenis_peserta = $this->input->post('id', TRUE);
			$data = $this->Transaksi->getJenisTransaksiByJenisPeserta($kode_jenis_peserta);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

}

?>