<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sikep extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));

		$this->load->model('TPK');
		$this->load->model('Area');
		$this->load->model('Peserta');
		$this->load->model('Instansi');
		$this->load->model('Provinsi');
		$this->load->model('Transaksi');
		$this->load->model('Admin');
	}

	public function index()
	{
		if ($this->session->admin == 0) {
			redirect(base_url('AdminController/login'));
			
		}else{
			$this->dashboard();
		}
	}

	public function dashboard(){
		if ($this->session->kode_admin) {
			$this->session->page = "dashboard";


			$data['area_pelayanan'] = $this->Area->getAllArea();
			$peserta_faskes = $this->Peserta->getAllPesertaFaskes();
			$data['jumlah_peserta_faskes'] = count($peserta_faskes);

			$peserta_kk = $this->Peserta->getPesertaKK();
			$data['jumlah_peserta_kk'] = count($peserta_kk);

			$peserta_pasangan = $this->Peserta->getPesertaPasangan();
			$data['jumlah_peserta_pasangan'] = count($peserta_pasangan);

			$peserta_anak = $this->Peserta->getPesertaAnak();
			$data['jumlah_peserta_anak'] = count($peserta_anak);

			$peserta_janda_duda = $this->Peserta->getPesertaJandaDuda();
			$data['jumlah_peserta_janda_duda'] = count($peserta_janda_duda);

			$tpkk = $this->TPK->getTPKK();
			$data['jumlah_tpkk'] = count($tpkk);

			$tpku = $this->TPK->getTPKU();
			$data['jumlah_tpku'] = count($tpku);

			$non_tpk = $this->TPK->getNonTPK();
			$data['jumlah_non_tpk'] = count($non_tpk);
				

			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('dashboard', $data);
			$this->load->view('footer');
		}else{
			$this->load->view('error');
		}
		
			

	}
}

?>