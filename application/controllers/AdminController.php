<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));

		$this->load->model('Admin');
		$this->load->model('Transaksi');
	}

	public function login(){
		$this->load->view('login');
	}

	public function loginAdmin(){
		$this->session->page = "login";

		$data['username'] = $this->input->post('username');
		$data['password'] = $this->input->post('password');

		$admin = $this->Admin->getAdmin($data);

		if ($admin->num_rows() > 0) {
			$this->session->admin = 1;
			$this->session->kode_admin = $admin->row()->kode_admin;
			redirect(base_url());

		}else{
			$this->session->admin = 0;;
			$this->session->gagal_login = 1;;
			redirect(base_url());

		}
	}

	public function logoutAdmin(){
		if ($this->session->kode_admin) {
			$this->session->admin = 0;
			$this->session->kode_admin = 0;

			redirect(base_url());
		}else{
			$this->load->view('error');
		}


	}

	

	public function dataAdmin()
	{
		if ($this->session->kode_admin) {
			$this->session->page = "data_admin";

			$data['admin'] = $this->Admin->getAllAdmin();
			$data['transaksi'] = $this->Transaksi->getAllTransaksi();

			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('data_admin', $data);
			$this->load->view('footer');
		} else {
			$this->load->view('error');
		}
	}

	public function editAdmin($kode_admin){
		if ($this->session->kode_admin) {
			$this->session->page = "edit_admin";
			if ($this->session->kode_admin == 1) {
				$admin = $this->Admin->getAdminById($this->session->kode_admin);
				$header['nama_admin'] = $admin->row()->nama_admin;
				$header['username'] = $admin->row()->username;

				$data['admin'] = $this->Admin->getAdminById($kode_admin);

				$this->load->view('header', $header);
				$this->load->view('sidebar');
				$this->load->view('edit_admin', $data);
				$this->load->view('footer');
			}else{

				$admin = $this->Admin->getAdminById($this->session->kode_admin);
				$header['nama_admin'] = $admin->row()->nama_admin;
				$header['username'] = $admin->row()->username;


				$this->load->view('header', $header);
				$this->load->view('sidebar');
				$this->load->view('error_admin');
				$this->load->view('footer');
			}
		} else {
			$this->load->view('error');
		}
		
	}

	public function editAdminProses($kode_admin){
		$this->session->page = "edit_admin";

		if ($this->session->kode_admin) {
			if ($this->session->kode_admin == 1) {
				$data['kode_admin'] = $kode_admin;
				$data['nama_admin'] = $this->input->post('nama_admin');
				$data['username'] = $this->input->post('username');
				$data['password'] = $this->input->post('password');

				$this->Admin->editAdmin($data);
				$this->session->edit_admin_berhasil = $data['username'];
				redirect(base_url('AdminController/dataAdmin'));
			}else{
				$admin = $this->Admin->getAdminById($this->session->kode_admin);
				$header['nama_admin'] = $admin->row()->nama_admin;
				$header['username'] = $admin->row()->username;


				$this->load->view('header', $header);
				$this->load->view('sidebar');
				$this->load->view('error_admin');
				$this->load->view('footer');
			}
		} else {
			$this->load->view('error');
		}



	}

	public function hapusAdminProses($kode_admin){
		if ($this->session->kode_admin) {
			if ($this->session->kode_admin == 1 && $kode_admin != 1) {
				
				$username = $this->Admin->getAdminById($kode_admin);
				$this->Admin->hapusAdmin($kode_admin);
				$this->session->hapus_admin_berhasil = $username->row()->username;
				redirect(base_url('AdminController/dataAdmin'));

				
			}else{
				$admin = $this->Admin->getAdminById($this->session->kode_admin);
				$header['nama_admin'] = $admin->row()->nama_admin;
				$header['username'] = $admin->row()->username;

				$this->load->view('header', $header);
				$this->load->view('sidebar');
				$this->load->view('error_admin');
				$this->load->view('footer');
			}
		} else {
			$this->load->view('error');
		}
	}

	public function inputAdmin(){
		if ($this->session->kode_admin) {
			$this->session->page = "input_admin";
			if ($this->session->kode_admin == 1) {

				$admin = $this->Admin->getAdminById($this->session->kode_admin);
				$header['nama_admin'] = $admin->row()->nama_admin;
				$header['username'] = $admin->row()->username;


				$this->load->view('header', $header);
				$this->load->view('sidebar');
				$this->load->view('input_admin');
				$this->load->view('footer');

			} else {
				$admin = $this->Admin->getAdminById($this->session->kode_admin);
				$header['nama_admin'] = $admin->row()->nama_admin;
				$header['username'] = $admin->row()->username;


				$this->load->view('header', $header);
				$this->load->view('sidebar');
				$this->load->view('error_admin');
				$this->load->view('footer');
			}
		} else {
			$this->load->view('error');
		}
	}

	public function inputAdminProses(){
		if ($this->session->kode_admin) {
			$this->session->page = "input_admin";
			if ($this->session->kode_admin == 1) {

				$data['nama_admin'] = $this->input->post('nama_admin');
				$data['username'] = $this->input->post('username');
				$data['password'] = $this->input->post('password');

				$admin = $this->Admin->getAdminByUsername($data['username']);
				if ($admin->num_rows() == 0) {
					// proses input
					$this->session->input_admin_berhasil =$data['username'];
					$this->Admin->inputAdmin($data);
					redirect(base_url('AdminController/dataAdmin'));
				} else {
					// ada admin dgn username yg sama
					$this->session->input_admin_gagal =$data['username'];
					redirect(base_url('AdminController/inputAdmin'));
				}
			} else {
				$admin = $this->Admin->getAdminById($this->session->kode_admin);
				$header['nama_admin'] = $admin->row()->nama_admin;
				$header['username'] = $admin->row()->username;


				$this->load->view('header', $header);
				$this->load->view('sidebar');
				$this->load->view('error_admin');
				$this->load->view('footer');
			}
		} else {
			$this->load->view('error');
		}

	}

}

?>