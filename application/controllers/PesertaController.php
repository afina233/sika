<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PesertaController extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url'));

		$this->load->model('Peserta');
		$this->load->model('Transaksi');
		$this->load->model('Area');
		$this->load->model('Admin');
		$this->load->model('TPK');
		$this->load->model('Instansi');
		$this->load->model('Provinsi');
		
	}

	public function editPeserta()
	{
		if ($this->session->kode_admin) {
			$this->session->page = "edit_peserta";
			$this->session->cari = 0;
			$this->session->cari_simpan = 0;

			$data['jenis_peserta'] = $this->Peserta->getAllJenisPeserta();
			$data['area_pelayanan'] = $this->Area->getAllArea();


			$data['peserta'] = null;
			if (!empty($_POST)) {

				$data['nik'] = $this->input->post('nik');
				$data['jenis_peserta'] = $this->input->post('jenis_kepesertaan');
				$data['no_peserta'] = $this->input->post('no_peserta');
				
				// tabel data pribadi
				$data['nama'] = $this->input->post('nama');
				$data['nikep'] = $this->input->post('nikep');
				$data['no_bpjs'] = $this->input->post('no_bpjs');

				// tabel data tpk
				$data['kode_tpk'] = $this->input->post('tpk_cari_peserta');
				
				// tabel data kepegawaian
				$data['kode_area'] = $this->input->post('area_pelayanan_cari_peserta');



				$peserta = $this->Peserta->cariPeserta($data);
				$data['peserta'] = $peserta;
				if (count($peserta)) {
					$this->session->cari = 1;

				}
				$this->session->cari_simpan = 1;
				
			}


			$data['jenis_peserta'] = $this->Peserta->getAllJenisPeserta();
			$data['area'] = $this->Area->getAllArea();
			$data['band_posisi'] = $this->Peserta->getAllBandPosisi();

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('edit_peserta', $data);
			$this->load->view('footer');
		} else {
			$this->load->view('error');
		}


	}

	public function pesertaKK($kode_peserta){

		if ($this->session->kode_admin) {
			$this->session->page = "edit_peserta";

			$data['kelompok_transaksi'] = $this->Transaksi->getAllKelompokTransaksi();
			$data['jenis_peserta'] = $this->Peserta->getAllJenisPeserta();
			$data['kelompok_peserta'] = $this->Peserta->getAllKelompokPeserta();
			$data['agama'] = $this->Peserta->getAllAgama();
			$data['area'] = $this->Area->getAllArea();
			$data['tpk'] = $this->TPK->getAllTPK();
			$data['pendidikan'] = $this->Peserta->getAllPendidikan();
			$data['bagian'] = $this->Instansi->getAllBagian();
			$data['bank'] = $this->Peserta->getAllBank();
			$data['instansi'] = $this->Peserta->getAllInstansi();
			$data['divisi'] = $this->Peserta->getAllDivisi();
			$data['band_posisi'] = $this->Peserta->getAllBandPosisi();
			$data['personal_area'] = $this->Area->getAllPersonalArea();
			$data['personal_sub_area'] = $this->Area->getAllPersonalSubArea();
			$data['kota_kantor'] = $this->Area->getAllKotaKantor();

			$data['provinsi'] = $this->Provinsi->getAllProvinsi();
			$data['kabupaten'] = $this->Provinsi->getAllKabupaten();
			$data['kecamatan'] = $this->Provinsi->getAllKecamatan();
			$data['kelurahan'] = $this->Provinsi->getAllKelurahan();





			// $data['kode_peserta'] = $kode_peserta;
			$peserta = $this->Peserta->getPesertaById($kode_peserta);
			$data['peserta'] = $peserta;
			
			$kode_nik = $peserta->row()->kode_nik;
			$data['nik'] = $this->Peserta->getNikById($kode_nik);
			$kode_data_pribadi = $peserta->row()->kode_data_pribadi;
			$data['peserta_data_pribadi'] = $this->Peserta->getDataPribadiById($kode_data_pribadi);

			$kode_data_kepegawaian = $peserta->row()->kode_data_kepegawaian;
			$peserta_data_kepegawaian = $this->Peserta->getDataKepegawaianById($kode_data_kepegawaian);
			$data['peserta_data_kepegawaian'] = $peserta_data_kepegawaian;

			$peserta_band_posisi = $this->Peserta->getBandPosisiById($peserta_data_kepegawaian->row()->kode_band_posisi);
			$data['nama_band'] = $peserta_band_posisi->row()->nama_band;
			$data['peserta_band_posisi'] = $peserta_band_posisi;

			$kode_data_tpk = $peserta->row()->kode_data_tpk;
			$data['peserta_data_tpk'] = $this->Peserta->getDataTPKById($kode_data_tpk);
			$data['peserta_tpk'] = $this->TPK->getTPKById($kode_data_tpk);

			$data['peserta_personal_sub_area'] = $this->Peserta->getPersonalSubAreaById($peserta_data_kepegawaian->row()->kode_personal_sub_area);

			$kode_alamat_ktp = $peserta->row()->kode_alamat_ktp;
			$data['peserta_data_alamat_ktp'] = $this->Peserta->getDataAlamatById($kode_alamat_ktp);

			$kode_alamat_domisili = $peserta->row()->kode_alamat_domisili;
			$data['peserta_data_alamat_domisili'] = $this->Peserta->getDataAlamatById($kode_alamat_domisili);

			$kode_data_info_lain = $peserta->row()->kode_data_info_lain;
			$data['peserta_data_info_lain'] = $this->Peserta->getDataInfoLainById($kode_data_info_lain);

			$kode_data_bank = $peserta->row()->kode_data_bank;
			$data['peserta_data_bank'] = $this->Peserta->getDataBankById($kode_data_bank);
			
			$kode_data_pernikahan = $peserta->row()->kode_status_pernikahan;
			$data['peserta_data_pernikahan'] = $this->Peserta->getDataPernikahanById($kode_data_pernikahan);
			
			$data['peserta_data_pendukung'] = $this->Peserta->getDataPendukung($kode_peserta);

			$this->db->select('*');
		    $this->db->from('peserta');
		    $this->db->join('nik', 'nik.kode_nik = peserta.kode_nik', 'left');
		    $this->db->join('peserta_data_pribadi', 'peserta_data_pribadi.kode_data_pribadi = peserta.kode_data_pribadi', 'left');
		    $this->db->where('nik', $data['nik']->row()->nik);
		    $query = $this->db->get();
		    $all_peserta = $query->result();

		    $data['all_peserta'] = $all_peserta;


			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('edit_peserta_kk', $data);
			$this->load->view('footer');
		} else {
			$this->load->view('error');
		}



	}

	public function pesertaPasangan($kode_peserta){

		if ($this->session->kode_admin) {
			$this->session->page = "edit_peserta";
			



			$data['kelompok_transaksi'] = $this->Transaksi->getAllKelompokTransaksi();
			$data['jenis_peserta'] = $this->Peserta->getAllJenisPeserta();
			$data['kelompok_peserta'] = $this->Peserta->getAllKelompokPeserta();
			$data['agama'] = $this->Peserta->getAllAgama();
			$data['area'] = $this->Area->getAllArea();
			$data['tpk'] = $this->TPK->getAllTPK();
			$data['pendidikan'] = $this->Peserta->getAllPendidikan();
			$data['bagian'] = $this->Instansi->getAllBagian();
			$data['bank'] = $this->Peserta->getAllBank();
			$data['instansi'] = $this->Peserta->getAllInstansi();
			$data['divisi'] = $this->Peserta->getAllDivisi();
			$data['band_posisi'] = $this->Peserta->getAllBandPosisi();
			$data['personal_area'] = $this->Area->getAllPersonalArea();
			$data['personal_sub_area'] = $this->Area->getAllPersonalSubArea();
			$data['kota_kantor'] = $this->Area->getAllKotaKantor();
			$data['pekerjaan'] = $this->Peserta->getAllPekerjaan();


			$data['provinsi'] = $this->Provinsi->getAllProvinsi();
			$data['kabupaten'] = $this->Provinsi->getAllKabupaten();
			$data['kecamatan'] = $this->Provinsi->getAllKecamatan();
			$data['kelurahan'] = $this->Provinsi->getAllKelurahan();





			// $data['kode_peserta'] = $kode_peserta;
			$peserta = $this->Peserta->getPesertaById($kode_peserta);
			$data['peserta'] = $peserta;
			
			$kode_nik = $peserta->row()->kode_nik;
			$data['nik'] = $this->Peserta->getNikById($kode_nik);
			$kode_data_pribadi = $peserta->row()->kode_data_pribadi;
			$data['peserta_data_pribadi'] = $this->Peserta->getDataPribadiById($kode_data_pribadi);

			$kode_data_kepegawaian = $peserta->row()->kode_data_kepegawaian;
			$peserta_data_kepegawaian = $this->Peserta->getDataKepegawaianById($kode_data_kepegawaian);
			$data['peserta_data_kepegawaian'] = $peserta_data_kepegawaian;

			$peserta_band_posisi = $this->Peserta->getBandPosisiById($peserta_data_kepegawaian->row()->kode_band_posisi);
			$data['peserta_band_posisi'] = $peserta_band_posisi;

			$kode_data_tpk = $peserta->row()->kode_data_tpk;
			$data['peserta_data_tpk'] = $this->Peserta->getDataTPKById($kode_data_tpk);
			$data['peserta_tpk'] = $this->TPK->getTPKById($kode_data_tpk);

			$data['peserta_personal_sub_area'] = $this->Peserta->getPersonalSubAreaById($peserta_data_kepegawaian->row()->kode_personal_sub_area);

			$kode_alamat_ktp = $peserta->row()->kode_alamat_ktp;
			$data['peserta_data_alamat_ktp'] = $this->Peserta->getDataAlamatById($kode_alamat_ktp);

			$kode_alamat_domisili = $peserta->row()->kode_alamat_domisili;
			$data['peserta_data_alamat_domisili'] = $this->Peserta->getDataAlamatById($kode_alamat_domisili);

			$kode_data_info_lain = $peserta->row()->kode_data_info_lain;
			$data['peserta_data_info_lain'] = $this->Peserta->getDataInfoLainById($kode_data_info_lain);

			$kode_data_bank = $peserta->row()->kode_data_bank;
			$data['peserta_data_bank'] = $this->Peserta->getDataBankById($kode_data_bank);
			
			$kode_data_pernikahan = $peserta->row()->kode_status_pernikahan;
			$data['peserta_data_pernikahan'] = $this->Peserta->getDataPernikahanById($kode_data_pernikahan);
			
			$data['peserta_data_pendukung'] = $this->Peserta->getDataPendukung($kode_peserta);

			$this->db->select('*');
		    $this->db->from('peserta');
		    $this->db->join('nik', 'nik.kode_nik = peserta.kode_nik', 'left');
		    $this->db->join('peserta_data_pribadi', 'peserta_data_pribadi.kode_data_pribadi = peserta.kode_data_pribadi', 'left');
		    $this->db->where('nik', $data['nik']->row()->nik);
		    $query = $this->db->get();
		    $all_peserta = $query->result();

		    $data['all_peserta'] = $all_peserta;

			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('edit_peserta_pasangan', $data);
			$this->load->view('footer');
		} else {
			$this->load->view('error');
		}



	}

	public function pesertaAnak($kode_peserta){

		if ($this->session->kode_admin) {
			$this->session->page = "edit_peserta";
			



			$data['kelompok_transaksi'] = $this->Transaksi->getAllKelompokTransaksi();
			$data['jenis_peserta'] = $this->Peserta->getAllJenisPeserta();
			$data['kelompok_peserta'] = $this->Peserta->getAllKelompokPeserta();
			$data['agama'] = $this->Peserta->getAllAgama();
			$data['area'] = $this->Area->getAllArea();
			$data['tpk'] = $this->TPK->getAllTPK();
			$data['pendidikan'] = $this->Peserta->getAllPendidikan();
			$data['bagian'] = $this->Instansi->getAllBagian();
			$data['bank'] = $this->Peserta->getAllBank();
			$data['instansi'] = $this->Peserta->getAllInstansi();
			$data['divisi'] = $this->Peserta->getAllDivisi();
			$data['band_posisi'] = $this->Peserta->getAllBandPosisi();
			$data['personal_area'] = $this->Area->getAllPersonalArea();
			$data['personal_sub_area'] = $this->Area->getAllPersonalSubArea();
			$data['kota_kantor'] = $this->Area->getAllKotaKantor();
			$data['pekerjaan'] = $this->Peserta->getAllPekerjaan();


			$data['provinsi'] = $this->Provinsi->getAllProvinsi();
			$data['kabupaten'] = $this->Provinsi->getAllKabupaten();
			$data['kecamatan'] = $this->Provinsi->getAllKecamatan();
			$data['kelurahan'] = $this->Provinsi->getAllKelurahan();





			// $data['kode_peserta'] = $kode_peserta;
			$peserta = $this->Peserta->getPesertaById($kode_peserta);
			$data['peserta'] = $peserta;
			
			$kode_nik = $peserta->row()->kode_nik;
			$data['nik'] = $this->Peserta->getNikById($kode_nik);
			$kode_data_pribadi = $peserta->row()->kode_data_pribadi;
			$data['peserta_data_pribadi'] = $this->Peserta->getDataPribadiById($kode_data_pribadi);

			$kode_data_kepegawaian = $peserta->row()->kode_data_kepegawaian;
			$peserta_data_kepegawaian = $this->Peserta->getDataKepegawaianById($kode_data_kepegawaian);
			$data['peserta_data_kepegawaian'] = $peserta_data_kepegawaian;

			$peserta_band_posisi = $this->Peserta->getBandPosisiById($peserta_data_kepegawaian->row()->kode_band_posisi);
			$data['peserta_band_posisi'] = $peserta_band_posisi;

			$kode_data_tpk = $peserta->row()->kode_data_tpk;
			$data['peserta_data_tpk'] = $this->Peserta->getDataTPKById($kode_data_tpk);
			$data['peserta_tpk'] = $this->TPK->getTPKById($kode_data_tpk);

			$data['peserta_personal_sub_area'] = $this->Peserta->getPersonalSubAreaById($peserta_data_kepegawaian->row()->kode_personal_sub_area);

			$kode_alamat_ktp = $peserta->row()->kode_alamat_ktp;
			$data['peserta_data_alamat_ktp'] = $this->Peserta->getDataAlamatById($kode_alamat_ktp);

			$kode_alamat_domisili = $peserta->row()->kode_alamat_domisili;
			$data['peserta_data_alamat_domisili'] = $this->Peserta->getDataAlamatById($kode_alamat_domisili);

			$kode_data_info_lain = $peserta->row()->kode_data_info_lain;
			$data['peserta_data_info_lain'] = $this->Peserta->getDataInfoLainById($kode_data_info_lain);

			$kode_data_bank = $peserta->row()->kode_data_bank;
			$data['peserta_data_bank'] = $this->Peserta->getDataBankById($kode_data_bank);
			
			$kode_data_pernikahan = $peserta->row()->kode_status_pernikahan;
			$data['peserta_data_pernikahan'] = $this->Peserta->getDataPernikahanById($kode_data_pernikahan);
			
			$data['peserta_data_pendukung'] = $this->Peserta->getDataPendukung($kode_peserta);

			$this->db->select('*');
		    $this->db->from('peserta');
		    $this->db->join('nik', 'nik.kode_nik = peserta.kode_nik', 'left');
		    $this->db->join('peserta_data_pribadi', 'peserta_data_pribadi.kode_data_pribadi = peserta.kode_data_pribadi', 'left');
		    $this->db->where('nik', $data['nik']->row()->nik);
		    $query = $this->db->get();
		    $all_peserta = $query->result();

		    $data['all_peserta'] = $all_peserta;


			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('edit_peserta_anak', $data);
			$this->load->view('footer');
		} else {
			$this->load->view('error');
		}

	}

	public function inputKK()
	{
		if ($this->session->kode_admin) {
			$this->session->page = "input_kepala_keluarga";
			$this->session->input_kepala_keluarga = 0;

			$data['tpk'] = $this->TPK->getAllTPK();
			$data['area'] = $this->Area->getAllArea();
			$data['personal_area'] = $this->Area->getAllPersonalArea();
			$data['personal_sub_area'] = $this->Area->getAllPersonalSubArea();
			$data['kelompok_peserta'] = $this->Peserta->getAllKelompokPeserta();
			$data['jenis_peserta'] = $this->Peserta->getAllJenisPeserta();
			$data['divisi'] = $this->Instansi->getAllDivisi();
			$data['bagian'] = $this->Instansi->getAllBagian();
			$data['pendidikan'] = $this->Peserta->getAllPendidikan();
			$data['klas_posisi'] = $this->Instansi->getAllKlasPosisi();

			$data['provinsi'] = $this->Provinsi->getAllProvinsi();

			$data['kelompok_transaksi'] = $this->Transaksi->getAllKelompokTransaksi();

			$data['bank'] = $this->Peserta->getAllBank();
			$data['agama'] = $this->Peserta->getAllAgama();



			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('input_kepala_keluarga', $data);
			$this->load->view('footer');
			$this->session->page = "";
		} else {
			$this->load->view('error');
		}
	

	}

	public function inputPasangan()
	{
		if ($this->session->kode_admin) {
			$this->session->page = "input_pasangan";

			$data['tpk'] = $this->TPK->getAllTPK();
			$data['area'] = $this->Area->getAllArea();
			$data['personal_area'] = $this->Area->getAllPersonalArea();
			$data['personal_sub_area'] = $this->Area->getAllPersonalSubArea();
			$data['kelompok_peserta'] = $this->Peserta->getAllKelompokPeserta();
			$data['divisi'] = $this->Instansi->getAllDivisi();
			$data['bagian'] = $this->Instansi->getAllBagian();
			$data['pendidikan'] = $this->Peserta->getAllPendidikan();
			$data['klas_posisi'] = $this->Instansi->getAllKlasPosisi();
			$data['agama'] = $this->Peserta->getAllAgama();


			$data['provinsi'] = $this->Provinsi->getAllProvinsi();

			$data['kelompok_transaksi'] = $this->Transaksi->getAllKelompokTransaksi();

			$data['bank'] = $this->Peserta->getAllBank();
			$data['pekerjaan'] = $this->Peserta->getAllPekerjaan();
			$data['jenis_peserta'] = $this->Peserta->getAllJenisPeserta();

			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('input_pasangan', $data);
			$this->load->view('footer');
			$this->session->page = "";
		} else {
			$this->load->view('error');
		}

	}

	public function inputAnak()
	{
		if ($this->session->kode_admin) {
			$this->session->page = "input_anak";

			$data['tpk'] = $this->TPK->getAllTPK();
			$data['area'] = $this->Area->getAllArea();
			$data['personal_area'] = $this->Area->getAllPersonalArea();
			$data['personal_sub_area'] = $this->Area->getAllPersonalSubArea();
			$data['kelompok_peserta'] = $this->Peserta->getAllKelompokPeserta();
			$data['divisi'] = $this->Instansi->getAllDivisi();
			$data['bagian'] = $this->Instansi->getAllBagian();
			$data['pendidikan'] = $this->Peserta->getAllPendidikan();
			$data['klas_posisi'] = $this->Instansi->getAllKlasPosisi();
			$data['agama'] = $this->Peserta->getAllAgama();


			$data['provinsi'] = $this->Provinsi->getAllProvinsi();

			$data['kelompok_transaksi'] = $this->Transaksi->getAllKelompokTransaksi();

			$data['bank'] = $this->Peserta->getAllBank();
			$data['pekerjaan'] = $this->Peserta->getAllPekerjaan();
			$data['jenis_peserta'] = $this->Peserta->getAllJenisPeserta();

			$this->session->page = "input_anak";
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('input_anak', $data);
			$this->load->view('footer');
			$this->session->page = "";
		} else {
			$this->load->view('error');
		}


	}

	public function mutasiTPK()
	{
		if ($this->session->kode_admin) {
			$this->session->page = "mutasi_tpk";

			$data['area'] = $this->Area->getAllArea();

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('mutasi_tpk', $data);
			$this->load->view('footer');
		} else {
			$this->load->view('error');
		}
	}

	public function mutasiTPKProses(){

		if ($this->session->kode_admin) {
			$data['kode_area'] = $this->input->post('area_pelayanan');
			$data['tpk_asal'] = $this->input->post('tpk_asal');
			$data['tpk_tujuan'] = $this->input->post('tpk_tujuan');

			$this->Peserta->mutasiTPK($data);


			redirect(base_url('PesertaController/mutasiTPK'));
		} else {
			$this->load->view('error');
		}

	}


	public function inputAnakProses(){

		if ($this->session->kode_admin) {
			// nik
			$data['nik'] = $this->input->post('nik');

			$query = $this->Peserta->getNik($data['nik']);

			if ($query->num_rows() == 0) {
				$this->session->input_fail = 1;
				redirect(base_url('../input_anak/'));
			}else if ($query->num_rows() > 0) {
				$kode_nik = $query->row()->kode_nik;


				// upload foto
				$data['nik'] = $this->input->post('nik');
				$data['anak_ke'] = $this->input->post('anak_ke');
				$data['pasangan_ke'] = $this->input->post('pasangan_ke');

				$config['upload_path'] = './uploads/foto';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] . "0" . $data['anak_ke'];
				$config['overwrite'] = true;


				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				// cek foto sudah ada apa belum


				$data['foto'] = "";
				if ( ! $this->upload->do_upload('foto_kk'))
				{
					$error = array('error' => $this->upload->display_errors());
					// echo $error['error'];
	      		}
	      		else
	      		{
	      			$foto = array('upload_data' => $this->upload->data());
	      			$data['foto'] = $foto['upload_data']['file_name'];
			    }

			    // upload dokumen 1
				$data['nik'] = $this->input->post('nik');
				$data['anak_ke'] = $this->input->post('anak_ke');
				$data['pasangan_ke'] = $this->input->post('pasangan_ke');

			    $config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] ."0" . $data['anak_ke'] . "-1";

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_1'] = "";
				if ( ! $this->upload->do_upload('file_1'))
				{
					$error = array('error' => $this->upload->display_errors());
	      		}
	      		else
	      		{
	      			$dokumen_1 = array('upload_data' => $this->upload->data());
	      			$data['file_1'] = $dokumen_1['upload_data']['file_name'];
			    }

			    // upload dokumen 2
				$data['nik'] = $this->input->post('nik');
				$data['anak_ke'] = $this->input->post('anak_ke');
				$data['pasangan_ke'] = $this->input->post('pasangan_ke');

			    $config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] ."0" . $data['anak_ke'] . "-2";

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_2'] = "";
				if ( ! $this->upload->do_upload('file_2'))
				{
					$error = array('error' => $this->upload->display_errors());
	      		}
	      		else
	      		{
	      			$dokumen_2 = array('upload_data' => $this->upload->data());
	      			$data['file_2'] = $dokumen_1['upload_data']['file_name'];
			    }

			    // upload dokumen 3
				$data['nik'] = $this->input->post('nik');
				$data['anak_ke'] = $this->input->post('anak_ke');
				$data['pasangan_ke'] = $this->input->post('pasangan_ke');

			    $config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] ."0" . $data['anak_ke'] . "-3";

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_3'] = "";
				if ( ! $this->upload->do_upload('file_3'))
				{
					$error = array('error' => $this->upload->display_errors());
	      		}
	      		else
	      		{
	      			$dokumen_3 = array('upload_data' => $this->upload->data());
	      			$data['file_3'] = $dokumen_1['upload_data']['file_name'];
			    }

			    $data['nik'] = $this->input->post('nik');
				$data['anak_ke'] = $this->input->post('anak_ke');
				$data['pasangan_ke'] = $this->input->post('pasangan_ke');
				$data['nikes'] = $data['nik'] . "." . $data['pasangan_ke'] . "0" . $data['anak_ke'];


				// transaksi
				$data['kelompok_transaksi'] = $this->input->post('kelompok_transaksi_kk');
				$data['jenis_transaksi'] = $this->input->post('jenis_transaksi_anak');
				$data['waktu'] = $now = date('Y-m-d H:i:s');

				// jenis peserta
				$kode_group = $this->input->post('jenis_peserta_kk');
				$jenis_peserta = $this->Peserta->getAllJenisPeserta();
				$kode_jenis_peserta = "";
				foreach ($jenis_peserta as $value) {
					if (($value->kode_group_jenis_peserta == $kode_group) && $value->kode_group_status_keluarga == 3) {	
						$kode_jenis_peserta = $value->kode_jenis_peserta;
					}
				}
				
				$data['jenis_peserta'] = $kode_jenis_peserta;

				// peserta_data_pribadi
				$data['nama'] = $this->input->post('nama');
				$data['tempat_lahir'] = $this->input->post('tempat_lahir');
				$date = str_replace('/', '-', $this->input->post('tgl_lahir'));
				$newDate = date('Y-m-d', strtotime($date));
				$data['tgl_lahir'] = $newDate;
				$data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
				$data['agama'] = $this->input->post('agama');
				$data['no_ktp'] = $this->input->post('no_ktp');
				$data['gol_darah'] = $this->input->post('gol_darah');
				$data['rhesus'] = $this->input->post('rhesus');
				$data['no_bpjs'] = $this->input->post('no_bpjs');

				// peserta_data_tpk
				$data['kode_tpk'] = $this->input->post('tpk');
				$data['status_faskes'] = $this->input->post('status_faskes');
				$data['tgl_faskes'] = null;
				if (!empty($this->input->post('tgl_faskes'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_faskes'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_faskes'] = $newDate;
				}

				// peserta_data_kepegawaian
				$data['kode_area'] = $this->input->post('area_pelayanan');

				$this->db->select('*');
		        $this->db->from('peserta');
		        $this->db->join('nik', 'nik.kode_nik = peserta.kode_nik', 'left');
		        $this->db->join('peserta_data_kepegawaian', 'peserta_data_kepegawaian.kode_data_kepegawaian = peserta.kode_data_kepegawaian', 'left');
		        $this->db->where('nik', $data['nik']);
		        $query = $this->db->get();
		        $temp = $query->result();

		        $data['kode_band_posisi'] = "";
		        $data['kode_personal_sub_area'] = "";
		        foreach ($temp as $value) {
		        	if ($value->kode_band_posisi) {
		        		$data['kode_band_posisi'] = $value->kode_band_posisi;
		        	}
		        	if ($value->kode_personal_sub_area) {
		        		$data['kode_personal_sub_area'] = $value->kode_personal_sub_area;
		        	}
		        	
		        }

				// peserta_data_alamat
				$data['alamat_ktp'] = $this->input->post('alamat_ktp');
				$data['rt_ktp'] = $this->input->post('rt_ktp');
				$data['rw_ktp'] = $this->input->post('rw_ktp');
				$data['kode_provinsi_ktp'] = $this->input->post('provinsi_ktp');
				$data['kode_kabupaten_ktp'] = $this->input->post('kabupaten_ktp');
				$data['kode_kecamatan_ktp'] = $this->input->post('kecamatan_ktp');
				$data['kode_kelurahan_ktp'] = $this->input->post('kelurahan_ktp');
				$data['kode_pos_ktp'] = $this->input->post('kode_pos_ktp');
				$data['no_telp_ktp'] = $this->input->post('no_telp_ktp');

				$data['alamat_domisili'] = $this->input->post('alamat_domisili');
				$data['rt_domisili'] = $this->input->post('rt_domisili');
				$data['rw_domisili'] = $this->input->post('rw_domisili');
				$data['kode_provinsi_domisili'] = $this->input->post('provinsi_domisili');
				$data['kode_kabupaten_domisili'] = $this->input->post('kabupaten_domisili');
				$data['kode_kecamatan_domisili'] = $this->input->post('kecamatan_domisili');
				$data['kode_kelurahan_domisili'] = $this->input->post('kelurahan_domisili');
				$data['kode_pos_domisili'] = $this->input->post('kode_pos_domisili');
				$data['no_telp_domisili'] = $this->input->post('no_telp_domisili');

				// peserta_data_info_lain
				$data['no_hp'] = $this->input->post('no_hp');
				$data['tgl_meninggal'] = null;
				if (!empty($this->input->post('tgl_meninggal'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_meninggal'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_meninggal'] = $newDate;
				}
				$data['catatan'] = $this->input->post('catatan');
				$data['alasan_berhenti_peserta'] = $this->input->post('alasan_berhenti_peserta');
				$data['no_anggota_keluarga'] = $data['pasangan_ke'];
				$data['no_anggota_keluarga_2'] = $data['anak_ke'];

				// peserta_data_bank
				$data['nama_pemilik_rekening'] = $this->input->post('nama_pemilik_rekening');
				$data['kode_bank'] = $this->input->post('nama_bank');
				$data['no_rekening_bank'] = $this->input->post('no_rekening_bank');
				$data['status_kartu'] = $this->input->post('status_kartu');
				$data['tgl_cetak'] = null;
				if (!empty($this->input->post('tgl_cetak'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_cetak'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_cetak'] = $newDate;
				}
				$data['tgl_akhir_kartu'] = null;
				if (!empty($this->input->post('tgl_akhir_kartu'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_akhir_kartu'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_akhir_kartu'] = $newDate;
				}


				// peserta_data_pendukung
				$data['no_dokumen_1'] = $this->input->post('no_dokumen_1');
				$data['perihal_1'] = $this->input->post('perihal_1');
				$data['tgl_dokumen_1'] = null;
				if (!empty($this->input->post('tgl_dokumen_1'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_1'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_1'] = $newDate;
				}

				$data['no_dokumen_2'] = $this->input->post('no_dokumen_2');
				$data['perihal_2'] = $this->input->post('perihal_2');
				$data['tgl_dokumen_2'] = null;
				if (!empty($this->input->post('tgl_dokumen_2'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_2'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_2'] = $newDate;
				}

				$data['no_dokumen_3'] = $this->input->post('no_dokumen_3');
				$data['perihal_3'] = $this->input->post('perihal_3');
				$data['tgl_dokumen_3'] = null;
				if (!empty($this->input->post('tgl_dokumen_3'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_3'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_3'] = $newDate;
				}

				$this->Peserta->inputAnak($data);
				$this->session->input_berhasil = 1;
				redirect(base_url('PesertaController/pesertaAnak/'. $this->Peserta->getLastPeserta()));
			}
		} else {
			$this->load->view('error');
		}


	}

	

	public function inputPasanganProses()
	{
		if ($this->session->kode_admin) {
			// nik
			$data['nik'] = $this->input->post('nik');
			$data['nikes'] = $data['nik'] . ".000";

			$query = $this->Peserta->getNik($data['nik']);

			if ($query->num_rows() == 0) {
				$this->session->input_fail = 1;
				redirect(base_url('../input_pasangan/'));
			}else if ($query->num_rows() > 0) {

				$kode_nik = $query->row()->kode_nik;


				// upload foto
				$data['nik'] = $this->input->post('nik');
				$data['pasangan_ke'] = $this->input->post('pasangan_ke');

				$config['upload_path'] = './uploads/foto';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] . "00";
				$config['overwrite'] = true;


				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				// cek foto sudah ada apa belum


				$data['foto'] = "";
				if ( ! $this->upload->do_upload('foto_kk'))
				{
					$error = array('error' => $this->upload->display_errors());
					// echo $error['error'];
	      		}
	      		else
	      		{
	      			$foto = array('upload_data' => $this->upload->data());
	      			$data['foto'] = $foto['upload_data']['file_name'];
			     }

			     // upload dokumen 1
				$data['nik'] = $this->input->post('nik');
				$data['pasangan_ke'] = $this->input->post('pasangan_ke');

			    $config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] ."00" . "-1";

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_1'] = "";
				if ( ! $this->upload->do_upload('file_1'))
				{
					$error = array('error' => $this->upload->display_errors());
	      		}
	      		else
	      		{
	      			$dokumen_1 = array('upload_data' => $this->upload->data());
	      			$data['file_1'] = $dokumen_1['upload_data']['file_name'];
			    }

			    // upload dokumen 2
				$data['nik'] = $this->input->post('nik');
				$data['pasangan_ke'] = $this->input->post('pasangan_ke');

			    $config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-". $data['pasangan_ke'] ."00" . "-2";

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_2'] = "";
				if ( ! $this->upload->do_upload('file_2'))
				{
					$error = array('error' => $this->upload->display_errors());
	      		}
	      		else
	      		{
	      			$dokumen_2 = array('upload_data' => $this->upload->data());
	      			$data['file_2'] = $dokumen_2['upload_data']['file_name'];
			    }

			    // upload dokumen 3
				$data['nik'] = $this->input->post('nik');
				$data['pasangan_ke'] = $this->input->post('pasangan_ke');

			    $config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] . "00" . "-3";

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_3'] = "";
				if ( ! $this->upload->do_upload('file_3'))
				{
					$error = array('error' => $this->upload->display_errors());
	      		}
	      		else
	      		{
	      			$dokumen_3 = array('upload_data' => $this->upload->data());
	      			$data['file_3'] = $dokumen_3['upload_data']['file_name'];
			    }

			    $data['nik'] = $this->input->post('nik');
				$data['pasangan_ke'] = $this->input->post('pasangan_ke');
				$data['nikes'] = $data['nik'] . "." . $data['pasangan_ke'] . "00";


				// transaksi
				$data['kelompok_transaksi'] = $this->input->post('kelompok_transaksi_kk');
				$data['jenis_transaksi'] = $this->input->post('jenis_transaksi_pasangan');
				$data['waktu'] = $now = date('Y-m-d H:i:s');

				// jenis peserta
				$kode_group = $this->input->post('jenis_peserta_kk');
				$jenis_peserta = $this->Peserta->getAllJenisPeserta();
				$kode_jenis_peserta = "";
				foreach ($jenis_peserta as $value) {
					if (($value->kode_group_jenis_peserta == $kode_group) && $value->kode_group_status_keluarga == 2) {	
						$kode_jenis_peserta = $value->kode_jenis_peserta;
					}
				}
				
				$data['jenis_peserta'] = $kode_jenis_peserta;


				// $jenis_transaksi = $this->Transaksi->getJenisTransaksiById($data['jenis_transaksi']);
				// $data['kode_jenis_peserta'] = $jenis_transaksi->row()->kode_jenis_peserta;

				
				// peserta_data_pribadi
				$data['nama'] = $this->input->post('nama');
				$data['tempat_lahir'] = $this->input->post('tempat_lahir');
				$date = str_replace('/', '-', $this->input->post('tgl_lahir'));
				$newDate = date('Y-m-d', strtotime($date));
				$data['tgl_lahir'] = $newDate;
				$data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
				$data['agama'] = $this->input->post('agama');
				$data['no_ktp'] = $this->input->post('no_ktp');
				$data['gol_darah'] = $this->input->post('gol_darah');
				$data['rhesus'] = $this->input->post('rhesus');
				$data['no_bpjs'] = $this->input->post('no_bpjs');


				// peserta_data_tpk
				$data['kode_tpk'] = $this->input->post('tpk');
				$data['status_faskes'] = $this->input->post('status_faskes');
				$data['tgl_faskes'] = null;
				if (!empty($this->input->post('tgl_faskes'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_faskes'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_faskes'] = $newDate;
				}


				// peserta_data_kepegawaian
				$data['kode_area'] = $this->input->post('area_pelayanan');
				$data['nip_pasangan'] = $this->input->post('nip_pasangan');
				$data['kode_pendidikan'] = $this->input->post('pendidikan');
				$data['kode_pekerjaan'] = $this->input->post('pekerjaan');

				$this->db->select('*');
		        $this->db->from('peserta');
		        $this->db->join('nik', 'nik.kode_nik = peserta.kode_nik', 'left');
		        $this->db->join('peserta_data_kepegawaian', 'peserta_data_kepegawaian.kode_data_kepegawaian = peserta.kode_data_kepegawaian', 'left');
		        $this->db->where('nik', $data['nik']);
		        $query = $this->db->get();
		        $temp = $query->result();

		        $data['kode_band_posisi'] = "";
		        $data['kode_personal_sub_area'] = "";
		        foreach ($temp as $value) {
		        	if ($value->kode_band_posisi) {
		        		$data['kode_band_posisi'] = $value->kode_band_posisi;
		        	}
		        	if ($value->kode_personal_sub_area) {
		        		$data['kode_personal_sub_area'] = $value->kode_personal_sub_area;
		        	}
		        }

				
				// peserta_data_alamat
				$data['alamat_ktp'] = $this->input->post('alamat_ktp');
				$data['rt_ktp'] = $this->input->post('rt_ktp');
				$data['rw_ktp'] = $this->input->post('rw_ktp');
				$data['kode_provinsi_ktp'] = $this->input->post('provinsi_ktp');
				$data['kode_kabupaten_ktp'] = $this->input->post('kabupaten_ktp');
				$data['kode_kecamatan_ktp'] = $this->input->post('kecamatan_ktp');
				$data['kode_kelurahan_ktp'] = $this->input->post('kelurahan_ktp');
				$data['kode_pos_ktp'] = $this->input->post('kode_pos_ktp');
				$data['no_telp_ktp'] = $this->input->post('no_telp_ktp');



				$data['alamat_domisili'] = $this->input->post('alamat_domisili');
				$data['rt_domisili'] = $this->input->post('rt_domisili');
				$data['rw_domisili'] = $this->input->post('rw_domisili');
				$data['kode_provinsi_domisili'] = $this->input->post('provinsi_domisili');
				$data['kode_kabupaten_domisili'] = $this->input->post('kabupaten_domisili');
				$data['kode_kecamatan_domisili'] = $this->input->post('kecamatan_domisili');
				$data['kode_kelurahan_domisili'] = $this->input->post('kelurahan_domisili');
				$data['kode_pos_domisili'] = $this->input->post('kode_pos_domisili');
				$data['no_telp_domisili'] = $this->input->post('no_telp_domisili');

				// peserta_data_info_lain
				$data['no_hp'] = $this->input->post('no_hp');
				$data['tgl_meninggal'] = null;
				if (!empty($this->input->post('tgl_meninggal'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_meninggal'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_meninggal'] = $newDate;
				}
				$data['catatan'] = $this->input->post('catatan');
				$data['alasan_berhenti_peserta'] = $this->input->post('alasan_berhenti_peserta');
				$data['no_anggota_keluarga'] = $data['pasangan_ke'];

				
				// peserta_data_bank
				$data['nama_pemilik_rekening'] = $this->input->post('nama_pemilik_rekening');
				$data['kode_bank'] = $this->input->post('nama_bank');
				$data['no_rekening_bank'] = $this->input->post('no_rekening_bank');
				$data['status_kartu'] = $this->input->post('status_kartu');
				$data['tgl_cetak'] = null;
				if (!empty($this->input->post('tgl_cetak'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_cetak'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_cetak'] = $newDate;
				}
				$data['tgl_akhir_kartu'] = null;
				if (!empty($this->input->post('tgl_akhir_kartu'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_akhir_kartu'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_akhir_kartu'] = $newDate;
				}



				// peserta_data_pernikahan
				$data['status_pernikahan'] = $this->input->post('status_pernikahan');
				$data['tgl_nikah'] = null;
				if (!empty($this->input->post('tgl_nikah'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_nikah'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_nikah'] = $newDate;
				}
				$data['tgl_cerai'] = null;
				if (!empty($this->input->post('tgl_cerai'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_cerai'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_cerai'] = $newDate;
				}



				// peserta_data_pendukung
				$data['no_dokumen_1'] = $this->input->post('no_dokumen_1');
				$data['perihal_1'] = $this->input->post('perihal_1');
				$data['tgl_dokumen_1'] = null;
				if (!empty($this->input->post('tgl_dokumen_1'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_1'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_1'] = $newDate;
				}
				$data['no_dokumen_2'] = $this->input->post('no_dokumen_2');
				$data['perihal_2'] = $this->input->post('perihal_2');
				$data['tgl_dokumen_2'] = null;
				if (!empty($this->input->post('tgl_dokumen_2'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_2'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_2'] = $newDate;
				}
				$data['no_dokumen_3'] = $this->input->post('no_dokumen_3');
				$data['perihal_3'] = $this->input->post('perihal_3');
				$data['tgl_dokumen_3'] = null;
				if (!empty($this->input->post('tgl_dokumen_3'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_3'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_3'] = $newDate;
				}

				$this->Peserta->inputPasangan($data);
				$this->session->input_berhasil = 1;
				redirect(base_url('PesertaController/pesertaPasangan/'. $this->Peserta->getLastPeserta()));
			}
		} else {
			$this->load->view('error');
		}
	}

	
	public function inputKKProses()
	{
		if ($this->session->kode_admin) {
			// nik
			$data['nik'] = $this->input->post('nik');

			$query = $this->Peserta->getNik($data['nik']);

			if ($query->num_rows() > 0) {
				$this->session->input_fail = 1;
				redirect(base_url('../input_kepala_keluarga/'));
				
			}else if($query->num_rows() == 0){

				// upload foto
				$data['nik'] = $this->input->post('nik');

				$config['upload_path'] = './uploads/foto';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-000";
				$config['overwrite'] = true;


				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				// cek foto sudah ada apa belum


				$data['foto'] = "";
				if ( ! $this->upload->do_upload('foto_kk'))
				{
					$error = array('error' => $this->upload->display_errors());
					// echo $error['error'];
	      		}
	      		else
	      		{
	      			$foto = array('upload_data' => $this->upload->data());
	      			$data['foto'] = $foto['upload_data']['file_name'];
			     }

			     // upload dokumen 1
				$data['nik'] = $this->input->post('nik');

			    $config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-000-1";

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_1'] = "";
				if ( ! $this->upload->do_upload('file_1'))
				{
					$error = array('error' => $this->upload->display_errors());
	      		}
	      		else
	      		{
	      			$dokumen_1 = array('upload_data' => $this->upload->data());
	      			$data['file_1'] = $dokumen_1['upload_data']['file_name'];
			    }

			    // upload dokumen 2
				$data['nik'] = $this->input->post('nik');

			    $config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-000-2";

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_2'] = "";
				if ( ! $this->upload->do_upload('file_2'))
				{
					$error = array('error' => $this->upload->display_errors());
	      		}
	      		else
	      		{
	      			$dokumen_2 = array('upload_data' => $this->upload->data());
	      			$data['file_2'] = $dokumen_2['upload_data']['file_name'];
			    }

			    // upload dokumen 3
				$data['nik'] = $this->input->post('nik');

			    $config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-000-3";

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_3'] = "";
				if ( ! $this->upload->do_upload('file_3'))
				{
					$error = array('error' => $this->upload->display_errors());
	      		}
	      		else
	      		{
	      			$dokumen_3 = array('upload_data' => $this->upload->data());
	      			$data['file_3'] = $dokumen_3['upload_data']['file_name'];
			    }
	      		
	      		$data['nik'] = $this->input->post('nik');
				$data['nikes'] = $data['nik'] . ".000";


				// transaksi
				$data['kelompok_transaksi'] = $this->input->post('kelompok_transaksi_kk');
				$data['jenis_transaksi'] = $this->input->post('jenis_transaksi_kk');
				$data['waktu'] = $now = date('Y-m-d H:i:s');

				// jenis peserta
				$kode_group = $this->input->post('jenis_peserta_kk');
				$jenis_peserta = $this->Peserta->getAllJenisPeserta();
				$kode_jenis_peserta = "";
				foreach ($jenis_peserta as $value) {
					if (($value->kode_group_jenis_peserta == $kode_group) && $value->kode_group_status_keluarga == 1) {	
						$kode_jenis_peserta = $value->kode_jenis_peserta;
					}
				}

				$data['jenis_peserta'] = $kode_jenis_peserta;

				// peserta_data_pribadi
				$data['nama'] = $this->input->post('nama');
				$data['tempat_lahir'] = $this->input->post('tempat_lahir');
				$date = str_replace('/', '-', $this->input->post('tgl_lahir'));
				$newDate = date('Y-m-d', strtotime($date));
				$data['tgl_lahir'] = $newDate;
				$data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
				$data['agama'] = $this->input->post('agama');
				$data['no_ktp'] = $this->input->post('no_ktp');
				$data['gol_darah'] = $this->input->post('gol_darah');
				$data['rhesus'] = $this->input->post('rhesus');
				$data['no_bpjs'] = $this->input->post('no_bpjs');

				// peserta_data_tpk
				$data['kode_tpk'] = $this->input->post('tpk');
				$data['status_faskes'] = $this->input->post('status_faskes');
				$data['tgl_faskes'] = null;
				if (!empty($this->input->post('tgl_faskes'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_faskes'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_faskes'] = $newDate;
				}

				// peserta_data_kepegawaian
				$data['kode_kelompok_peserta'] = $this->input->post('kelompok_peserta');
				$data['kode_instansi'] = $this->input->post('instansi');
				$data['kode_band_posisi'] = $this->input->post('klas_posisi');
				$data['klas_posisi'] = $data['kode_band_posisi'];
				$data['jabatan'] = $this->input->post('jabatan');
				$data['kode_area'] = $this->input->post('area_pelayanan');
				$data['tgl_capeg'] = null;
				if (!empty($this->input->post('tgl_capeg'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_capeg'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_capeg'] = $newDate;
				}
				$data['tgl_mulai_kerja'] = null;
				if (!empty($this->input->post('tgl_mulai_kerja'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_mulai_kerja'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_mulai_kerja'] = $newDate;
				}
				$data['tgl_pensiun'] = null;
				if (!empty($this->input->post('tgl_pensiun'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_pensiun'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_pensiun'] = $newDate;
				}
				$data['tgl_berhenti_kerja'] = null;
				if (!empty($this->input->post('tgl_berhenti_kerja'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_berhenti_kerja'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_berhenti_kerja'] = $newDate;
				}
				$data['alasan_berhenti_kerja'] = $this->input->post('alasan_berhenti_kerja');
				$data['kode_pendidikan'] = $this->input->post('pendidikan');
				$data['kode_divisi'] = $this->input->post('divisi');
				$data['kode_bagian'] = $this->input->post('bagian');
				$data['kelas_perawatan'] = $this->input->post('kelas_perawatan');
				$data['kode_personal_sub_area'] = $this->input->post('personal_sub_area');

				// peserta_data_alamat
				$data['alamat_ktp'] = $this->input->post('alamat_ktp');
				$data['rt_ktp'] = $this->input->post('rt_ktp');
				$data['rw_ktp'] = $this->input->post('rw_ktp');
				$data['kode_provinsi_ktp'] = $this->input->post('provinsi_ktp');
				$data['kode_kabupaten_ktp'] = $this->input->post('kabupaten_ktp');
				$data['kode_kecamatan_ktp'] = $this->input->post('kecamatan_ktp');
				$data['kode_kelurahan_ktp'] = $this->input->post('kelurahan_ktp');
				$data['kode_pos_ktp'] = $this->input->post('kode_pos_ktp');
				$data['no_telp_ktp'] = $this->input->post('no_telp_ktp');

				$data['alamat_domisili'] = $this->input->post('alamat_domisili');
				$data['rt_domisili'] = $this->input->post('rt_domisili');
				$data['rw_domisili'] = $this->input->post('rw_domisili');
				$data['kode_provinsi_domisili'] = $this->input->post('provinsi_domisili');
				$data['kode_kabupaten_domisili'] = $this->input->post('kabupaten_domisili');
				$data['kode_kecamatan_domisili'] = $this->input->post('kecamatan_domisili');
				$data['kode_kelurahan_domisili'] = $this->input->post('kelurahan_domisili');
				$data['kode_pos_domisili'] = $this->input->post('kode_pos_domisili');
				$data['no_telp_domisili'] = $this->input->post('no_telp_domisili');

				// peserta_data_info_lain
				$data['no_hp'] = $this->input->post('no_hp');
				$data['tgl_meninggal'] = null;
				if (!empty($this->input->post('tgl_meninggal'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_meninggal'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_meninggal'] = $newDate;
				}
				$data['catatan'] = $this->input->post('catatan');
				
				// peserta_data_bank
				$data['nama_pemilik_rekening'] = $this->input->post('nama_pemilik_rekening');
				$data['kode_bank'] = $this->input->post('nama_bank');
				$data['no_rekening_bank'] = $this->input->post('no_rekening_bank');
				$data['status_kartu'] = $this->input->post('status_kartu');
				$data['tgl_cetak'] = null;
				if (!empty($this->input->post('tgl_cetak'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_cetak'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_cetak'] = $newDate;
				}
				$data['tgl_akhir_kartu'] = null;
				if (!empty($this->input->post('tgl_akhir_kartu'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_akhir_kartu'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_akhir_kartu'] = $newDate;
				}

				// peserta_data_pernikahan
				$data['status_pernikahan'] = $this->input->post('status_pernikahan');
				$data['tgl_nikah'] = null;
				if (!empty($this->input->post('tgl_nikah'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_nikah'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_nikah'] = $newDate;
				}
				$data['tgl_cerai'] = null;
				if (!empty($this->input->post('tgl_cerai'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_cerai'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_cerai'] = $newDate;
				}

				// peserta_data_pendukung
				$data['no_dokumen_1'] = $this->input->post('no_dokumen_1');
				$data['perihal_1'] = $this->input->post('perihal_1');
				$data['tgl_dokumen_1'] = null;
				if (!empty($this->input->post('tgl_dokumen_1'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_1'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_1'] = $newDate;
				}
				$data['no_dokumen_2'] = $this->input->post('no_dokumen_2');
				$data['perihal_2'] = $this->input->post('perihal_2');
				$data['tgl_dokumen_2'] = null;
				if (!empty($this->input->post('tgl_dokumen_2'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_2'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_2'] = $newDate;
				}
				$data['no_dokumen_3'] = $this->input->post('no_dokumen_3');
				$data['perihal_3'] = $this->input->post('perihal_3');
				$data['tgl_dokumen_3'] = null;
				if (!empty($this->input->post('tgl_dokumen_3'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_3'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_3'] = $newDate;
				}




				$this->Peserta->inputKepalaKeluarga($data);
				$this->session->input_kepala_keluarga = 1;
				$this->session->input_berhasil = 1;
				redirect(base_url('PesertaController/pesertaKK/'. $this->Peserta->getLastPeserta()));


			}	
		} else {
			$this->load->view('error');
		}
	}

	public function editAnak($kode_peserta){
		if ($this->session->kode_admin) {
			// nik
			$nik = $this->input->post('nik');
			$query = $this->Peserta->getNik($nik); // cek ada nik baru atau ngga
			$peserta = $this->Peserta->getPesertaById($kode_peserta); // nik lama 
			$nik_peserta = $this->Peserta->getNikById($peserta->row()->kode_nik);

			$nik_lama = $nik_peserta->row()->nik;
			$nik_baru = $nik;
			echo "nik lama: " . $nik_lama . "<br>";
			echo "nik baru: " . $nik_baru . "<br>";

			if ($query->num_rows() > 0) {
				// do

				$jenis_transaksi = $this->input->post('jenis_transaksi_anak_edit');
				if ($jenis_transaksi == '89' || $jenis_transaksi == '90' || $jenis_transaksi == '91' || $jenis_transaksi == '92' || $jenis_transaksi == '93') {
					
					// transaksi
					$data['kelompok_transaksi'] = $this->input->post('kelompok_transaksi_anak_edit');
					$data['jenis_transaksi'] = $this->input->post('jenis_transaksi_anak_edit');
					$data['nik'] = $this->input->post('nik');
					$data['nikes'] = $this->input->post('nikes');
					$data['waktu'] = $now = date('Y-m-d H:i:s');
					$data['kode_peserta'] = $kode_peserta;

					$this->Peserta->hapusAnak($data);

	 				$this->session->hapus_anak = $peserta->row()->nikes;

	 				$kk = $this->Peserta->getKKByNik($peserta->row()->kode_nik, $jenis_transaksi);
	 				$kk_kode = 0;
	 				foreach ($kk as $value) {
	 					$kk_kode = $value->kode_peserta;
	 				}
					redirect(base_url('PesertaController/pesertaKK/'.$kk_kode));
				}else{
					$data_info_lain = $this->Peserta->getDataInfoLainById($peserta->row()->kode_data_info_lain);
					$data['pasangan_ke'] = $data_info_lain->row()->no_anggota_keluarga;
					$data['anak_ke'] = $data_info_lain->row()->no_anggota_keluarga_2;

					// upload foto
					$data['nik'] = $nik_lama;

					$config['upload_path'] = './uploads/foto';
					$config["allowed_types"] ="*";
					$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] . "0" . $data['anak_ke'];
					$config['overwrite'] = true;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					// cek foto
					$data_pribadi = $this->Peserta->getDataPribadiById($peserta->row()->kode_data_pribadi);
					$data['foto'] = "";
					if ($data_pribadi->row()->foto) {
						$data['foto'] = $data_pribadi->row()->foto;
					}

					if ( ! $this->upload->do_upload('foto_kk'))
					{
						$error = array('error' => $this->upload->display_errors());
						// echo $error['error'];
			      	}
			      	else
			      	{
			      		$foto = array('upload_data' => $this->upload->data());
			      		$data['foto'] = $foto['upload_data']['file_name'];
					}

					// upload dokumen

					$pendukung = $this->Peserta->getDataPendukung($kode_peserta);
					$jml_pendukung = 0;
					foreach ($pendukung as $value) {
						$jml_pendukung = $jml_pendukung + 1;
					}
					$jml_pendukung = $jml_pendukung + 1;


					// upload dokumen 1

					$data['nik'] = $nik_lama;

					$config['upload_path'] = './uploads/data_pendukung';
					$config["allowed_types"] ="*";
					$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] ."0" . $data['anak_ke'] ."-" . $jml_pendukung;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					$data['file_1'] = "";
					if ( ! $this->upload->do_upload('file_1'))
					{
						$error = array('error' => $this->upload->display_errors());

			      	}
			      	else
			      	{
			      		$dokumen_1 = array('upload_data' => $this->upload->data());
			      		$data['file_1'] = $dokumen_1['upload_data']['file_name'];
						$jml_pendukung = $jml_pendukung + 1;
					}

					// upload dokumen 2
					$data['nik'] = $nik_lama;

					$config['upload_path'] = './uploads/data_pendukung';
					$config["allowed_types"] ="*";
					$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] ."0" . $data['anak_ke'] ."-" . $jml_pendukung;
					

					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					$data['file_2'] = "";
					if ( ! $this->upload->do_upload('file_2'))
					{
						$error = array('error' => $this->upload->display_errors());
			      	}
			      	else
			      	{
			      		$dokumen_2 = array('upload_data' => $this->upload->data());
			      		$data['file_2'] = $dokumen_2['upload_data']['file_name'];
						$jml_pendukung = $jml_pendukung + 1;

					}

					// upload dokumen 3
					$data['nik'] = $nik_lama;

					$config['upload_path'] = './uploads/data_pendukung';
					$config["allowed_types"] ="*";
					$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] ."0" . $data['anak_ke'] ."-" . $jml_pendukung;
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					$data['file_3'] = "";
					if ( ! $this->upload->do_upload('file_3'))
					{
						$error = array('error' => $this->upload->display_errors());
			      	}
			      	else
			      	{
			      		$dokumen_3 = array('upload_data' => $this->upload->data());
			      		$data['file_3'] = $dokumen_3['upload_data']['file_name'];
					}

					// nik
					$data['nik'] = $this->input->post('nik');
					$data['nikes'] = $data['nik'] . "." . $data['pasangan_ke'] . "0" . $data['anak_ke'];

					// transaksi
					$data['kelompok_transaksi'] = $this->input->post('kelompok_transaksi_anak_edit');
					$data['jenis_transaksi'] = $this->input->post('jenis_transaksi_anak_edit');
					$data['jenis_transaksi_full'] = $this->Transaksi->getJenisTransaksiById($data['jenis_transaksi']);
					
					$data['waktu'] = $now = date('Y-m-d H:i:s');

					// peserta_data_pribadi
					$nama = $this->input->post('nama');
					$status_faskes = $this->input->post('status_faskes');
					$data['status_meninggal'] = 0;
					if ($data['jenis_transaksi'] == '94' || $data['jenis_transaksi'] == '95' || $data['jenis_transaksi'] == '96' || $data['jenis_transaksi'] == '97' || $data['jenis_transaksi'] == '98' ) {
							$nama = $this->input->post('nama') . " (ALM.)";
							$data['status_meninggal'] = 1;
							$status_faskes = 0;
					}

					$data['nama'] = $nama;

					
					$data['tempat_lahir'] = $this->input->post('tempat_lahir');
					$date = str_replace('/', '-', $this->input->post('tgl_lahir'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_lahir'] = $newDate;
					$data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
					$data['agama'] = $this->input->post('agama');
					$data['no_ktp'] = $this->input->post('no_ktp');
					$data['gol_darah'] = $this->input->post('gol_darah');
					$data['rhesus'] = $this->input->post('rhesus');
					$data['no_bpjs'] = $this->input->post('no_bpjs');

					// peserta_data_tpk
					$data['kode_tpk'] = $this->input->post('tpk');
					$data['status_faskes'] = $status_faskes;
					$data['tgl_faskes'] = null;
					if (!empty($this->input->post('tgl_faskes'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_faskes'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_faskes'] = $newDate;
					}



					// peserta_data_kepegawaian
					$data['kode_area'] = $this->input->post('area_pelayanan');

					$this->db->select('*');
			        $this->db->from('peserta');
			        $this->db->join('nik', 'nik.kode_nik = peserta.kode_nik', 'left');
			        $this->db->join('peserta_data_kepegawaian', 'peserta_data_kepegawaian.kode_data_kepegawaian = peserta.kode_data_kepegawaian', 'left');
			        $this->db->where('nik', $data['nik']);
			        $query = $this->db->get();
			        $temp = $query->result();

			        $data['kode_band_posisi'] = "";
			        $data['kode_personal_sub_area'] = "";
			        foreach ($temp as $value) {
			        	if ($value->kode_band_posisi) {
			        		$data['kode_band_posisi'] = $value->kode_band_posisi;
			        	}
			        	if ($value->kode_personal_sub_area) {
			        		$data['kode_personal_sub_area'] = $value->kode_personal_sub_area;
			        	}
			        }

					// peserta_data_alamat
					$data['alamat_ktp'] = $this->input->post('alamat_ktp');
					$data['rt_ktp'] = $this->input->post('rt_ktp');
					$data['rw_ktp'] = $this->input->post('rw_ktp');
					$data['kode_provinsi_ktp'] = $this->input->post('provinsi_ktp');
					$data['kode_kabupaten_ktp'] = $this->input->post('kabupaten_ktp');
					$data['kode_kecamatan_ktp'] = $this->input->post('kecamatan_ktp');
					$data['kode_kelurahan_ktp'] = $this->input->post('kelurahan_ktp');
					$data['kode_pos_ktp'] = $this->input->post('kode_pos_ktp');
					$data['no_telp_ktp'] = $this->input->post('no_telp_ktp');

					$data['alamat_domisili'] = $this->input->post('alamat_domisili');
					$data['rt_domisili'] = $this->input->post('rt_domisili');
					$data['rw_domisili'] = $this->input->post('rw_domisili');
					$data['kode_provinsi_domisili'] = $this->input->post('provinsi_domisili');
					$data['kode_kabupaten_domisili'] = $this->input->post('kabupaten_domisili');
					$data['kode_kecamatan_domisili'] = $this->input->post('kecamatan_domisili');
					$data['kode_kelurahan_domisili'] = $this->input->post('kelurahan_domisili');
					$data['kode_pos_domisili'] = $this->input->post('kode_pos_domisili');
					$data['no_telp_domisili'] = $this->input->post('no_telp_domisili');

					// peserta_data_info_lain
					$data['no_hp'] = $this->input->post('no_hp');
					$data['tgl_meninggal'] = null;
					if (!empty($this->input->post('tgl_meninggal'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_meninggal'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_meninggal'] = $newDate;
					}
					$data['catatan'] = $this->input->post('catatan');
					$data['alasan_berhenti_peserta'] = $this->input->post('alasan_berhenti_peserta');

					// peserta_data_bank
					$data['nama_pemilik_rekening'] = $this->input->post('nama_pemilik_rekening');
					$data['kode_bank'] = $this->input->post('nama_bank');
					$data['no_rekening_bank'] = $this->input->post('no_rekening_bank');
					$data['status_kartu'] = $this->input->post('status_kartu');
					$data['tgl_cetak'] = null;
					if (!empty($this->input->post('tgl_cetak'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_cetak'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_cetak'] = $newDate;
					}
					$data['tgl_akhir_kartu'] = null;
					if (!empty($this->input->post('tgl_akhir_kartu'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_akhir_kartu'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_akhir_kartu'] = $newDate;
					}

					// peserta_data_pendukung
					$data['no_dokumen_1'] = $this->input->post('no_dokumen_1');
					$data['perihal_1'] = $this->input->post('perihal_1');
					$data['tgl_dokumen_1'] = null;
					if (!empty($this->input->post('tgl_dokumen_1'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_dokumen_1'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_dokumen_1'] = $newDate;
					}
					$data['no_dokumen_2'] = $this->input->post('no_dokumen_2');
					$data['perihal_2'] = $this->input->post('perihal_2');
					$data['tgl_dokumen_2'] = null;
					if (!empty($this->input->post('tgl_dokumen_2'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_dokumen_2'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_dokumen_2'] = $newDate;
					}
					$data['no_dokumen_3'] = $this->input->post('no_dokumen_3');
					$data['perihal_3'] = $this->input->post('perihal_3');
					$data['tgl_dokumen_3'] = null;
					if (!empty($this->input->post('tgl_dokumen_3'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_dokumen_3'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_dokumen_3'] = $newDate;
					}

					$data['nik_lama'] = $nik_lama;
					$data['nik_baru'] = $nik_baru;
					$data['peserta'] = $peserta;

					$data['no_dokumen_edit'] = $this->input->post('no_dokumen_edit[]');
					$data['perihal_edit'] = $this->input->post('perihal_edit[]');
					$tgl = $this->input->post('tgl_dokumen_edit[]');
					$newDate = array();
					
					$pendukung = $this->Peserta->getDataPendukung($kode_peserta);
					$jml_pendukung = 0;
					foreach ($pendukung as $value) {
						$jml_pendukung = $jml_pendukung + 1;
					}
					for ($i = 0; $i < $jml_pendukung ; $i++) { 
						$newDate[$i] = null;
						if (!empty($tgl[$i])) {
							$date = str_replace('/', '-', $tgl[$i]);
							$newDate[$i]  = date('Y-m-d', strtotime($date));
						}
					}
					
					$data['tgl_dokumen_edit'] = $newDate;

					$data['pendukung'] = $pendukung;

					$this->Peserta->editAnak($data);
					$this->session->berhasil_edit = 1;
					redirect(base_url('PesertaController/pesertaAnak/'. $kode_peserta));
				}
				


			}else{
				// dont
				$this->session->gagal_edit = 1;
				redirect(base_url('PesertaController/pesertaAnak/'. $kode_peserta));

			}
		} else {
			$this->load->view('error');
		}

	}

	public function editPasangan($kode_peserta){
		if ($this->session->kode_admin) {
			// nik
			$nik = $this->input->post('nik');
			$query = $this->Peserta->getNik($nik); // cek ada nik baru atau ngga
			$peserta = $this->Peserta->getPesertaById($kode_peserta); // nik lama 
			$nik_peserta = $this->Peserta->getNikById($peserta->row()->kode_nik);

			$nik_lama = $nik_peserta->row()->nik;
			$nik_baru = $nik;
			echo "nik lama: " . $nik_lama . "<br>";
			echo "nik baru: " . $nik_baru . "<br>";

			if ($query->num_rows() > 0) {
				// do
				$jenis_transaksi = $this->input->post('jenis_transaksi_pasangan_edit');
				if ($jenis_transaksi == '15' || $jenis_transaksi == '37' || $jenis_transaksi == '52' || $jenis_transaksi == '65' || $jenis_transaksi == '78') {
					
					// transaksi
					$data['kelompok_transaksi'] = $this->input->post('kelompok_transaksi_pasangan_edit');
					$data['jenis_transaksi'] = $this->input->post('jenis_transaksi_pasangan_edit');
					$data['nik'] = $this->input->post('nik');
					$data['nikes'] = $this->input->post('nikes');
					$data['waktu'] = $now = date('Y-m-d H:i:s');
					$data['kode_peserta'] = $kode_peserta;

					$this->Peserta->hapusPasangan($data);


	 				$this->session->hapus_pasangan = $peserta->row()->nikes;

	 				$kk = $this->Peserta->getKKByNik($peserta->row()->kode_nik);
	 				$kk_kode = 0;
	 				foreach ($kk as $value) {
	 					$kk_kode = $value->kode_peserta;
	 				}
					redirect(base_url('PesertaController/pesertaKK/'.$kk_kode));


				}else{
					$data_info_lain = $this->Peserta->getDataInfoLainById($peserta->row()->kode_data_info_lain);
					$data['pasangan_ke'] = $data_info_lain->row()->no_anggota_keluarga;

					// upload foto
					$data['nik'] = $this->input->post('nik');

					$config['upload_path'] = './uploads/foto';
					$config["allowed_types"] ="*";
					$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] . "00";
					$config['overwrite'] = true;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					// cek foto
					$data_pribadi = $this->Peserta->getDataPribadiById($peserta->row()->kode_data_pribadi);
					$data['foto'] = "";
					if ($data_pribadi->row()->foto) {
						$data['foto'] = $data_pribadi->row()->foto;
					}

					if ( ! $this->upload->do_upload('foto_kk'))
					{
						$error = array('error' => $this->upload->display_errors());
						// echo $error['error'];
			      	}
			      	else
			      	{
			      		$foto = array('upload_data' => $this->upload->data());
			      		$data['foto'] = $foto['upload_data']['file_name'];
					}

					// upload dokumen

					$pendukung = $this->Peserta->getDataPendukung($kode_peserta);
					$jml_pendukung = 0;
					foreach ($pendukung as $value) {
						$jml_pendukung = $jml_pendukung + 1;
					}
					$jml_pendukung = $jml_pendukung + 1;


					// upload dokumen 1

					$data['nik'] = $this->input->post('nik');

					$config['upload_path'] = './uploads/data_pendukung';
					$config["allowed_types"] ="*";
					$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] ."00" ."-" . $jml_pendukung;

					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					$data['file_1'] = "";
					if ( ! $this->upload->do_upload('file_1'))
					{
						$error = array('error' => $this->upload->display_errors());

			      	}
			      	else
			      	{
			      		$dokumen_1 = array('upload_data' => $this->upload->data());
			      		$data['file_1'] = $dokumen_1['upload_data']['file_name'];
						$jml_pendukung = $jml_pendukung + 1;
					}

					// upload dokumen 2
					$data['nik'] = $this->input->post('nik');

					$config['upload_path'] = './uploads/data_pendukung';
					$config["allowed_types"] ="*";
					$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] ."00" ."-" . $jml_pendukung;
					

					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					$data['file_2'] = "";
					if ( ! $this->upload->do_upload('file_2'))
					{
						$error = array('error' => $this->upload->display_errors());
			      	}
			      	else
			      	{
			      		$dokumen_2 = array('upload_data' => $this->upload->data());
			      		$data['file_2'] = $dokumen_2['upload_data']['file_name'];
						$jml_pendukung = $jml_pendukung + 1;

					}

					// upload dokumen 3
					$data['nik'] = $this->input->post('nik');

					$config['upload_path'] = './uploads/data_pendukung';
					$config["allowed_types"] ="*";
					$config['file_name'] = $data['nik'] . "-" . $data['pasangan_ke'] ."00" ."-" . $jml_pendukung;
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					$data['file_3'] = "";
					if ( ! $this->upload->do_upload('file_3'))
					{
						$error = array('error' => $this->upload->display_errors());
			      	}
			      	else
			      	{
			      		$dokumen_3 = array('upload_data' => $this->upload->data());
			      		$data['file_3'] = $dokumen_3['upload_data']['file_name'];
					}

					// nik
					$data['nik'] = $this->input->post('nik');
					$data['nikes'] = $data['nik'] . "." . $data['pasangan_ke'] . "00";

					
					// transaksi
					$data['kelompok_transaksi'] = $this->input->post('kelompok_transaksi_pasangan_edit');
					$data['jenis_transaksi'] = $this->input->post('jenis_transaksi_pasangan_edit');
					$data['jenis_transaksi_full'] = $this->Transaksi->getJenisTransaksiById($data['jenis_transaksi']);
					$data['waktu'] = $now = date('Y-m-d H:i:s');

					



					// peserta_data_pribadi
					$nama = $this->input->post('nama');
					$status_faskes = $this->input->post('status_faskes');
					$data['status_meninggal'] = 0;
					if ($data['jenis_transaksi'] == '13' || $data['jenis_transaksi'] == '35' || $data['jenis_transaksi'] == '51' || $data['jenis_transaksi'] == '63' || $data['jenis_transaksi'] == '77' ) {
							$nama = $this->input->post('nama') . " (ALM.)";
							$data['status_meninggal'] = 1;
							$status_faskes = 0;
					}

					$data['nama'] = $nama;
					$data['tempat_lahir'] = $this->input->post('tempat_lahir');
					$date = str_replace('/', '-', $this->input->post('tgl_lahir'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_lahir'] = $newDate;
					$data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
					$data['agama'] = $this->input->post('agama');
					$data['no_ktp'] = $this->input->post('no_ktp');
					$data['gol_darah'] = $this->input->post('gol_darah');
					$data['rhesus'] = $this->input->post('rhesus');
					$data['no_bpjs'] = $this->input->post('no_bpjs');

					


					// peserta_data_tpk
					$data['kode_tpk'] = $this->input->post('tpk');
					$data['status_faskes'] = $status_faskes;
					$data['tgl_faskes'] = null;
					if (!empty($this->input->post('tgl_faskes'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_faskes'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_faskes'] = $newDate;
					}

					


					// peserta_data_kepegawaian
					$data['kode_area'] = $this->input->post('area_pelayanan');
					$data['nip_pasangan'] = $this->input->post('nip_pasangan');
					$data['kode_pendidikan'] = $this->input->post('pendidikan');
					$data['kode_pekerjaan'] = $this->input->post('pekerjaan');

					$this->db->select('*');
			        $this->db->from('peserta');
			        $this->db->join('nik', 'nik.kode_nik = peserta.kode_nik', 'left');
			        $this->db->join('peserta_data_kepegawaian', 'peserta_data_kepegawaian.kode_data_kepegawaian = peserta.kode_data_kepegawaian', 'left');
			        $this->db->where('nik', $data['nik']);
			        $query = $this->db->get();
			        $temp = $query->result();

			        $data['kode_band_posisi'] = "";
			        $data['kode_personal_sub_area'] = "";
			        foreach ($temp as $value) {
			        	if ($value->kode_band_posisi) {
			        		$data['kode_band_posisi'] = $value->kode_band_posisi;
			        	}
			        	if ($value->kode_personal_sub_area) {
			        		$data['kode_personal_sub_area'] = $value->kode_personal_sub_area;
			        	}
			        }

					


					// peserta_data_alamat
					$data['alamat_ktp'] = $this->input->post('alamat_ktp');
					$data['rt_ktp'] = $this->input->post('rt_ktp');
					$data['rw_ktp'] = $this->input->post('rw_ktp');
					$data['kode_provinsi_ktp'] = $this->input->post('provinsi_ktp');
					$data['kode_kabupaten_ktp'] = $this->input->post('kabupaten_ktp');
					$data['kode_kecamatan_ktp'] = $this->input->post('kecamatan_ktp');
					$data['kode_kelurahan_ktp'] = $this->input->post('kelurahan_ktp');
					$data['kode_pos_ktp'] = $this->input->post('kode_pos_ktp');
					$data['no_telp_ktp'] = $this->input->post('no_telp_ktp');

					


					$data['alamat_domisili'] = $this->input->post('alamat_domisili');
					$data['rt_domisili'] = $this->input->post('rt_domisili');
					$data['rw_domisili'] = $this->input->post('rw_domisili');
					$data['kode_provinsi_domisili'] = $this->input->post('provinsi_domisili');
					$data['kode_kabupaten_domisili'] = $this->input->post('kabupaten_domisili');
					$data['kode_kecamatan_domisili'] = $this->input->post('kecamatan_domisili');
					$data['kode_kelurahan_domisili'] = $this->input->post('kelurahan_domisili');
					$data['kode_pos_domisili'] = $this->input->post('kode_pos_domisili');
					$data['no_telp_domisili'] = $this->input->post('no_telp_domisili');

					


					// peserta_data_info_lain
					$data['no_hp'] = $this->input->post('no_hp');
					$data['tgl_meninggal'] = null;
					if (!empty($this->input->post('tgl_meninggal'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_meninggal'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_meninggal'] = $newDate;
					}
					$data['catatan'] = $this->input->post('catatan');
					$data['alasan_berhenti_peserta'] = $this->input->post('alasan_berhenti_peserta');

					


					// peserta_data_bank
					$data['nama_pemilik_rekening'] = $this->input->post('nama_pemilik_rekening');
					$data['kode_bank'] = $this->input->post('nama_bank');
					$data['no_rekening_bank'] = $this->input->post('no_rekening_bank');
					$data['status_kartu'] = $this->input->post('status_kartu');
					$data['tgl_cetak'] = null;
					if (!empty($this->input->post('tgl_cetak'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_cetak'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_cetak'] = $newDate;
					}
					$data['tgl_akhir_kartu'] = null;
					if (!empty($this->input->post('tgl_akhir_kartu'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_akhir_kartu'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_akhir_kartu'] = $newDate;
					}

					


					// peserta_data_pernikahan
					$data['status_pernikahan'] = $this->input->post('status_pernikahan');
					$data['tgl_nikah'] = null;
					if (!empty($this->input->post('tgl_nikah'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_nikah'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_nikah'] = $newDate;
					}
					$data['tgl_cerai'] = null;
					if (!empty($this->input->post('tgl_cerai'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_cerai'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_cerai'] = $newDate;
					}

					


					// peserta_data_pendukung
					$data['no_dokumen_1'] = $this->input->post('no_dokumen_1');
					$data['perihal_1'] = $this->input->post('perihal_1');
					$data['tgl_dokumen_1'] = null;
					if (!empty($this->input->post('tgl_dokumen_1'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_dokumen_1'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_dokumen_1'] = $newDate;
					}
					$data['no_dokumen_2'] = $this->input->post('no_dokumen_2');
					$data['perihal_2'] = $this->input->post('perihal_2');
					$data['tgl_dokumen_2'] = null;
					if (!empty($this->input->post('tgl_dokumen_2'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_dokumen_2'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_dokumen_2'] = $newDate;
					}
					$data['no_dokumen_3'] = $this->input->post('no_dokumen_3');
					$data['perihal_3'] = $this->input->post('perihal_3');
					$data['tgl_dokumen_3'] = null;
					if (!empty($this->input->post('tgl_dokumen_3'))) {
						$date = str_replace('/', '-', $this->input->post('tgl_dokumen_3'));
						$newDate = date('Y-m-d', strtotime($date));
						$data['tgl_dokumen_3'] = $newDate;
					}

					

					$data['nik_lama'] = $nik_lama;
					$data['nik_baru'] = $nik_baru;
					$data['peserta'] = $peserta;

					


					$data['no_dokumen_edit'] = $this->input->post('no_dokumen_edit[]');
					$data['perihal_edit'] = $this->input->post('perihal_edit[]');
					$tgl = $this->input->post('tgl_dokumen_edit[]');
					$newDate = array();
					
					$pendukung = $this->Peserta->getDataPendukung($kode_peserta);
					$jml_pendukung = 0;
					foreach ($pendukung as $value) {
						$jml_pendukung = $jml_pendukung + 1;
					}
					for ($i = 0; $i < $jml_pendukung ; $i++) { 
						$newDate[$i] = null;
						if (!empty($tgl[$i])) {
							$date = str_replace('/', '-', $tgl[$i]);
							$newDate[$i]  = date('Y-m-d', strtotime($date));
						}
					}
					
					$data['tgl_dokumen_edit'] = $newDate;

					$data['pendukung'] = $pendukung;

					

					$this->Peserta->editPasangan($data);
					$this->session->berhasil_edit = 1;
					
					redirect(base_url('PesertaController/pesertaPasangan/'. $kode_peserta));
				}

				


			}else{
				// dont
				$this->session->gagal_edit = 1;
				redirect(base_url('PesertaController/pesertaPasangan/'. $kode_peserta));
			}
		} else {
			$this->load->view('error');
		}

	}


	public function editKepalaKeluarga($kode_peserta){
		if ($this->session->kode_admin) {
			// nik
			$nik = $this->input->post('nik');
			$query = $this->Peserta->getNik($nik); // cek ada nik baru atau ngga
			$peserta = $this->Peserta->getPesertaById($kode_peserta); // nik lama 
			$nik_peserta = $this->Peserta->getNikById($peserta->row()->kode_nik);

			$nik_lama = $nik_peserta->row()->nik;
			$nik_baru = $nik;
			echo "nik lama: " . $nik_lama . "<br>";
			echo "nik baru: " . $nik_baru . "<br>";

			if (($query->num_rows() > 0 && $nik_lama == $nik_baru) || $query->num_rows() == 0) {
				// do
				// kalau nik baru != nik lama, maka nik lama hapus

				// kalau kk mutasi keluar, semua keluarga dihapus
				$jenis_transaksi = $this->input->post('jenis_transaksi_kk_edit');
				if ($jenis_transaksi == '8' || $jenis_transaksi == '31' || $jenis_transaksi == '46' || $jenis_transaksi == '59' || $jenis_transaksi == '73') {
					
					// transaksi
					$data['kelompok_transaksi'] = $this->input->post('kelompok_transaksi_kk_edit');
					$data['jenis_transaksi'] = $this->input->post('jenis_transaksi_kk_edit');
					$data['nik'] = $this->input->post('nik');
					$data['nikes'] = $this->input->post('nikes');
					$data['waktu'] = $now = date('Y-m-d H:i:s');
					$data['kode_nik'] = $nik_peserta->row()->kode_nik;

					$this->Peserta->hapusPasangan($data);

					$this->Peserta->hapusKeluarga($data);

	 				$this->session->hapus_keluarga = $nik_lama;
					redirect(base_url('../edit_peserta/'));


				}else{
					// upload foto
				$data['nik'] = $this->input->post('nik');

				$config['upload_path'] = './uploads/foto';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-000";
				$config['overwrite'] = true;


				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				// cek foto
				$data_pribadi = $this->Peserta->getDataPribadiById($peserta->row()->kode_data_pribadi);
				$data['foto'] = "";
				if ($data_pribadi->row()->foto) {
					$data['foto'] = $data_pribadi->row()->foto;
				}

				if ( ! $this->upload->do_upload('foto_kk'))
				{
					$error = array('error' => $this->upload->display_errors());
					// echo $error['error'];
		      	}
		      	else
		      	{
		      		$foto = array('upload_data' => $this->upload->data());
		      		$data['foto'] = $foto['upload_data']['file_name'];
				}

				// upload dokumen

				$pendukung = $this->Peserta->getDataPendukung($kode_peserta);
				$jml_pendukung = 0;
				foreach ($pendukung as $value) {
					$jml_pendukung = $jml_pendukung + 1;
				}
				$jml_pendukung = $jml_pendukung + 1;


				// upload dokumen 1

				$data['nik'] = $this->input->post('nik');

				$config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-000-" . $jml_pendukung;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_1'] = "";
				if ( ! $this->upload->do_upload('file_1'))
				{
					$error = array('error' => $this->upload->display_errors());

		      	}
		      	else
		      	{
		      		$dokumen_1 = array('upload_data' => $this->upload->data());
		      		$data['file_1'] = $dokumen_1['upload_data']['file_name'];
					$jml_pendukung = $jml_pendukung + 1;
				}

				// upload dokumen 2
				$data['nik'] = $this->input->post('nik');

				$config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-000-" . $jml_pendukung;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_2'] = "";
				if ( ! $this->upload->do_upload('file_2'))
				{
					$error = array('error' => $this->upload->display_errors());
		      	}
		      	else
		      	{
		      		$dokumen_2 = array('upload_data' => $this->upload->data());
		      		$data['file_2'] = $dokumen_2['upload_data']['file_name'];
					$jml_pendukung = $jml_pendukung + 1;

				}

				// upload dokumen 3
				$data['nik'] = $this->input->post('nik');

				$config['upload_path'] = './uploads/data_pendukung';
				$config["allowed_types"] ="*";
				$config['file_name'] = $data['nik'] . "-000-" . $jml_pendukung;

				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				$data['file_3'] = "";
				if ( ! $this->upload->do_upload('file_3'))
				{
					$error = array('error' => $this->upload->display_errors());
		      	}
		      	else
		      	{
		      		$dokumen_3 = array('upload_data' => $this->upload->data());
		      		$data['file_3'] = $dokumen_3['upload_data']['file_name'];
				}

				// nik
				$data['nik'] = $this->input->post('nik');
				$data['nikes'] = $data['nik'] . ".000";

				// transaksi
				$data['kelompok_transaksi'] = $this->input->post('kelompok_transaksi_kk_edit');
				$data['jenis_transaksi'] = $this->input->post('jenis_transaksi_kk_edit');
				$data['jenis_transaksi_full'] = $this->Transaksi->getJenisTransaksiById($data['jenis_transaksi']);
				$data['jenis_peserta_full'] = $this->Peserta->getJenisPesertaById($peserta->row()->kode_jenis_peserta); 
				$data['waktu'] = $now = date('Y-m-d H:i:s');


				

				// peserta_data_pribadi
				$nama = $this->input->post('nama');
				$status_faskes = $this->input->post('status_faskes');
				$data['status_meninggal'] = 0;
				if ($data['jenis_transaksi'] == '9' || $data['jenis_transaksi'] == '33' || $data['jenis_transaksi'] == '47' || $data['jenis_transaksi'] == '61' || $data['jenis_transaksi'] == '74' ) {
						$nama = $this->input->post('nama') . " (ALM.)";
						$data['status_meninggal'] = 1;
						$status_faskes = 0;
				}

				$data['nama'] = $nama;

				$data['tempat_lahir'] = $this->input->post('tempat_lahir');

				$date = str_replace('/', '-', $this->input->post('tgl_lahir'));
				$newDate = date('Y-m-d', strtotime($date));

				$data['tgl_lahir'] = $newDate;
				$data['jenis_kelamin'] = $this->input->post('jenis_kelamin');
				$data['agama'] = $this->input->post('agama');
				$data['no_ktp'] = $this->input->post('no_ktp');
				$data['gol_darah'] = $this->input->post('gol_darah');
				$data['rhesus'] = $this->input->post('rhesus');
				$data['no_bpjs'] = $this->input->post('no_bpjs');

				// peserta_data_tpk
				$data['kode_tpk'] = $this->input->post('tpk');
				$data['status_faskes'] = $status_faskes;
				$data['tgl_faskes'] = null;
				if (!empty($this->input->post('tgl_faskes'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_faskes'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_faskes'] = $newDate;
				}

				// peserta_data_kepegawaian
				$data['kode_kelompok_peserta'] = $this->input->post('kelompok_peserta');
				$data['kode_instansi'] = $this->input->post('instansi');
				$data['kode_band_posisi'] = $this->input->post('klas_posisi');
				$data['klas_posisi'] = $data['kode_band_posisi'];
				$data['jabatan'] = $this->input->post('jabatan');
				$data['kode_area'] = $this->input->post('area_pelayanan');

				$data['tgl_capeg'] = null;
				if (!empty($this->input->post('tgl_capeg'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_capeg'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_capeg'] = $newDate;
				}
				$data['tgl_mulai_kerja'] = null;
				if (!empty($this->input->post('tgl_mulai_kerja'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_mulai_kerja'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_mulai_kerja'] = $newDate;
				}
				$data['tgl_pensiun'] = null;
				if (!empty($this->input->post('tgl_pensiun'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_pensiun'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_pensiun'] = $newDate;
				}
				$data['tgl_berhenti_kerja'] = null;
				if (!empty($this->input->post('tgl_berhenti_kerja'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_berhenti_kerja'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_berhenti_kerja'] = $newDate;
				}

				$data['alasan_berhenti_kerja'] = $this->input->post('alasan_berhenti_kerja');
				$data['kode_pendidikan'] = $this->input->post('pendidikan');
				$data['kode_divisi'] = $this->input->post('divisi');
				$data['kode_bagian'] = $this->input->post('bagian');
				$data['kelas_perawatan'] = $this->input->post('kelas_perawatan');
				$data['kode_personal_sub_area'] = $this->input->post('personal_sub_area');

				// peserta_data_alamat
				$data['alamat_ktp'] = $this->input->post('alamat_ktp');
				$data['rt_ktp'] = $this->input->post('rt_ktp');
				$data['rw_ktp'] = $this->input->post('rw_ktp');
				$data['kode_provinsi_ktp'] = $this->input->post('provinsi_ktp');
				$data['kode_kabupaten_ktp'] = $this->input->post('kabupaten_ktp');
				$data['kode_kecamatan_ktp'] = $this->input->post('kecamatan_ktp');
				$data['kode_kelurahan_ktp'] = $this->input->post('kelurahan_ktp');
				$data['kode_pos_ktp'] = $this->input->post('kode_pos_ktp');
				$data['no_telp_ktp'] = $this->input->post('no_telp_ktp');

				$data['alamat_domisili'] = $this->input->post('alamat_domisili');
				$data['rt_domisili'] = $this->input->post('rt_domisili');
				$data['rw_domisili'] = $this->input->post('rw_domisili');
				$data['kode_provinsi_domisili'] = $this->input->post('provinsi_domisili');
				$data['kode_kabupaten_domisili'] = $this->input->post('kabupaten_domisili');
				$data['kode_kecamatan_domisili'] = $this->input->post('kecamatan_domisili');
				$data['kode_kelurahan_domisili'] = $this->input->post('kelurahan_domisili');
				$data['kode_pos_domisili'] = $this->input->post('kode_pos_domisili');
				$data['no_telp_domisili'] = $this->input->post('no_telp_domisili');

				// peserta_data_info_lain
				$data['no_hp'] = $this->input->post('no_hp');
				$data['tgl_meninggal'] = null;
				if (!empty($this->input->post('tgl_meninggal'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_meninggal'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_meninggal'] = $newDate;
				}
				$data['catatan'] = $this->input->post('catatan');

				// peserta_data_bank
				$data['nama_pemilik_rekening'] = $this->input->post('nama_pemilik_rekening');
				$data['kode_bank'] = $this->input->post('nama_bank');
				$data['no_rekening_bank'] = $this->input->post('no_rekening_bank');
				$data['status_kartu'] = $this->input->post('status_kartu');

				$data['tgl_cetak'] = null;
				if (!empty($this->input->post('tgl_cetak'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_cetak'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_cetak'] = $newDate;
				}
				$data['tgl_akhir_kartu'] = null;
				if (!empty($this->input->post('tgl_akhir_kartu'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_akhir_kartu'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_akhir_kartu'] = $newDate;
				}

				// peserta_data_pernikahan
				$data['status_pernikahan'] = $this->input->post('status_pernikahan');
				$data['tgl_nikah'] = null;
				if (!empty($this->input->post('tgl_nikah'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_nikah'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_nikah'] = $newDate;
				}
				$data['tgl_cerai'] = null;
				if (!empty($this->input->post('tgl_cerai'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_cerai'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_cerai'] = $newDate;
				}

				// peserta_data_pendukung
				$data['no_dokumen_1'] = $this->input->post('no_dokumen_1');
				$data['perihal_1'] = $this->input->post('perihal_1');
				$data['tgl_dokumen_1'] = null;
				if (!empty($this->input->post('tgl_dokumen_1'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_1'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_1'] = $newDate;
				}

				$data['no_dokumen_2'] = $this->input->post('no_dokumen_2');
				$data['perihal_2'] = $this->input->post('perihal_2');
				$data['tgl_dokumen_2'] = null;
				if (!empty($this->input->post('tgl_dokumen_2'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_2'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_2'] = $newDate;
				}

				$data['no_dokumen_3'] = $this->input->post('no_dokumen_3');
				$data['perihal_3'] = $this->input->post('perihal_3');
				$data['tgl_dokumen_3'] = null;
				if (!empty($this->input->post('tgl_dokumen_3'))) {
					$date = str_replace('/', '-', $this->input->post('tgl_dokumen_3'));
					$newDate = date('Y-m-d', strtotime($date));
					$data['tgl_dokumen_3'] = $newDate;
				}

				$data['nik_lama'] = $nik_lama;
				$data['nik_baru'] = $nik_baru;
				$data['peserta'] = $peserta;


				$data['no_dokumen_edit'] = $this->input->post('no_dokumen_edit[]');
				$data['perihal_edit'] = $this->input->post('perihal_edit[]');

				$tgl = $this->input->post('tgl_dokumen_edit[]');
				$newDate = array();

				$pendukung = $this->Peserta->getDataPendukung($kode_peserta);
				$jml_pendukung = 0;
				foreach ($pendukung as $value) {
					$jml_pendukung = $jml_pendukung + 1;
				}
				for ($i = 0; $i < $jml_pendukung ; $i++) { 
					$newDate[$i] = null;
					if (!empty($tgl[$i])) {
						$date = str_replace('/', '-', $tgl[$i]);
						$newDate[$i]  = date('Y-m-d', strtotime($date));
					}
				}

				$data['tgl_dokumen_edit'] = $newDate;

				$data['pendukung'] = $pendukung;

				$this->Peserta->editKepalaKeluarga($data);
				$this->session->berhasil_edit = 1;
				
				redirect(base_url('PesertaController/pesertaKK/'. $kode_peserta));
				}
				



				

				




			}else{
				// dont
				$this->session->gagal_edit = 1;
				redirect(base_url('PesertaController/pesertaKK/'. $kode_peserta));
			}
		} else {
			$this->load->view('error');
		}

	}

	public function dataKepesertaan()
	{
		if ($this->session->kode_admin) {
			$this->session->page = "data_kepesertaan";
			$this->session->cari_kepesertaan = 0;

			$data['peserta'] = null;
			if (!empty($_POST)) {
				$data['kode_area'] = $this->input->post('area_pelayanan');
				$data['kode_tpk'] = $this->input->post('tpk_kepesertaan');
				$data['group_status_keluarga'] = $this->input->post('group_status_keluarga');
				$data['group_jenis_peserta'] = $this->input->post('group_jenis_peserta');

				$peserta = $this->Peserta->cariDataKepesertaan($data);
				$data['peserta'] = $peserta;
				$this->session->cari_kepesertaan = 1;

				

			}
			
			$data['group_jenis_peserta'] = $this->Peserta->getAllGroupJenisPeserta();
			$data['area_pelayanan'] = $this->Area->getAllArea();
			$data['group_status_keluarga'] = $this->Peserta->getAllGroupStatusKeluarga();
			$data['personal_sub_area'] = $this->Area->getAllPersonalSubArea();
			$data['peserta_kk'] = $this->Peserta->getKKOnly();



			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			
			$admin = $this->Admin->getAdminById($this->session->kode_admin);
			$header['nama_admin'] = $admin->row()->nama_admin;
			$header['username'] = $admin->row()->username;

			$this->load->view('header', $header);
			$this->load->view('sidebar');
			$this->load->view('data_kepesertaan', $data);
			$this->load->view('footer');
		} else {
			$this->load->view('error');
		}
	}

	public function getJenisPesertaByGroup(){
		if ($this->session->kode_admin) {
			$kode_group_jenis_peserta = $this->input->post('id', TRUE);
			$data = $this->Peserta->getJenisPesertaByGroup($kode_group_jenis_peserta);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}


	public function getPersonalSubArea(){
		if ($this->session->kode_admin) {
			$kode_personal_area = $this->input->post('id', TRUE);
			$data = $this->Peserta->getPersonalSubArea($kode_personal_area);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getKlasPosisi(){
		if ($this->session->kode_admin) {
			$nama_band = $this->input->post('id', TRUE);
			$data = $this->Peserta->getKlasPosisi($nama_band);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getInstansi(){
		if ($this->session->kode_admin) {
			$kode_kelompok_peserta = $this->input->post('id', TRUE);
			$data = $this->Peserta->getInstansi($kode_kelompok_peserta);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}
	
	public function getDivisi(){
		if ($this->session->kode_admin) {
			$kode_instansi = $this->input->post('id', TRUE);
			$data = $this->Peserta->getDivisi($kode_instansi);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getKabupaten(){
		if ($this->session->kode_admin) {
			$kode_provinsi = $this->input->post('id', TRUE);
			$data = $this->Provinsi->getKabupaten($kode_provinsi);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getKecamatan(){
		if ($this->session->kode_admin) {
			$kode_kabupaten = $this->input->post('id', TRUE);
			$data = $this->Provinsi->getKecamatan($kode_kabupaten);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getKelurahan(){
		if ($this->session->kode_admin) {
			$kode_kecamatan = $this->input->post('id', TRUE);
			$data = $this->Provinsi->getKelurahan($kode_kecamatan);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getTPK(){
		if ($this->session->kode_admin) {
			$kode_area = $this->input->post('id', TRUE);
			$data = $this->TPK->getTPK($kode_area);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getTPK2(){
		if ($this->session->kode_admin) {
			$kode_tpk = $this->input->post('id', TRUE);
			$data = $this->TPK->getTPK2($kode_tpk);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}

	public function getKotaKantorByTPK(){
		if ($this->session->kode_admin) {
			$kode_tpk = $this->input->post('id', TRUE);
			$tpk = $this->TPK->getTPKById($kode_tpk);
			$kode_kota_kantor = $tpk->row()->kode_kota_kantor;
			$data = $this->TPK->getKotaKantor($kode_kota_kantor);
			echo json_encode($data);
		} else {
			$this->load->view('error');
		}
	}


}

?>