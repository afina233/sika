<?php  

class Admin extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	public function getAllAdmin()
	{
		$query = $this->db->select('*')
							->from('admin')->get();

		return $query->result();
	}

	public function getAdmin($data){
		$query = $this->db->select('*')
								->from('admin')
								->where('username', $data['username'])
								->where('password', $data['password'])->get();

		return $query;

	}

	public function getAdminById($kode_admin){

		$query = $this->db->select('*')
								->from('admin')
								->where('kode_admin', $kode_admin)->get();

		return $query;
	}

	public function getAdminByUsername($username){

		$query = $this->db->select('*')
								->from('admin')
								->where('username', $username)->get();

		return $query;
	}

	public function editAdmin($data){

		$admin = array(
			'nama_admin' => $data['nama_admin'],
			'username' => $data['username'],
			'password' => $data['password']
		);

		$this->db->where('kode_admin', $data['kode_admin']);
		$this->db->update('admin', $admin);
	}

	public function hapusAdmin($kode_admin){
		$this->db->where('kode_admin', $kode_admin);
 		$this->db-> delete('admin');
	}

	public function inputAdmin($data){
		$admin = array(
			'nama_admin' => $data['nama_admin'],
			'username' => $data['username'],
			'password' => $data['password']
		);
		$this->db->insert('admin', $admin);

	}





}

?>