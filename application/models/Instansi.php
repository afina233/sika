<?php  

class Instansi extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	public function getAllInstansi()
	{
		$query = $this->db->select('*')
							->from('instansi')->get();

		return $query->result();
	}

	public function getAllDivisi(){
		$query = $this->db->select('*')
							->from('divisi')->get();

		return $query->result();

	}

	public function getAllBagian(){
		$query = $this->db->select('*')
							->from('bagian')->get();

		return $query->result();
		
	}

	public function getAllKlasPosisi(){
		$query = $this->db->select('*')
							->from('band_posisi')->get();

		return $query->result();
	}

}


?>