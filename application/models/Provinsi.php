<?php  

class Provinsi extends CI_Model{

	function __construct()
	{
		parent::__construct();
	}

	public function getAllProvinsi()
	{
		$query = $this->db->get('provinces');
        return $query->result();

	}

	public function getAllKabupaten()
	{
		$query = $this->db->get('regencies');
        return $query->result();

	}

	public function getAllKecamatan()
	{
		$query = $this->db->get('districts');
        return $query->result();

	}

	public function getAllKelurahan()
	{
		$query = $this->db->get('villages');
        return $query->result();

	}

	public function getKabupaten($kode_provinsi)
	{
		$query = $this->db->get_where('regencies', array('province_id' => $kode_provinsi));

		return $query->result();
	}

	public function getKecamatan($kode_kabupaten)
	{
		$query = $this->db->get_where('districts', array('regency_id' => $kode_kabupaten));

		return $query->result();
	}

	public function getKelurahan($kode_kecamatan)
	{
		$query = $this->db->get_where('villages', array('district_id' => $kode_kecamatan));

		return $query->result();
	}


}


?>