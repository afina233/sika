<?php  

class Area extends CI_Model{

	function __construct()
	{
		parent::__construct();
	}

	public function getAllArea()
	{
		$query = $this->db->select('*')
							->from('area')->get();

		return $query->result();
	}

	public function getAllPersonalArea()
	{
		$query = $this->db->select('*')
							->from('personal_area')->get();

		return $query->result();
	}

	public function getAllPersonalSubArea()
	{
		$query = $this->db->select('*')
							->from('personal_sub_area')->get();

		return $query->result();
	}

	public function getArea($kode_area){
		$query = $this->db->select('*')
								->from('area')
								->where('kode_area', $kode_area)->get();

		return $query;
	}

	public function getAllKotaKantor()
	{
		$query = $this->db->select('*')
							->from('kota_kantor')->get();

		return $query->result();
	}

}


?>