<?php  

class Peserta extends CI_Model{

	public $kode_peserta = "";
	public $no_peserta = "";
	public $kode_nik = "";
	public $nikes = "";
	public $status_faskes = "";
	public $tgl_faskes = "";
	public $kode_data_pribadi = "";
	public $kode_data_tpk = "";
	public $kode_data_kepegawaian = "";
	public $kode_data_info_lain = "";
	public $kode_jenis_peserta = "";
	public $kode_alamat_ktp = "";
	public $kode_alamat_domisili = "";
	public $kode_status_pernikahan = "";
	public $kode_data_bank = "";

	function __construct(){
		parent::__construct();

		$this->load->helper('url');

	}

	public function getKKOnly(){

		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->join('nik', 'nik.kode_nik = peserta.kode_nik', 'left');
        $this->db->join('peserta_data_pribadi', 'peserta_data_pribadi.kode_data_pribadi = peserta.kode_data_pribadi', 'left');
        $this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
        $this->db->where('kode_group_status_keluarga', '1');

        $query = $this->db->get();

        $temp = $query->result();

        return $temp;



	}

	public function getKKByNik($kode_nik){

		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
        $this->db->where('kode_group_status_keluarga', '1');
        $this->db->where('kode_nik', $kode_nik);

        $query = $this->db->get();

        $temp = $query->result();

        return $temp;



	}

	public function getPesertaKK(){
		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
        $this->db->where('kode_group_status_keluarga', '1');

        $query = $this->db->get();

        $temp = $query->result();

        return $temp;

	}


	public function getPesertaPasangan(){
		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
        $this->db->where('kode_group_status_keluarga', '2');

        $query = $this->db->get();

        $temp = $query->result();

        return $temp;

	}

	

	public function getPesertaAnak(){
		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
        $this->db->where('kode_group_status_keluarga', '3');

        $query = $this->db->get();

        $temp = $query->result();

        return $temp;

	}

	

	public function getPesertaJandaDuda(){
		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
        $this->db->where('kode_group_status_keluarga', '4');

        $query = $this->db->get();

        $temp = $query->result();

        return $temp;

	}

	public function mutasiTPK($data){

		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->join('peserta_data_tpk', 'peserta_data_tpk.kode_data_tpk = peserta.kode_data_tpk', 'left');
        $this->db->where('kode_tpk', $data['tpk_asal']);
        $query = $this->db->get();
        $tpk_asal = $query->result();

        if (!empty($tpk_asal)) {
	        $kode_peserta = array();
	        
	        foreach ($tpk_asal as $value) {
	        	$peserta_data_tpk = array('kode_tpk' => $data['tpk_tujuan']);

	        	$this->db->where('kode_data_tpk', $value->kode_data_tpk);
				$this->db->update('peserta_data_tpk', $peserta_data_tpk);

	        }
			$this->session->mutasi = 1;

        }else{
			$this->session->mutasi = 2;

        }


	}

	public function cariDataKepesertaan($data){
		$this->db->select('*');
        $this->db->from('peserta');
         $this->db->join('nik', 'nik.kode_nik = peserta.kode_nik', 'left');
        $this->db->join('peserta_data_pribadi', 'peserta_data_pribadi.kode_data_pribadi = peserta.kode_data_pribadi', 'left');
        $this->db->join('peserta_data_kepegawaian', 'peserta_data_kepegawaian.kode_data_kepegawaian = peserta.kode_data_kepegawaian', 'left');
        $this->db->join('peserta_data_tpk', 'peserta_data_tpk.kode_data_tpk = peserta.kode_data_tpk', 'left');
        $this->db->join('peserta_data_info_lain', 'peserta_data_info_lain.kode_data_info_lain = peserta.kode_data_info_lain', 'left');
        $this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
        $this->db->like('kode_area', $data['kode_area']);
        $this->db->like('kode_tpk', $data['kode_tpk']);
        $this->db->like('kode_group_status_keluarga', $data['group_status_keluarga']);
        $this->db->like('kode_group_jenis_peserta', $data['group_jenis_peserta']);
        $query = $this->db->get();

        $temp = $query->result();

        return $temp;

   
        




	}

	public function cariPeserta($data){

		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->join('nik', 'nik.kode_nik = peserta.kode_nik', 'left');
        $this->db->join('peserta_data_pribadi', 'peserta_data_pribadi.kode_data_pribadi = peserta.kode_data_pribadi', 'left');
        $this->db->join('peserta_data_kepegawaian', 'peserta_data_kepegawaian.kode_data_kepegawaian = peserta.kode_data_kepegawaian', 'left');
        $this->db->join('peserta_data_tpk', 'peserta_data_tpk.kode_data_tpk = peserta.kode_data_tpk', 'left');
        $this->db->like('nik', $data['nik'], 'both');
        $this->db->like('kode_jenis_peserta', $data['jenis_peserta']);
        $this->db->like('no_peserta', $data['no_peserta'], 'both');
        $this->db->like('nama', $data['nama'], 'both');
        $this->db->like('no_ktp', $data['nikep'], 'both');
        $this->db->like('no_bpjs', $data['no_bpjs'], 'both');
        $this->db->like('kode_area', $data['kode_area']);
        $this->db->like('kode_tpk', $data['kode_tpk']);
        $query = $this->db->get();

        $temp = $query->result();

  
        return $temp;


	}


	public function inputAnak($data){
		// transaksi
		$transaksi = array('kode_kelompok_transaksi' => $data['kelompok_transaksi'],
			'kode_jenis_transaksi' => $data['jenis_transaksi'],
			'nik' => $data['nik'],
			'nikes' => $data['nikes'],
			'kode_admin' => $this->session->kode_admin,
			'waktu' => $data['waktu']);

		$nik = $this->Peserta->getNik($data['nik']);
		$kode_nik = $nik->row()->kode_nik;

		// peserta_data_pribadi
		$peserta_data_pribadi = array(
			'nama' => $data['nama'],
			'tempat_lahir' => $data['tempat_lahir'],
			'tgl_lahir' => $data['tgl_lahir'],
			'jenis_kelamin' => $data['jenis_kelamin'],
			'kode_agama' => $data['agama'],
			'no_ktp' => $data['no_ktp'],
			'gol_darah' => $data['gol_darah'],
			'rhesus' => $data['rhesus'],
			'foto' => $data['foto'],
			'no_bpjs' => $data['no_bpjs']);

		// peserta_data_tpk
		$peserta_data_tpk = array(
			'kode_tpk' => $data['kode_tpk']);

		// peserta_data_kepegawaian
		$peserta_data_kepegawaian = array(
			'kode_area' => $data['kode_area'],
			'kode_band_posisi' => $data['kode_band_posisi'],
			'kode_personal_sub_area' => $data['kode_personal_sub_area']
		);

		// peserta_data_alamat (ktp)
		$peserta_data_alamat_ktp = array(
			'jenis_alamat' => "ktp",
			'alamat' => $data['alamat_ktp'],
			'rt' => $data['rt_ktp'],
			'rw' => $data['rw_ktp'],
			'kode_provinsi' => $data['kode_provinsi_ktp'],
			'kode_kabupaten' => $data['kode_kabupaten_ktp'],
			'kode_kecamatan' => $data['kode_kecamatan_ktp'],
			'kode_kelurahan' => $data['kode_kelurahan_ktp'],
			'kode_pos' => $data['kode_pos_ktp'],
			'no_telp' => $data['no_telp_ktp']
		);

		// peserta_data_alamat (domisili)
		$peserta_data_alamat_domisili = array(
			'jenis_alamat' => "domisili",
			'alamat' => $data['alamat_domisili'],
			'rt' => $data['rt_domisili'],
			'rw' => $data['rw_domisili'],
			'kode_provinsi' => $data['kode_provinsi_domisili'],
			'kode_kabupaten' => $data['kode_kabupaten_domisili'],
			'kode_kecamatan' => $data['kode_kecamatan_domisili'],
			'kode_kelurahan' => $data['kode_kelurahan_domisili'],
			'kode_pos' => $data['kode_pos_domisili'],
			'no_telp' => $data['no_telp_domisili']
		);

		// peserta_data_info_lain
		$peserta_data_info_lain = array(
			'no_hp' => $data['no_hp'],
			'tgl_meninggal' => $data['tgl_meninggal'],
			'alasan_berhenti_peserta' => $data['alasan_berhenti_peserta'],
			'no_anggota_keluarga' => $data['no_anggota_keluarga'],
			'no_anggota_keluarga_2' => $data['no_anggota_keluarga_2'],
			'catatan' => $data['catatan']
		);

		// peserta_data_bank
		$peserta_data_bank = array(
			'nama_pemilik_rekening' => $data['nama_pemilik_rekening'],
			'kode_bank' => $data['kode_bank'],
			'no_rekening' => $data['no_rekening_bank'],
			'status_kartu' => $data['status_kartu'],
			'tgl_cetak' => $data['tgl_cetak'],
			'tgl_akhir_kartu' => $data['tgl_akhir_kartu']
		);


		$this->db->insert('transaksi', $transaksi);
		$this->db->insert('peserta_data_pribadi', $peserta_data_pribadi);
		$this->db->insert('peserta_data_tpk', $peserta_data_tpk);
		$this->db->insert('peserta_data_kepegawaian', $peserta_data_kepegawaian);
		$this->db->insert('peserta_data_alamat', $peserta_data_alamat_ktp);
		$this->db->insert('peserta_data_alamat', $peserta_data_alamat_domisili);
		$this->db->insert('peserta_data_info_lain', $peserta_data_info_lain);
		$this->db->insert('peserta_data_bank', $peserta_data_bank);

		$kode_data_pribadi = $this->db->select('*')
			->from('peserta_data_pribadi')
			->order_by('kode_data_pribadi',"desc")
			->limit(1)->get();

		$kode_data_tpk = $this->db->select('*')
			->from('peserta_data_tpk')
			->order_by('kode_data_tpk',"desc")
			->limit(1)->get();

		$kode_data_alamat_ktp = $this->db->select('*')
			->from('peserta_data_alamat')
			->where('jenis_alamat', "ktp")
			->order_by('kode_alamat',"desc")
			->limit(1)->get();

		$kode_data_alamat_domisili = $this->db->select('*')
			->from('peserta_data_alamat')
			->where('jenis_alamat', "domisili")
			->order_by('kode_alamat',"desc")
			->limit(1)->get();

		$kode_data_kepegawaian = $this->db->select('*')
			->from('peserta_data_kepegawaian')
			->order_by('kode_data_kepegawaian',"desc")
			->limit(1)->get();

		$kode_data_info_lain = $this->db->select('*')
			->from('peserta_data_info_lain')
			->order_by('kode_data_info_lain',"desc")
			->limit(1)->get();

		$kode_data_bank = $this->db->select('*')
			->from('peserta_data_bank')
			->order_by('kode_data_bank',"desc")
			->limit(1)->get();

			$data_peserta = array(
			'no_peserta' => "TLK" . $data['nik'] . $data['pasangan_ke'] . "0" . $data['anak_ke'],
			'kode_nik' => $kode_nik,
			'nikes' => $data['nikes'],
			'kode_jenis_peserta' => $data['jenis_peserta'],
			'kode_data_pribadi' => $kode_data_pribadi->row()->kode_data_pribadi,
			'kode_data_tpk' => $kode_data_tpk->row()->kode_data_tpk,
			'kode_data_kepegawaian' => $kode_data_kepegawaian->row()->kode_data_kepegawaian,
			'kode_data_info_lain' => $kode_data_info_lain->row()->kode_data_info_lain,
			'kode_alamat_ktp' => $kode_data_alamat_ktp->row()->kode_alamat,
			'kode_alamat_domisili' => $kode_data_alamat_domisili->row()->kode_alamat,
			'kode_data_bank' => $kode_data_bank->row()->kode_data_bank,
			'status_faskes' => $data['status_faskes'],
			'tgl_faskes' => $data['tgl_faskes']
		);

		$this->db->insert('peserta', $data_peserta);

		$kode_peserta = $this->db->select('*')
			->from('peserta')
			->order_by('kode_peserta',"desc")
			->limit(1)->get();

		// peserta_data_pendukung
		if ($data['file_1'] != "") {
			$peserta_data_pendukung_1 = array(
				'no_dokumen' => $data['no_dokumen_1'],
				'perihal' => $data['perihal_1'],
				'tgl_dokumen' => $data['tgl_dokumen_1'],
				'file' => $data['file_1'],
				'kode_peserta' => $kode_peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_1);

		}

		if ($data['file_2'] != "") {
			$peserta_data_pendukung_2 = array(
				'no_dokumen' => $data['no_dokumen_2'],
				'perihal' => $data['perihal_2'],
				'tgl_dokumen' => $data['tgl_dokumen_2'],
				'file' => $data['file_2'],
				'kode_peserta' => $kode_peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_2);

		}

		if ($data['file_3'] != "") {
			$peserta_data_pendukung_3 = array(
				'no_dokumen' => $data['no_dokumen_3'],
				'perihal' => $data['perihal_3'],
				'tgl_dokumen' => $data['tgl_dokumen_3'],
				'file' => $data['file_3'],
				'kode_peserta' => $kode_peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_3);

		}



	}	

	public function inputPasangan($data){
		// transaksi
		$transaksi = array('kode_kelompok_transaksi' => $data['kelompok_transaksi'],
			'kode_jenis_transaksi' => $data['jenis_transaksi'],
			'nik' => $data['nik'],
			'nikes' => $data['nikes'],
			'kode_admin' => $this->session->kode_admin,
			'waktu' => $data['waktu']);

		$nik = $this->Peserta->getNik($data['nik']);
		$kode_nik = $nik->row()->kode_nik;

		// peserta_data_pribadi
		$peserta_data_pribadi = array(
			'nama' => $data['nama'],
			'tempat_lahir' => $data['tempat_lahir'],
			'tgl_lahir' => $data['tgl_lahir'],
			'jenis_kelamin' => $data['jenis_kelamin'],
			'kode_agama' => $data['agama'],
			'no_ktp' => $data['no_ktp'],
			'gol_darah' => $data['gol_darah'],
			'rhesus' => $data['rhesus'],
			'foto' => $data['foto'],
			'no_bpjs' => $data['no_bpjs']);

		// peserta_data_tpk
		$peserta_data_tpk = array(
			'kode_tpk' => $data['kode_tpk']);

		// peserta_data_kepegawaian
		$peserta_data_kepegawaian = array(
			'nip' => $data['nip_pasangan'],
			'kode_pendidikan' => $data['kode_pendidikan'],
			'kode_area' => $data['kode_area'],
			'kode_band_posisi' => $data['kode_band_posisi'],
			'kode_personal_sub_area' => $data['kode_personal_sub_area'],
			'kode_pekerjaan' => $data['kode_pekerjaan']
		);

		// peserta_data_alamat (ktp)
		$peserta_data_alamat_ktp = array(
			'jenis_alamat' => "ktp",
			'alamat' => $data['alamat_ktp'],
			'rt' => $data['rt_ktp'],
			'rw' => $data['rw_ktp'],
			'kode_provinsi' => $data['kode_provinsi_ktp'],
			'kode_kabupaten' => $data['kode_kabupaten_ktp'],
			'kode_kecamatan' => $data['kode_kecamatan_ktp'],
			'kode_kelurahan' => $data['kode_kelurahan_ktp'],
			'kode_pos' => $data['kode_pos_ktp'],
			'no_telp' => $data['no_telp_ktp']
		);

		// peserta_data_alamat (domisili)
		$peserta_data_alamat_domisili = array(
			'jenis_alamat' => "domisili",
			'alamat' => $data['alamat_domisili'],
			'rt' => $data['rt_domisili'],
			'rw' => $data['rw_domisili'],
			'kode_provinsi' => $data['kode_provinsi_domisili'],
			'kode_kabupaten' => $data['kode_kabupaten_domisili'],
			'kode_kecamatan' => $data['kode_kecamatan_domisili'],
			'kode_kelurahan' => $data['kode_kelurahan_domisili'],
			'kode_pos' => $data['kode_pos_domisili'],
			'no_telp' => $data['no_telp_domisili']
		);

		// peserta_data_info_lain
		$peserta_data_info_lain = array(
			'no_hp' => $data['no_hp'],
			'tgl_meninggal' => $data['tgl_meninggal'],
			'alasan_berhenti_peserta' => $data['alasan_berhenti_peserta'],
			'no_anggota_keluarga' => $data['no_anggota_keluarga'],
			'catatan' => $data['catatan']
		);

		// peserta_data_bank
		$peserta_data_bank = array(
			'nama_pemilik_rekening' => $data['nama_pemilik_rekening'],
			'kode_bank' => $data['kode_bank'],
			'no_rekening' => $data['no_rekening_bank'],
			'status_kartu' => $data['status_kartu'],
			'tgl_cetak' => $data['tgl_cetak'],
			'tgl_akhir_kartu' => $data['tgl_akhir_kartu']
		);

		// peserta_data_pernikahan
		$peserta_data_pernikahan = array(
			'status_pernikahan' => $data['status_pernikahan'],
			'tgl_nikah' => $data['tgl_nikah'],
			'tgl_cerai' => $data['tgl_cerai']
		);

		$this->db->insert('transaksi', $transaksi);
		$this->db->insert('peserta_data_pribadi', $peserta_data_pribadi);
		$this->db->insert('peserta_data_tpk', $peserta_data_tpk);
		$this->db->insert('peserta_data_kepegawaian', $peserta_data_kepegawaian);
		$this->db->insert('peserta_data_alamat', $peserta_data_alamat_ktp);
		$this->db->insert('peserta_data_alamat', $peserta_data_alamat_domisili);
		$this->db->insert('peserta_data_info_lain', $peserta_data_info_lain);
		$this->db->insert('peserta_data_bank', $peserta_data_bank);
		$this->db->insert('peserta_data_pernikahan', $peserta_data_pernikahan);

		$kode_data_pribadi = $this->db->select('*')
			->from('peserta_data_pribadi')
			->order_by('kode_data_pribadi',"desc")
			->limit(1)->get();

		$kode_data_tpk = $this->db->select('*')
			->from('peserta_data_tpk')
			->order_by('kode_data_tpk',"desc")
			->limit(1)->get();

		$kode_data_alamat_ktp = $this->db->select('*')
			->from('peserta_data_alamat')
			->where('jenis_alamat', "ktp")
			->order_by('kode_alamat',"desc")
			->limit(1)->get();

		$kode_data_alamat_domisili = $this->db->select('*')
			->from('peserta_data_alamat')
			->where('jenis_alamat', "domisili")
			->order_by('kode_alamat',"desc")
			->limit(1)->get();

		$kode_data_kepegawaian = $this->db->select('*')
			->from('peserta_data_kepegawaian')
			->order_by('kode_data_kepegawaian',"desc")
			->limit(1)->get();

		$kode_data_info_lain = $this->db->select('*')
			->from('peserta_data_info_lain')
			->order_by('kode_data_info_lain',"desc")
			->limit(1)->get();

		$kode_data_bank = $this->db->select('*')
			->from('peserta_data_bank')
			->order_by('kode_data_bank',"desc")
			->limit(1)->get();

		$kode_data_pernikahan = $this->db->select('*')
			->from('peserta_data_pernikahan')
			->order_by('kode_data_pernikahan',"desc")
			->limit(1)->get();

		$data_peserta = array(
			'no_peserta' => "TLK" . $data['nik'] . $data['pasangan_ke'] . "00",
			'kode_nik' => $kode_nik,
			'nikes' => $data['nik'] . ".".$data['pasangan_ke']."00",
			'kode_jenis_peserta' => $data['jenis_peserta'],
			'kode_data_pribadi' => $kode_data_pribadi->row()->kode_data_pribadi,
			'kode_data_tpk' => $kode_data_tpk->row()->kode_data_tpk,
			'kode_data_kepegawaian' => $kode_data_kepegawaian->row()->kode_data_kepegawaian,
			'kode_data_info_lain' => $kode_data_info_lain->row()->kode_data_info_lain,
			'kode_alamat_ktp' => $kode_data_alamat_ktp->row()->kode_alamat,
			'kode_alamat_domisili' => $kode_data_alamat_domisili->row()->kode_alamat,
			'kode_status_pernikahan' => $kode_data_pernikahan->row()->kode_data_pernikahan,
			'kode_data_bank' => $kode_data_bank->row()->kode_data_bank,
			'status_faskes' => $data['status_faskes'],
			'tgl_faskes' => $data['tgl_faskes']
		);

		$this->db->insert('peserta', $data_peserta);

		$kode_peserta = $this->db->select('*')
			->from('peserta')
			->order_by('kode_peserta',"desc")
			->limit(1)->get();

		// peserta_data_pendukung
		if ($data['file_1'] != "") {
			$peserta_data_pendukung_1 = array(
				'no_dokumen' => $data['no_dokumen_1'],
				'perihal' => $data['perihal_1'],
				'tgl_dokumen' => $data['tgl_dokumen_1'],
				'file' => $data['file_1'],
				'kode_peserta' => $kode_peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_1);

		}

		if ($data['file_2'] != "") {
			$peserta_data_pendukung_2 = array(
				'no_dokumen' => $data['no_dokumen_2'],
				'perihal' => $data['perihal_2'],
				'tgl_dokumen' => $data['tgl_dokumen_2'],
				'file' => $data['file_2'],
				'kode_peserta' => $kode_peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_2);

		}

		if ($data['file_3'] != "") {
			$peserta_data_pendukung_3 = array(
				'no_dokumen' => $data['no_dokumen_3'],
				'perihal' => $data['perihal_3'],
				'tgl_dokumen' => $data['tgl_dokumen_3'],
				'file' => $data['file_3'],
				'kode_peserta' => $kode_peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_3);

		}


	}

	public function inputKepalaKeluarga($data)
	{
		// transaksi
		$transaksi = array('kode_kelompok_transaksi' => $data['kelompok_transaksi'],
			'kode_jenis_transaksi' => $data['jenis_transaksi'],
			'nik' => $data['nik'],
			'nikes' => $data['nikes'],
			'kode_admin' => $this->session->kode_admin,
			'waktu' => $data['waktu']);


		// nik
		$nik = array('nik' => $data['nik']);

		// peserta_data_pribadi
		$peserta_data_pribadi = array(
			'nama' => $data['nama'],
			'tempat_lahir' => $data['tempat_lahir'],
			'tgl_lahir' => $data['tgl_lahir'],
			'jenis_kelamin' => $data['jenis_kelamin'],
			'kode_agama' => $data['kode_agama'],
			'no_ktp' => $data['no_ktp'],
			'gol_darah' => $data['gol_darah'],
			'rhesus' => $data['rhesus'],
			'foto' => $data['foto'],
			'no_bpjs' => $data['no_bpjs']);

		// peserta_data_tpk
		$peserta_data_tpk = array(
			'kode_tpk' => $data['kode_tpk']);
			

		// peserta_data_kepegawaian
		$peserta_data_kepegawaian = array(
			'kode_kelompok_peserta' => $data['kode_kelompok_peserta'],
			'kode_instansi' => $data['kode_instansi'],
			'kode_band_posisi' => $data['kode_band_posisi'],
			'klas_posisi' => $data['klas_posisi'],
			'jabatan' => $data['jabatan'],
			'kode_area' => $data['kode_area'],
			'tgl_capeg' => $data['tgl_capeg'],
			'tgl_mulai_kerja' => $data['tgl_mulai_kerja'],
			'tgl_pensiun' => $data['tgl_pensiun'],
			'tgl_berhenti_kerja' => $data['tgl_berhenti_kerja'],
			'alasan_berhenti_kerja' => $data['alasan_berhenti_kerja'],
			'kode_pendidikan' => $data['kode_pendidikan'],
			'kode_divisi' => $data['kode_divisi'],
			'kode_bagian' => $data['kode_bagian'],
			'kelas_perawatan' => $data['kelas_perawatan'],
			'kode_personal_sub_area' => $data['kode_personal_sub_area']
		);

		// peserta_data_alamat (ktp)
		$peserta_data_alamat_ktp = array(
			'jenis_alamat' => "ktp",
			'alamat' => $data['alamat_ktp'],
			'rt' => $data['rt_ktp'],
			'rw' => $data['rw_ktp'],
			'kode_provinsi' => $data['kode_provinsi_ktp'],
			'kode_kabupaten' => $data['kode_kabupaten_ktp'],
			'kode_kecamatan' => $data['kode_kecamatan_ktp'],
			'kode_kelurahan' => $data['kode_kelurahan_ktp'],
			'kode_pos' => $data['kode_pos_ktp'],
			'no_telp' => $data['no_telp_ktp']
		);

		// peserta_data_alamat (domisili)
		$peserta_data_alamat_domisili = array(
			'jenis_alamat' => "domisili",
			'alamat' => $data['alamat_domisili'],
			'rt' => $data['rt_domisili'],
			'rw' => $data['rw_domisili'],
			'kode_provinsi' => $data['kode_provinsi_domisili'],
			'kode_kabupaten' => $data['kode_kabupaten_domisili'],
			'kode_kecamatan' => $data['kode_kecamatan_domisili'],
			'kode_kelurahan' => $data['kode_kelurahan_domisili'],
			'kode_pos' => $data['kode_pos_domisili'],
			'no_telp' => $data['no_telp_domisili']
		);

		// peserta_data_info_lain
		$peserta_data_info_lain = array(
			'no_hp' => $data['no_hp'],
			'tgl_meninggal' => $data['tgl_meninggal'],
			'catatan' => $data['catatan']
		);

		// peserta_data_bank
		$peserta_data_bank = array(
			'nama_pemilik_rekening' => $data['nama_pemilik_rekening'],
			'kode_bank' => $data['kode_bank'],
			'no_rekening' => $data['no_rekening_bank'],
			'status_kartu' => $data['status_kartu'],
			'tgl_cetak' => $data['tgl_cetak'],
			'tgl_akhir_kartu' => $data['tgl_akhir_kartu']
		);

		// peserta_data_pernikahan
		$peserta_data_pernikahan = array(
			'status_pernikahan' => $data['status_pernikahan'],
			'tgl_nikah' => $data['tgl_nikah'],
			'tgl_cerai' => $data['tgl_cerai']
		);

		$this->db->insert('transaksi', $transaksi);
		$this->db->insert('nik', $nik);
		$this->db->insert('peserta_data_pribadi', $peserta_data_pribadi);
		$this->db->insert('peserta_data_tpk', $peserta_data_tpk);
		$this->db->insert('peserta_data_kepegawaian', $peserta_data_kepegawaian);
		$this->db->insert('peserta_data_alamat', $peserta_data_alamat_ktp);
		$this->db->insert('peserta_data_alamat', $peserta_data_alamat_domisili);
		$this->db->insert('peserta_data_info_lain', $peserta_data_info_lain);
		$this->db->insert('peserta_data_bank', $peserta_data_bank);
		$this->db->insert('peserta_data_pernikahan', $peserta_data_pernikahan);

		$kode_nik = $this->db->select('*')
			->from('nik')
			->order_by('kode_nik',"desc")
			->limit(1)->get();

		$kode_data_pribadi = $this->db->select('*')
			->from('peserta_data_pribadi')
			->order_by('kode_data_pribadi',"desc")
			->limit(1)->get();

		$kode_data_tpk = $this->db->select('*')
			->from('peserta_data_tpk')
			->order_by('kode_data_tpk',"desc")
			->limit(1)->get();

		$kode_data_alamat_ktp = $this->db->select('*')
			->from('peserta_data_alamat')
			->where('jenis_alamat', "ktp")
			->order_by('kode_alamat',"desc")
			->limit(1)->get();

		$kode_data_alamat_domisili = $this->db->select('*')
			->from('peserta_data_alamat')
			->where('jenis_alamat', "domisili")
			->order_by('kode_alamat',"desc")
			->limit(1)->get();

		$kode_data_kepegawaian = $this->db->select('*')
			->from('peserta_data_kepegawaian')
			->order_by('kode_data_kepegawaian',"desc")
			->limit(1)->get();

		$kode_data_info_lain = $this->db->select('*')
			->from('peserta_data_info_lain')
			->order_by('kode_data_info_lain',"desc")
			->limit(1)->get();

		$kode_data_bank = $this->db->select('*')
			->from('peserta_data_bank')
			->order_by('kode_data_bank',"desc")
			->limit(1)->get();

		$kode_data_pernikahan = $this->db->select('*')
			->from('peserta_data_pernikahan')
			->order_by('kode_data_pernikahan',"desc")
			->limit(1)->get();

		$data_peserta = array(
			'no_peserta' => "TLK" . $data['nik'] . "000",
			'kode_nik' => $kode_nik->row()->kode_nik,
			'nikes' => $data['nik'] . ".000",
			'kode_jenis_peserta' => $data['jenis_peserta'],
			'kode_data_pribadi' => $kode_data_pribadi->row()->kode_data_pribadi,
			'kode_data_tpk' => $kode_data_tpk->row()->kode_data_tpk,
			'kode_data_kepegawaian' => $kode_data_kepegawaian->row()->kode_data_kepegawaian,
			'kode_data_info_lain' => $kode_data_info_lain->row()->kode_data_info_lain,
			'kode_alamat_ktp' => $kode_data_alamat_ktp->row()->kode_alamat,
			'kode_alamat_domisili' => $kode_data_alamat_domisili->row()->kode_alamat,
			'kode_status_pernikahan' => $kode_data_pernikahan->row()->kode_data_pernikahan,
			'kode_data_bank' => $kode_data_bank->row()->kode_data_bank,
			'status_faskes' => $data['status_faskes'],
			'tgl_faskes' => $data['tgl_faskes']
		);
	
		$this->db->insert('peserta', $data_peserta);

		$kode_peserta = $this->db->select('*')
			->from('peserta')
			->order_by('kode_peserta',"desc")
			->limit(1)->get();

		// peserta_data_pendukung
		if ($data['file_1'] != "") {
			$peserta_data_pendukung_1 = array(
				'no_dokumen' => $data['no_dokumen_1'],
				'perihal' => $data['perihal_1'],
				'tgl_dokumen' => $data['tgl_dokumen_1'],
				'file' => $data['file_1'],
				'kode_peserta' => $kode_peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_1);

		}

		if ($data['file_2'] != "") {
			$peserta_data_pendukung_2 = array(
				'no_dokumen' => $data['no_dokumen_2'],
				'perihal' => $data['perihal_2'],
				'tgl_dokumen' => $data['tgl_dokumen_2'],
				'file' => $data['file_2'],
				'kode_peserta' => $kode_peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_2);

		}

		if ($data['file_3'] != "") {
			$peserta_data_pendukung_3 = array(
				'no_dokumen' => $data['no_dokumen_3'],
				'perihal' => $data['perihal_3'],
				'tgl_dokumen' => $data['tgl_dokumen_3'],
				'file' => $data['file_3'],
				'kode_peserta' => $kode_peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_3);

		}



	}

	public function editAnak($data){
		$peserta = $data['peserta'];

		// transaksi
		$transaksi = array('kode_kelompok_transaksi' => $data['kelompok_transaksi'],
			'kode_jenis_transaksi' => $data['jenis_transaksi'],
			'nik' => $data['nik'],
			'nikes' => $data['nikes'],
			'kode_admin' => $this->session->kode_admin,
			'waktu' => $data['waktu']);

		$this->db->insert('transaksi', $transaksi);

		// peserta_data_pribadi
		$peserta_data_pribadi = array(
			'nama' => $data['nama'],
			'tempat_lahir' => $data['tempat_lahir'],
			'tgl_lahir' => $data['tgl_lahir'],
			'jenis_kelamin' => $data['jenis_kelamin'],
			'kode_agama' => $data['agama'],
			'no_ktp' => $data['no_ktp'],
			'gol_darah' => $data['gol_darah'],
			'rhesus' => $data['rhesus'],
			'foto' => $data['foto'],
			'no_bpjs' => $data['no_bpjs']);

		$this->db->where('kode_data_pribadi', $peserta->row()->kode_data_pribadi);
		$this->db->update('peserta_data_pribadi', $peserta_data_pribadi);

		// peserta_data_tpk
		$peserta_data_tpk = array(
			'kode_tpk' => $data['kode_tpk']);

		$this->db->where('kode_data_tpk', $peserta->row()->kode_data_tpk);
		$this->db->update('peserta_data_tpk', $peserta_data_tpk);

		// peserta_data_kepegawaian
		$peserta_data_kepegawaian = array(
			'kode_area' => $data['kode_area'],
			'kode_band_posisi' => $data['kode_band_posisi'],
			'kode_personal_sub_area' => $data['kode_personal_sub_area']
		);

		$this->db->where('kode_data_kepegawaian', $peserta->row()->kode_data_kepegawaian);
		$this->db->update('peserta_data_kepegawaian', $peserta_data_kepegawaian);

		// peserta_data_alamat (ktp)
		$peserta_data_alamat_ktp = array(
			'jenis_alamat' => "ktp",
			'alamat' => $data['alamat_ktp'],
			'rt' => $data['rt_ktp'],
			'rw' => $data['rw_ktp'],
			'kode_provinsi' => $data['kode_provinsi_ktp'],
			'kode_kabupaten' => $data['kode_kabupaten_ktp'],
			'kode_kecamatan' => $data['kode_kecamatan_ktp'],
			'kode_kelurahan' => $data['kode_kelurahan_ktp'],
			'kode_pos' => $data['kode_pos_ktp'],
			'no_telp' => $data['no_telp_ktp']
		);

		$this->db->where('kode_alamat', $peserta->row()->kode_alamat_ktp);
		$this->db->update('peserta_data_alamat', $peserta_data_alamat_ktp);

		// peserta_data_alamat (domisili)
		$peserta_data_alamat_domisili = array(
			'jenis_alamat' => "domisili",
			'alamat' => $data['alamat_domisili'],
			'rt' => $data['rt_domisili'],
			'rw' => $data['rw_domisili'],
			'kode_provinsi' => $data['kode_provinsi_domisili'],
			'kode_kabupaten' => $data['kode_kabupaten_domisili'],
			'kode_kecamatan' => $data['kode_kecamatan_domisili'],
			'kode_kelurahan' => $data['kode_kelurahan_domisili'],
			'kode_pos' => $data['kode_pos_domisili'],
			'no_telp' => $data['no_telp_domisili']
		);

		$this->db->where('kode_alamat', $peserta->row()->kode_alamat_domisili);
		$this->db->update('peserta_data_alamat', $peserta_data_alamat_domisili);

		// peserta_data_info_lain
		$peserta_data_info_lain = array(
			'no_hp' => $data['no_hp'],
			'tgl_meninggal' => $data['tgl_meninggal'],
			'status_meninggal' => $data['status_meninggal'],
			'catatan' => $data['catatan'],
			'alasan_berhenti_peserta' => $data['alasan_berhenti_peserta']
		);

		$this->db->where('kode_data_info_lain', $peserta->row()->kode_data_info_lain);
		$this->db->update('peserta_data_info_lain', $peserta_data_info_lain);

		// peserta_data_bank
		$peserta_data_bank = array(
			'nama_pemilik_rekening' => $data['nama_pemilik_rekening'],
			'kode_bank' => $data['kode_bank'],
			'no_rekening' => $data['no_rekening_bank'],
			'status_kartu' => $data['status_kartu'],
			'tgl_cetak' => $data['tgl_cetak'],
			'tgl_akhir_kartu' => $data['tgl_akhir_kartu']
		);

		$this->db->where('kode_data_bank', $peserta->row()->kode_data_bank);
		$this->db->update('peserta_data_bank', $peserta_data_bank);

		$nik = $this->Peserta->getNik($data['nik']);

		$kode_nik = $nik->row()->kode_nik;

		// peserta
		$status_faskes = $data['status_faskes'];
		foreach ($data['jenis_transaksi_full'] as $value) {
			if ($value->nama_jenis_transaksi == "Berakhirnya faskes anak") {
				$status_faskes = 0;
			}elseif ($value->kode_jenis_transaksi == '23' || $value->kode_jenis_transaksi == '42' || $value->kode_jenis_transaksi == '56' || $value->kode_jenis_transaksi == '69' || $value->kode_jenis_transaksi == '81') {
				$status_faskes = 1;
			}
		}
		$data_peserta = array(
			'no_peserta' => "TLK" . $data['nik'] . "000",
			'nikes' => $data['nikes'],
			'status_faskes' => $status_faskes,
			'tgl_faskes' => $data['tgl_faskes']
		);

	
		$this->db->where('kode_peserta', $peserta->row()->kode_peserta);
		$this->db->update('peserta', $data_peserta);

		// peserta_data_pendukung

		// update data pendukung
		$pendukung = $data['pendukung'];
		for ($i = 0; $i < count($pendukung) ; $i++) { 
			$peserta_data_pendukung = array(
				'no_dokumen' => $data['no_dokumen_edit'][$i],
				'perihal' => $data['perihal_edit'][$i],
				'tgl_dokumen' => $data['tgl_dokumen_edit'][$i]
			);

			$this->db->where('kode_data_pendukung', $pendukung[$i]->kode_data_pendukung);
			$this->db->update('peserta_data_pendukung', $peserta_data_pendukung);
		}


		// input data pendukung baru
		if ($data['file_1']) {

			$peserta_data_pendukung_1 = array(
				'no_dokumen' => $data['no_dokumen_1'],
				'perihal' => $data['perihal_1'],
				'tgl_dokumen' => $data['tgl_dokumen_1'],
				'file' => $data['file_1'],
				'kode_peserta' => $peserta->row()->kode_peserta
			);

			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_1);
		}
		
		if ($data['file_2']) {
			$peserta_data_pendukung_2 = array(
				'no_dokumen' => $data['no_dokumen_2'],
				'perihal' => $data['perihal_2'],
				'tgl_dokumen' => $data['tgl_dokumen_2'],
				'file' => $data['file_2'],
				'kode_peserta' => $peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_2);
		}

		if ($data['file_3']) {
			$peserta_data_pendukung_3 = array(
				'no_dokumen' => $data['no_dokumen_3'],
				'perihal' => $data['perihal_3'],
				'tgl_dokumen' => $data['tgl_dokumen_3'],
				'file' => $data['file_3'],
				'kode_peserta' => $peserta->row()->kode_peserta
			);

			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_3);
		}
	}

	public function editPasangan($data){
		$peserta = $data['peserta'];

		// transaksi
		$transaksi = array('kode_kelompok_transaksi' => $data['kelompok_transaksi'],
			'kode_jenis_transaksi' => $data['jenis_transaksi'],
			'nik' => $data['nik'],
			'nikes' => $data['nikes'],
			'kode_admin' => $this->session->kode_admin,
			'waktu' => $data['waktu']);

		$this->db->insert('transaksi', $transaksi);

		// peserta_data_pribadi
		$peserta_data_pribadi = array(
			'nama' => $data['nama'],
			'tempat_lahir' => $data['tempat_lahir'],
			'tgl_lahir' => $data['tgl_lahir'],
			'jenis_kelamin' => $data['jenis_kelamin'],
			'kode_agama' => $data['agama'],
			'no_ktp' => $data['no_ktp'],
			'gol_darah' => $data['gol_darah'],
			'rhesus' => $data['rhesus'],
			'foto' => $data['foto'],
			'no_bpjs' => $data['no_bpjs']);

		$this->db->where('kode_data_pribadi', $peserta->row()->kode_data_pribadi);
		$this->db->update('peserta_data_pribadi', $peserta_data_pribadi);

		// peserta_data_tpk
		$peserta_data_tpk = array(
			'kode_tpk' => $data['kode_tpk']);

		$this->db->where('kode_data_tpk', $peserta->row()->kode_data_tpk);
		$this->db->update('peserta_data_tpk', $peserta_data_tpk);

		// peserta_data_kepegawaian
		$peserta_data_kepegawaian = array(
			'kode_area' => $data['kode_area'],
			'kode_pendidikan' => $data['kode_pendidikan'],
			'kode_pekerjaan' => $data['kode_pekerjaan'],
			'kode_band_posisi' => $data['kode_band_posisi'],
			'kode_personal_sub_area' => $data['kode_personal_sub_area'],
			'nip' => $data['nip_pasangan']
		);


		$this->db->where('kode_data_kepegawaian', $peserta->row()->kode_data_kepegawaian);
		$this->db->update('peserta_data_kepegawaian', $peserta_data_kepegawaian);

		// peserta_data_alamat (ktp)
		$peserta_data_alamat_ktp = array(
			'jenis_alamat' => "ktp",
			'alamat' => $data['alamat_ktp'],
			'rt' => $data['rt_ktp'],
			'rw' => $data['rw_ktp'],
			'kode_provinsi' => $data['kode_provinsi_ktp'],
			'kode_kabupaten' => $data['kode_kabupaten_ktp'],
			'kode_kecamatan' => $data['kode_kecamatan_ktp'],
			'kode_kelurahan' => $data['kode_kelurahan_ktp'],
			'kode_pos' => $data['kode_pos_ktp'],
			'no_telp' => $data['no_telp_ktp']
		);

		$this->db->where('kode_alamat', $peserta->row()->kode_alamat_ktp);
		$this->db->update('peserta_data_alamat', $peserta_data_alamat_ktp);

		// peserta_data_alamat (domisili)
		$peserta_data_alamat_domisili = array(
			'jenis_alamat' => "domisili",
			'alamat' => $data['alamat_domisili'],
			'rt' => $data['rt_domisili'],
			'rw' => $data['rw_domisili'],
			'kode_provinsi' => $data['kode_provinsi_domisili'],
			'kode_kabupaten' => $data['kode_kabupaten_domisili'],
			'kode_kecamatan' => $data['kode_kecamatan_domisili'],
			'kode_kelurahan' => $data['kode_kelurahan_domisili'],
			'kode_pos' => $data['kode_pos_domisili'],
			'no_telp' => $data['no_telp_domisili']
		);

		$this->db->where('kode_alamat', $peserta->row()->kode_alamat_domisili);
		$this->db->update('peserta_data_alamat', $peserta_data_alamat_domisili);

		// peserta_data_info_lain
		$peserta_data_info_lain = array(
			'no_hp' => $data['no_hp'],
			'tgl_meninggal' => $data['tgl_meninggal'],
			'status_meninggal' => $data['status_meninggal'],
			'catatan' => $data['catatan'],
			'alasan_berhenti_peserta' => $data['alasan_berhenti_peserta']
		);

		$this->db->where('kode_data_info_lain', $peserta->row()->kode_data_info_lain);
		$this->db->update('peserta_data_info_lain', $peserta_data_info_lain);

		// peserta_data_bank
		$peserta_data_bank = array(
			'nama_pemilik_rekening' => $data['nama_pemilik_rekening'],
			'kode_bank' => $data['kode_bank'],
			'no_rekening' => $data['no_rekening_bank'],
			'status_kartu' => $data['status_kartu'],
			'tgl_cetak' => $data['tgl_cetak'],
			'tgl_akhir_kartu' => $data['tgl_akhir_kartu']
		);

		$this->db->where('kode_data_bank', $peserta->row()->kode_data_bank);
		$this->db->update('peserta_data_bank', $peserta_data_bank);

		// peserta_data_pernikahan
		$peserta_data_pernikahan = array(
			'status_pernikahan' => $data['status_pernikahan'],
			'tgl_nikah' => $data['tgl_nikah'],
			'tgl_cerai' => $data['tgl_cerai']
		);

		$this->db->where('kode_data_pernikahan', $peserta->row()->kode_status_pernikahan);
		$this->db->update('peserta_data_pernikahan', $peserta_data_pernikahan);

		$nik = $this->Peserta->getNik($data['nik']);

		$kode_nik = $nik->row()->kode_nik;

		// peserta
		$status_faskes = $data['status_faskes'];
		foreach ($data['jenis_transaksi_full'] as $value) {
			if ($value->nama_jenis_transaksi == "Berakhirnya faskes pasangan karena perceraian") {
				$status_faskes = 0;
			} elseif ($value->kode_jenis_transaksi == '16' || $value->kode_jenis_transaksi == '38' || $value->kode_jenis_transaksi == '53' || $value->kode_jenis_transaksi == '66' || $value->kode_jenis_transaksi == '79') {
				$status_faskes = 1;
			}
		}
		
		$data_peserta = array(
			'no_peserta' => "TLK" . $data['nik'] . $data['pasangan_ke'] . "00",
			'nikes' => $data['nikes'],
			'status_faskes' => $status_faskes,
			'tgl_faskes' => $data['tgl_faskes']
		);

	
		$this->db->where('kode_peserta', $peserta->row()->kode_peserta);
		$this->db->update('peserta', $data_peserta);

		// peserta_data_pendukung

		// update data pendukung
		$pendukung = $data['pendukung'];
		for ($i = 0; $i < count($pendukung) ; $i++) { 
			$peserta_data_pendukung = array(
				'no_dokumen' => $data['no_dokumen_edit'][$i],
				'perihal' => $data['perihal_edit'][$i],
				'tgl_dokumen' => $data['tgl_dokumen_edit'][$i]
			);

			$this->db->where('kode_data_pendukung', $pendukung[$i]->kode_data_pendukung);
			$this->db->update('peserta_data_pendukung', $peserta_data_pendukung);
		}


		// input data pendukung baru
		if ($data['file_1']) {

			$peserta_data_pendukung_1 = array(
				'no_dokumen' => $data['no_dokumen_1'],
				'perihal' => $data['perihal_1'],
				'tgl_dokumen' => $data['tgl_dokumen_1'],
				'file' => $data['file_1'],
				'kode_peserta' => $peserta->row()->kode_peserta
			);

			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_1);
		}
		
		if ($data['file_2']) {
			$peserta_data_pendukung_2 = array(
				'no_dokumen' => $data['no_dokumen_2'],
				'perihal' => $data['perihal_2'],
				'tgl_dokumen' => $data['tgl_dokumen_2'],
				'file' => $data['file_2'],
				'kode_peserta' => $peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_2);
		}

		if ($data['file_3']) {
			$peserta_data_pendukung_3 = array(
				'no_dokumen' => $data['no_dokumen_3'],
				'perihal' => $data['perihal_3'],
				'tgl_dokumen' => $data['tgl_dokumen_3'],
				'file' => $data['file_3'],
				'kode_peserta' => $peserta->row()->kode_peserta
			);

			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_3);
		}
	}


	public function editKepalaKeluarga($data){

		$peserta = $data['peserta'];

		// transaksi
		$transaksi = array('kode_kelompok_transaksi' => $data['kelompok_transaksi'],
			'kode_jenis_transaksi' => $data['jenis_transaksi'],
			'nik' => $data['nik'],
			'nikes' => $data['nikes'],
			'kode_admin' => $this->session->kode_admin,
			'waktu' => $data['waktu']);

		$this->db->insert('transaksi', $transaksi);

		// peserta_data_pribadi
		$peserta_data_pribadi = array(
			'nama' => $data['nama'],
			'tempat_lahir' => $data['tempat_lahir'],
			'tgl_lahir' => $data['tgl_lahir'],
			'jenis_kelamin' => $data['jenis_kelamin'],
			'kode_agama' => $data['agama'],
			'no_ktp' => $data['no_ktp'],
			'gol_darah' => $data['gol_darah'],
			'rhesus' => $data['rhesus'],
			'foto' => $data['foto'],
			'no_bpjs' => $data['no_bpjs']);

		$this->db->where('kode_data_pribadi', $peserta->row()->kode_data_pribadi);
		$this->db->update('peserta_data_pribadi', $peserta_data_pribadi);

		// peserta_data_tpk
		$peserta_data_tpk = array(
			'kode_tpk' => $data['kode_tpk']);

		$this->db->where('kode_data_tpk', $peserta->row()->kode_data_tpk);
		$this->db->update('peserta_data_tpk', $peserta_data_tpk);
			

		// peserta_data_kepegawaian
		$peserta_data_kepegawaian = array(
			'kode_kelompok_peserta' => $data['kode_kelompok_peserta'],
			'kode_instansi' => $data['kode_instansi'],
			'kode_band_posisi' => $data['kode_band_posisi'],
			'klas_posisi' => $data['klas_posisi'],
			'jabatan' => $data['jabatan'],
			'kode_area' => $data['kode_area'],
			'tgl_capeg' => $data['tgl_capeg'],
			'tgl_mulai_kerja' => $data['tgl_mulai_kerja'],
			'tgl_pensiun' => $data['tgl_pensiun'],
			'tgl_berhenti_kerja' => $data['tgl_berhenti_kerja'],
			'alasan_berhenti_kerja' => $data['alasan_berhenti_kerja'],
			'kode_pendidikan' => $data['kode_pendidikan'],
			'kode_divisi' => $data['kode_divisi'],
			'kode_bagian' => $data['kode_bagian'],
			'kelas_perawatan' => $data['kelas_perawatan'],
			'kode_personal_sub_area' => $data['kode_personal_sub_area']
		);

		$this->db->where('kode_data_kepegawaian', $peserta->row()->kode_data_kepegawaian);
		$this->db->update('peserta_data_kepegawaian', $peserta_data_kepegawaian);

		// peserta_data_alamat (ktp)
		$peserta_data_alamat_ktp = array(
			'jenis_alamat' => "ktp",
			'alamat' => $data['alamat_ktp'],
			'rt' => $data['rt_ktp'],
			'rw' => $data['rw_ktp'],
			'kode_provinsi' => $data['kode_provinsi_ktp'],
			'kode_kabupaten' => $data['kode_kabupaten_ktp'],
			'kode_kecamatan' => $data['kode_kecamatan_ktp'],
			'kode_kelurahan' => $data['kode_kelurahan_ktp'],
			'kode_pos' => $data['kode_pos_ktp'],
			'no_telp' => $data['no_telp_ktp']
		);

		$this->db->where('kode_alamat', $peserta->row()->kode_alamat_ktp);
		$this->db->update('peserta_data_alamat', $peserta_data_alamat_ktp);

		// peserta_data_alamat (domisili)
		$peserta_data_alamat_domisili = array(
			'jenis_alamat' => "domisili",
			'alamat' => $data['alamat_domisili'],
			'rt' => $data['rt_domisili'],
			'rw' => $data['rw_domisili'],
			'kode_provinsi' => $data['kode_provinsi_domisili'],
			'kode_kabupaten' => $data['kode_kabupaten_domisili'],
			'kode_kecamatan' => $data['kode_kecamatan_domisili'],
			'kode_kelurahan' => $data['kode_kelurahan_domisili'],
			'kode_pos' => $data['kode_pos_domisili'],
			'no_telp' => $data['no_telp_domisili']
		);

		$this->db->where('kode_alamat', $peserta->row()->kode_alamat_domisili);
		$this->db->update('peserta_data_alamat', $peserta_data_alamat_domisili);

		// peserta_data_info_lain
		$peserta_data_info_lain = array(
			'no_hp' => $data['no_hp'],
			'tgl_meninggal' => $data['tgl_meninggal'],
			'status_meninggal' => $data['status_meninggal'],
			'catatan' => $data['catatan']
		);

		$this->db->where('kode_data_info_lain', $peserta->row()->kode_data_info_lain);
		$this->db->update('peserta_data_info_lain', $peserta_data_info_lain);

		// peserta_data_bank
		$peserta_data_bank = array(
			'nama_pemilik_rekening' => $data['nama_pemilik_rekening'],
			'kode_bank' => $data['kode_bank'],
			'no_rekening' => $data['no_rekening_bank'],
			'status_kartu' => $data['status_kartu'],
			'tgl_cetak' => $data['tgl_cetak'],
			'tgl_akhir_kartu' => $data['tgl_akhir_kartu']
		);

		$this->db->where('kode_data_bank', $peserta->row()->kode_data_bank);
		$this->db->update('peserta_data_bank', $peserta_data_bank);

		// peserta_data_pernikahan
		$peserta_data_pernikahan = array(
			'status_pernikahan' => $data['status_pernikahan'],
			'tgl_nikah' => $data['tgl_nikah'],
			'tgl_cerai' => $data['tgl_cerai']
		);

		$this->db->where('kode_data_pernikahan', $peserta->row()->kode_status_pernikahan);
		$this->db->update('peserta_data_pernikahan', $peserta_data_pernikahan);

		// nik
		$kode_nik = "";
		if ($data['nik_baru'] != $data['nik_lama']) {

			// hapus nik lama
			$nik_lama = $this->Peserta->getNik($data['nik_lama']);
			
			// $this->db-> where('kode_nik', $nik_lama->row()->kode_nik);
 		// 	$this->db-> delete('nik');

			// // update nik baru
			$nik_baru = array('nik' => $data['nik_baru']);

			$this->db->where('kode_nik', $nik_lama->row()->kode_nik);
			$this->db->update('nik', $nik_baru);

			$this->db->select('*');
	        $this->db->from('peserta');
	        $this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
	        $this->db->join('peserta_data_info_lain', 'peserta_data_info_lain.kode_data_info_lain = peserta.kode_data_info_lain', 'left');
	        $this->db->where('kode_nik', $nik_lama->row()->kode_nik);

	        $query = $this->db->get();

	        $temp = $query->result();

	        foreach ($temp as $value) {
	        	if ($value->kode_group_status_keluarga == 3) {
	        		$anak = array('nikes' => $data['nik_baru'] . "." . $value->no_anggota_keluarga . "0" . $value->no_anggota_keluarga_2);

	        		$this->db->where('kode_peserta', $value->kode_peserta);
					$this->db->update('peserta', $anak);
	        	}else if($value->kode_group_status_keluarga == 2){
	        		$pasangan = array('nikes' => $data['nik_baru'] . "." . $value->no_anggota_keluarga . "00");

	        		$this->db->where('kode_peserta', $value->kode_peserta);
					$this->db->update('peserta', $pasangan);
	        	}
	        }




		}else{
			// nik tidak diapa-apain
			$nik_lama = $this->Peserta->getNik($data['nik_lama']);

			$kode_nik = $nik_lama->row()->kode_nik;
		}

		
		$jenis_peserta = $peserta->row()->kode_jenis_peserta;
		$jenis_peserta_pasangan = "";
		$jenis_peserta_anak = "";
		
		$status_faskes = $data['status_faskes'];

		if ($data['kelompok_transaksi'] == "02") {
			$this->db->select('*');
	        $this->db->from('peserta');
	        $this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
	        $this->db->where('kode_peserta', $peserta->row()->kode_peserta);
	        $query = $this->db->get();
	        $temp = $query->result();

	        foreach ($data['jenis_transaksi_full'] as $value) {
	        	foreach ($temp as $value2) {
			        if ($value->kode_jenis_peserta_2 != $value2->kode_group_jenis_peserta) {
			        	// ada perubahan group jenis peserta
			        	$this->db->select('*');
	        			$this->db->from('jenis_peserta');
	        			$this->db->where('kode_group_status_keluarga', "1");
	        			$this->db->where('kode_group_jenis_peserta', $value->kode_jenis_peserta_2);
	        			$query_2 = $this->db->get();
	        			$temp_2 = $query_2->result();

	        			foreach ($temp_2 as $value3) {
	        				$jenis_peserta = $value3->kode_jenis_peserta;
	        			}
	        			if ($value->kode_jenis_peserta_2 == "20") {
	        				if ($value->nama_jenis_transaksi == "Pensiun dengan faskes") {
	        					// pensiun dengan faskes
	        					$status_faskes = 1;
	        				}else{
	        					// pensiun tanpa faskes
	        					$status_faskes = 0;
	        				}
	        			}

	        			// update pasangan
	        			$this->db->select('*');
	        			$this->db->from('jenis_peserta');
	        			$this->db->where('kode_group_status_keluarga', "2");
	        			$this->db->where('kode_group_jenis_peserta', $value->kode_jenis_peserta_2);
	        			$query_3 = $this->db->get();
	        			$temp_3 = $query_3->result();

	        			foreach ($temp_3 as $value4) {
	        				$jenis_peserta_pasangan = $value4->kode_jenis_peserta;
	        			}

	        			// update anak
	        			$this->db->select('*');
	        			$this->db->from('jenis_peserta');
	        			$this->db->where('kode_group_status_keluarga', "3");
	        			$this->db->where('kode_group_jenis_peserta', $value->kode_jenis_peserta_2);
	        			$query_4 = $this->db->get();
	        			$temp_4 = $query_4->result();

	        			foreach ($temp_4 as $value5) {
	        				$jenis_peserta_anak = $value5->kode_jenis_peserta;
	        			}

			        }
	        	}
	        }
		}

		if ($data['jenis_transaksi'] == '32' || $data['jenis_transaksi'] == '60') {
			$status_faskes = 1;
		}

		// peserta
		$data_peserta = array(
			'no_peserta' => "TLK" . $data['nik'] . "000",
			'nikes' => $data['nik_baru'] . ".000",
			'status_faskes' => $status_faskes,
			'tgl_faskes' => $data['tgl_faskes'],
			'kode_jenis_peserta' => $jenis_peserta
		);

		// update kk
		$this->db->where('kode_peserta', $peserta->row()->kode_peserta);
		$this->db->update('peserta', $data_peserta);

		// update pasangan & anak
		if ($jenis_peserta != $peserta->row()->kode_jenis_peserta) {
			// update pasangan
			$this->db->select('*');
	        $this->db->from('peserta');
        	$this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
	        $this->db->where('kode_nik', $kode_nik);
	        $this->db->where('kode_group_status_keluarga', "2");
	        $query = $this->db->get();
	        $temp = $query->result();

	        $kode_peserta_pasangan = "";
	        $data_peserta_pasangan = array();
	        foreach ($temp as $value) {
	        	$kode_peserta_pasangan = $value->kode_peserta;
		        $data_peserta_pasangan = array(
					'status_faskes' => $status_faskes,
					'kode_jenis_peserta' => $jenis_peserta_pasangan
		        );

				$this->db->where('kode_peserta', $kode_peserta_pasangan);
				$this->db->update('peserta', $data_peserta_pasangan);
	        }


			// update anak
			$this->db->select('*');
	        $this->db->from('peserta');
        	$this->db->join('jenis_peserta', 'jenis_peserta.kode_jenis_peserta = peserta.kode_jenis_peserta', 'left');
	        $this->db->where('kode_nik', $kode_nik);
	        $this->db->where('kode_group_status_keluarga', "3");
	        $query = $this->db->get();
	        $temp = $query->result();

	        $kode_peserta_anak = "";
	        $data_peserta_anak = array();
	        foreach ($temp as $value) {
	        	$kode_peserta_anak = $value->kode_peserta;

		        $data_peserta_anak = array(
					'status_faskes' => $status_faskes,
					'kode_jenis_peserta' => $jenis_peserta_anak
		        );

		        // update kk
				$this->db->where('kode_peserta', $kode_peserta_anak);
				$this->db->update('peserta', $data_peserta_anak);
	        }


		}

		// peserta_data_pendukung

		// update data pendukung
		$pendukung = $data['pendukung'];
		for ($i = 0; $i < count($pendukung) ; $i++) { 
			$peserta_data_pendukung = array(
				'no_dokumen' => $data['no_dokumen_edit'][$i],
				'perihal' => $data['perihal_edit'][$i],
				'tgl_dokumen' => $data['tgl_dokumen_edit'][$i]
			);

			$this->db->where('kode_data_pendukung', $pendukung[$i]->kode_data_pendukung);
			$this->db->update('peserta_data_pendukung', $peserta_data_pendukung);
		}


		// input data pendukung baru
		if ($data['file_1']) {

			$peserta_data_pendukung_1 = array(
				'no_dokumen' => $data['no_dokumen_1'],
				'perihal' => $data['perihal_1'],
				'tgl_dokumen' => $data['tgl_dokumen_1'],
				'file' => $data['file_1'],
				'kode_peserta' => $peserta->row()->kode_peserta
			);

			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_1);
		}
		
		if ($data['file_2']) {
			$peserta_data_pendukung_2 = array(
				'no_dokumen' => $data['no_dokumen_2'],
				'perihal' => $data['perihal_2'],
				'tgl_dokumen' => $data['tgl_dokumen_2'],
				'file' => $data['file_2'],
				'kode_peserta' => $peserta->row()->kode_peserta
			);
			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_2);
		}

		if ($data['file_3']) {
			$peserta_data_pendukung_3 = array(
				'no_dokumen' => $data['no_dokumen_3'],
				'perihal' => $data['perihal_3'],
				'tgl_dokumen' => $data['tgl_dokumen_3'],
				'file' => $data['file_3'],
				'kode_peserta' => $peserta->row()->kode_peserta
			);

			$this->db->insert('peserta_data_pendukung', $peserta_data_pendukung_3);
		}

		
	}

	public function getLastPeserta(){
		$peserta = $this->db->select('*')
			->from('peserta')
			->order_by('kode_peserta',"desc")
			->limit(1)->get();

		return $peserta->row()->kode_peserta;

	}


	public function getAllKelompokPeserta()
	{
		$query = $this->db->select('*')
							->from('kelompok_peserta')->get();

		return $query->result();
	}

	public function getAllPeserta(){
		$query = $this->db->select('*')
							->from('peserta')->get();

		return $query->result();
	}	


	public function getAllPesertaFaskes()
	{
		$query = $this->db->select('*')
							->from('peserta')
							->where('status_faskes', '1')->get();

		return $query->result();
	}

	

	public function getAllGroupStatusKeluarga()
	{
		$query = $this->db->select('*')
							->from('group_status_keluarga')->get();

		return $query->result();
	}

	public function getAllGroupJenisPeserta()
	{
		$query = $this->db->select('*')
							->from('group_jenis_peserta')->get();

		return $query->result();
	}


	public function getAllPendidikan()
	{
		$query = $this->db->select('*')
							->from('pendidikan')->get();

		return $query->result();
	}



	public function getAllPekerjaan()
	{
		$query = $this->db->select('*')
							->from('pekerjaan')->get();

		return $query->result();
	}

	public function getAllJenisPeserta()
	{
		$query = $this->db->select('*')
							->from('jenis_peserta')->get();

		return $query->result();
	}

	public function getAllAgama()
	{
		$query = $this->db->select('*')
							->from('agama')->get();

		return $query->result();
	}

	
	

	public function getAllBank()
	{
		$query = $this->db->select('*')
							->from('bank')->get();

		return $query->result();
	}

	public function getAllInstansi()
	{
		$query = $this->db->select('*')
							->from('instansi')->get();

		return $query->result();
	}

	public function getInstansi($kode_kelompok_peserta)
	{
		$query = $this->db->get_where('instansi', array('kode_kelompok_peserta' => $kode_kelompok_peserta));

		return $query->result();
	}

	public function getAllDivisi()
	{
		$query = $this->db->select('*')
							->from('divisi')->get();

		return $query->result();
	}

	public function getDivisi($kode_instansi)
	{
		$query = $this->db->get_where('divisi', array('kode_instansi' => $kode_instansi));

		return $query->result();
	}

	public function getAllBandPosisi()
	{
		$query = $this->db->select('*')
							->from('band_posisi')->get();

		return $query->result();
	}

	public function getBandPosisiById($kode_band_posisi)
	{
		$query = $this->db->select('*')
								->from('band_posisi')
								->where('kode_band_posisi', $kode_band_posisi)->get();

		return $query;
	}

	public function getKlasPosisi($nama_band)
	{
		$query = $this->db->get_where('band_posisi', array('nama_band' => $nama_band));

		return $query->result();
	}

	public function getPersonalSubAreaById($kode_personal_sub_area)
	{
		$query = $this->db->select('*')
								->from('personal_sub_area')
								->where('kode_personal_sub_area', $kode_personal_sub_area)->get();

		return $query;
	}


	public function getPersonalSubArea($kode_personal_area)
	{
		$query = $this->db->get_where('personal_sub_area', array('kode_personal_area' => $kode_personal_area));

		return $query->result();
	}

	public function getPesertaByNik($nik){
		$kode_nik = $this->getNik($nik);
		$query = $this->db->get_where('peserta', array('kode_nik' => $kode_nik->row()->kode_nik));

		return $query->result();

	}

	public function getPesertaByNik2($nik){
		$kode_nik = "";
		if ($nik) {
			$temp = $this->getNik($nik);
			$kode_nik = $temp->row()->kode_nik;
		}
		$query = $this->db->get_where('peserta', array('kode_nik' => $kode_nik));

		return $query->result();

	}

	public function getPesertaById($kode_peserta){
		$query = $this->db->select('*')
								->from('peserta')
								->where('kode_peserta', $kode_peserta)->get();

		return $query;

	}

	public function getJenisPesertaById($kode_jenis_peserta){
		$query = $this->db->get_where('jenis_peserta', array('kode_jenis_peserta' => $kode_jenis_peserta));

		return $query->result();

	}

	public function getJenisPesertaByGroup($kode_group_jenis_peserta){
		$query = $this->db->get_where('jenis_peserta', array('kode_group_jenis_peserta' => $kode_group_jenis_peserta));

		return $query->result();

	}

	public function getDataPribadiById($kode_data_pribadi){
		$query = $this->db->select('*')
								->from('peserta_data_pribadi')
								->where('kode_data_pribadi', $kode_data_pribadi)->get();

		return $query;

	}

	public function getDataKepegawaianById($kode_data_kepegawaian){
		$query = $this->db->select('*')
								->from('peserta_data_kepegawaian')
								->where('kode_data_kepegawaian', $kode_data_kepegawaian)->get();

		return $query;

	}

	public function getDataTPK($kode_tpk){
		$query = $this->db->get_where('peserta_data_tpk', array('kode_tpk' => $kode_tpk));

		return $query->result();

	}

	public function getDataTPKById($kode_data_tpk){
		$query = $this->db->select('*')
								->from('peserta_data_tpk')
								->where('kode_data_tpk', $kode_data_tpk)->get();

		return $query;

	}

	public function getDataAlamatById($kode_alamat){
		$query = $this->db->select('*')
								->from('peserta_data_alamat')
								->where('kode_alamat', $kode_alamat)->get();

		return $query;

	}

	public function getDataInfoLainById($kode_data_info_lain){
		$query = $this->db->select('*')
								->from('peserta_data_info_lain')
								->where('kode_data_info_lain', $kode_data_info_lain)->get();

		return $query;

	}

	public function getDataBankById($kode_data_bank){
		$query = $this->db->select('*')
								->from('peserta_data_bank')
								->where('kode_data_bank', $kode_data_bank)->get();

		return $query;

	}

	public function getDataPernikahanById($kode_data_pernikahan){
		$query = $this->db->select('*')
								->from('peserta_data_pernikahan')
								->where('kode_data_pernikahan', $kode_data_pernikahan)->get();

		return $query;

	}

	public function getDataPendukung($kode_peserta){
		$query = $this->db->get_where('peserta_data_pendukung', array('kode_peserta' => $kode_peserta));

		return $query->result();

	}

	public function getDataPendukungById($kode_data_pendukung){
		$query = $this->db->select('*')
								->from('peserta_data_pendukung')
								->where('kode_data_pendukung', $kode_data_pendukung)->get();

		return $query;

	}





	public function getNik($nik){
		$query = $this->db->select('*')
								->from('nik')
								->where('nik', $nik)->get();


		return $query;

	}



	public function getNikById($kode_nik){
		$query = $this->db->select('*')
								->from('nik')
								->where('kode_nik', $kode_nik)->get();

		return $query;

	}

	public function getPegawai($kode_data_kepegawaian){
		$query = $this->db->select('*')
								->from('peserta_data_kepegawaian')
								->where('kode_data_kepegawaian', $kode_data_kepegawaian)->get();

		return $query;

	}

	public function getPribadi($kode_data_pribadi){
		$query = $this->db->select('*')
								->from('peserta_data_pribadi')
								->where('kode_data_pribadi', $kode_data_pribadi)->get();

		return $query;

	}

	public function hapusAnak($data){
		// peserta data pendukung

		// get all peserta
		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->where('kode_peserta', $data['kode_peserta']);
        $query = $this->db->get();

        $peserta = $query->result();

        $pendukung = $this->getDataPendukung($data['kode_peserta']);
        if (count($pendukung) > 0) {
        	foreach ($pendukung as $value2) {
        		// hapus file dari folder
        		unlink(FCPATH . 'uploads/data_pendukung/'.$value2->file);

        		// hapus row dari data pendukung
        		$this->db->where('kode_peserta', $data['kode_peserta']);
 				$this->db-> delete('peserta_data_pendukung');

        	}
        }

        // peserta
 		$this->db->where('kode_peserta', $data['kode_peserta']);
 		$this->db-> delete('peserta');

 		foreach ($peserta as $value) {
 			// peserta data alamat ktp
	        $this->db->where('kode_alamat', $value->kode_alamat_ktp);
	 		$this->db-> delete('peserta_data_alamat');

	        // peserta data alamat domisili
	        $this->db->where('kode_alamat', $value->kode_alamat_domisili);
	 		$this->db-> delete('peserta_data_alamat');

	        // peserta data bank
	        $this->db->where('kode_data_bank', $value->kode_data_bank);
	 		$this->db-> delete('peserta_data_bank');

	        // peserta data info lain
	        $this->db->where('kode_data_info_lain', $value->kode_data_info_lain);
	 		$this->db-> delete('peserta_data_info_lain');

	        // peserta data kepegawaian
	        $this->db->where('kode_data_kepegawaian', $value->kode_data_kepegawaian);
	 		$this->db-> delete('peserta_data_kepegawaian');

	        // peserta data pernikahan
	        $this->db->where('kode_data_pernikahan', $value->kode_status_pernikahan);
	 		$this->db-> delete('peserta_data_pernikahan');

	        // peserta data pribadi
	        $data_pribadi = $this->getDataPribadiById($value->kode_data_pribadi);
	        if ($data_pribadi->row()->foto) {
	        	// hapus file dari folder
	        	unlink(FCPATH . 'uploads/foto/'.$data_pribadi->row()->foto);
	        }

	        $this->db->where('kode_data_pribadi', $value->kode_data_pribadi);
	 		$this->db-> delete('peserta_data_pribadi');

	        // peserta data tpk
	 		$this->db->where('kode_data_tpk', $value->kode_data_tpk);
	 		$this->db-> delete('peserta_data_tpk');
 		}

 		$transaksi = array('kode_kelompok_transaksi' => $data['kelompok_transaksi'],
			'kode_jenis_transaksi' => $data['jenis_transaksi'],
			'nik' => $data['nik'],
			'nikes' => $data['nikes'],
			'kode_admin' => $this->session->kode_admin,
			'waktu' => $data['waktu']);

		$this->db->insert('transaksi', $transaksi);
        
	}


	public function hapusPasangan($data){
		// peserta data pendukung

		// get all peserta
		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->where('kode_peserta', $data['kode_peserta']);
        $query = $this->db->get();

        $peserta = $query->result();

        $pendukung = $this->getDataPendukung($data['kode_peserta']);
        if (count($pendukung) > 0) {
        	foreach ($pendukung as $value2) {
        		// hapus file dari folder
        		unlink(FCPATH . 'uploads/data_pendukung/'.$value2->file);

        		// hapus row dari data pendukung
        		$this->db->where('kode_peserta', $data['kode_peserta']);
 				$this->db-> delete('peserta_data_pendukung');

        	}
        }

        // peserta
 		$this->db->where('kode_peserta', $data['kode_peserta']);
 		$this->db-> delete('peserta');

 		foreach ($peserta as $value) {
 			// peserta data alamat ktp
	        $this->db->where('kode_alamat', $value->kode_alamat_ktp);
	 		$this->db-> delete('peserta_data_alamat');

	        // peserta data alamat domisili
	        $this->db->where('kode_alamat', $value->kode_alamat_domisili);
	 		$this->db-> delete('peserta_data_alamat');

	        // peserta data bank
	        $this->db->where('kode_data_bank', $value->kode_data_bank);
	 		$this->db-> delete('peserta_data_bank');

	        // peserta data info lain
	        $this->db->where('kode_data_info_lain', $value->kode_data_info_lain);
	 		$this->db-> delete('peserta_data_info_lain');

	        // peserta data kepegawaian
	        $this->db->where('kode_data_kepegawaian', $value->kode_data_kepegawaian);
	 		$this->db-> delete('peserta_data_kepegawaian');

	        // peserta data pernikahan
	        $this->db->where('kode_data_pernikahan', $value->kode_status_pernikahan);
	 		$this->db-> delete('peserta_data_pernikahan');

	        // peserta data pribadi
	        $data_pribadi = $this->getDataPribadiById($value->kode_data_pribadi);
	        if ($data_pribadi->row()->foto) {
	        	// hapus file dari folder
	        	unlink(FCPATH . 'uploads/foto/'.$data_pribadi->row()->foto);
	        }

	        $this->db->where('kode_data_pribadi', $value->kode_data_pribadi);
	 		$this->db-> delete('peserta_data_pribadi');

	        // peserta data tpk
	 		$this->db->where('kode_data_tpk', $value->kode_data_tpk);
	 		$this->db-> delete('peserta_data_tpk');
 		}
        $transaksi = array('kode_kelompok_transaksi' => $data['kelompok_transaksi'],
			'kode_jenis_transaksi' => $data['jenis_transaksi'],
			'nik' => $data['nik'],
			'nikes' => $data['nikes'],
			'kode_admin' => $this->session->kode_admin,
			'waktu' => $data['waktu']);

		$this->db->insert('transaksi', $transaksi);
	}

	public function hapusKeluarga($data){


		// $this->db->where('kode_nik', $kode_nik)
 		// 	$this->db-> delete('t_fkampus');

		// get all peserta
		$this->db->select('*');
        $this->db->from('peserta');
        $this->db->where('kode_nik', $data['kode_nik']);
        $query = $this->db->get();

        $peserta = $query->result();


        foreach ($peserta as $value) {
        	// peserta data pendukung

        	$pendukung = $this->getDataPendukung($value->kode_peserta);
        	if (count($pendukung) > 0) {
        		foreach ($pendukung as $value2) {
        			// hapus file dari folder
        			unlink(FCPATH . 'uploads/data_pendukung/'.$value2->file);

        			// hapus row dari data pendukung
        			$this->db->where('kode_peserta', $value->kode_peserta);
 					$this->db-> delete('peserta_data_pendukung');

        		}
        	}

        	// peserta
 			$this->db->where('kode_peserta', $value->kode_peserta);
 			$this->db-> delete('peserta');

        	// peserta data alamat ktp
        	$this->db->where('kode_alamat', $value->kode_alamat_ktp);
 			$this->db-> delete('peserta_data_alamat');

        	// peserta data alamat domisili
        	$this->db->where('kode_alamat', $value->kode_alamat_domisili);
 			$this->db-> delete('peserta_data_alamat');

        	// peserta data bank
        	$this->db->where('kode_data_bank', $value->kode_data_bank);
 			$this->db-> delete('peserta_data_bank');

        	// peserta data info lain
        	$this->db->where('kode_data_info_lain', $value->kode_data_info_lain);
 			$this->db-> delete('peserta_data_info_lain');

        	// peserta data kepegawaian
        	$this->db->where('kode_data_kepegawaian', $value->kode_data_kepegawaian);
 			$this->db-> delete('peserta_data_kepegawaian');

        	// peserta data pernikahan
        	$this->db->where('kode_data_pernikahan', $value->kode_status_pernikahan);
 			$this->db-> delete('peserta_data_pernikahan');

        	// peserta data pribadi
        	$data_pribadi = $this->getDataPribadiById($value->kode_data_pribadi);
        	if ($data_pribadi->row()->foto) {
        		// hapus file dari folder
        		unlink(FCPATH . 'uploads/foto/'.$data_pribadi->row()->foto);
        	}

        	$this->db->where('kode_data_pribadi', $value->kode_data_pribadi);
 			$this->db-> delete('peserta_data_pribadi');

        	// peserta data tpk
 			$this->db->where('kode_data_tpk', $value->kode_data_tpk);
 			$this->db-> delete('peserta_data_tpk');

        	

 			
        }
 		// hapus nik
 		$this->db->where('kode_nik', $value->kode_nik);
 		$this->db-> delete('nik');

 		$transaksi = array('kode_kelompok_transaksi' => $data['kelompok_transaksi'],
			'kode_jenis_transaksi' => $data['jenis_transaksi'],
			'nik' => $data['nik'],
			'nikes' => $data['nikes'],
			'kode_admin' => $this->session->kode_admin,
			'waktu' => $data['waktu']);

		$this->db->insert('transaksi', $transaksi);

	}





}


?>