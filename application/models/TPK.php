<?php  

class TPK extends CI_Model{

	function __construct()
	{
		parent::__construct();
	}

	public function getAllTPK()
	{
		$query = $this->db->select('*')
							->from('tpk')->get();

		return $query->result();
	}

	public function getTPK($kode_area)
	{
		$query = $this->db->get_where('tpk', array('kode_area' => $kode_area));

		return $query->result();
	}

	public function getTPKById($kode_tpk)
	{
		$query = $this->db->select('*')
								->from('tpk')
								->where('kode_tpk', $kode_tpk)->get();

		return $query;
	}

	public function getTPK2($kode_tpk)
	{
		$query = $this->db->get_where('tpk', array('kode_tpk' => $kode_tpk));

		return $query->result();
	}

	public function getKotaKantor($kode_kota_kantor)
	{
		$query = $this->db->get_where('kota_kantor', array('kode_kota_kantor' => $kode_kota_kantor));

		return $query->result();
	}

	public function getTPKK(){
		$this->db->select('*');
        $this->db->from('peserta_data_tpk');
        $this->db->join('tpk', 'tpk.kode_tpk = peserta_data_tpk.kode_tpk', 'left');
        $this->db->where('status_tpk', 'TPKK');

        $query = $this->db->get();

        $temp = $query->result();

        return $temp;
	}

	

	public function getTPKU(){
		$this->db->select('*');
        $this->db->from('peserta_data_tpk');
        $this->db->join('tpk', 'tpk.kode_tpk = peserta_data_tpk.kode_tpk', 'left');
        $this->db->where('status_tpk', 'TPKU');

        $query = $this->db->get();

        $temp = $query->result();

        return $temp;
	}

	public function getNonTPK(){
		$this->db->select('*');
        $this->db->from('peserta_data_tpk');
        $this->db->join('tpk', 'tpk.kode_tpk = peserta_data_tpk.kode_tpk', 'left');
        $this->db->where('status_tpk', 'NON TPK');

        $query = $this->db->get();

        $temp = $query->result();

        return $temp;
	}



}

?>