<?php  

class Transaksi extends CI_Model{

	function __construct(){
		parent::__construct();

	}

	public function getAllKelompokTransaksi()
	{
		$query = $this->db->select('*')
							->from('kelompok_transaksi')->get();

		return $query->result();
	}

	public function getAllTransaksi()
	{
		$query = $this->db->select('*')
							->from('transaksi')
							->order_by('waktu', 'desc')->get();

		return $query->result();
	}

	public function getJenisTransaksi($kode_kelompok_transaksi)
	{
		$query = $this->db->get_where('jenis_transaksi', array('kode_kelompok_transaksi' => $kode_kelompok_transaksi));

		return $query->result();
	}

	public function getJenisTransaksiKKEdit($kode_kelompok_transaksi, $kode_group_jenis_peserta)
	{
		$this->db->select('*');
        $this->db->from('jenis_transaksi');
        $this->db->where('kode_kelompok_transaksi', $kode_kelompok_transaksi);
        $this->db->where('jenis_transaksi.kode_jenis_peserta', $kode_group_jenis_peserta);

        $query = $this->db->get();
        $temp = $query->result();



		return $temp;
	}

	public function getJenisTransaksiRekap($kode_kelompok_transaksi, $kode_group_jenis_peserta)
	{

        // 
        $this->db->select('*');
        $this->db->from('jenis_transaksi');

        $this->db->where('kode_kelompok_transaksi', $kode_kelompok_transaksi);
        $this->db->where('kode_jenis_peserta', $kode_group_jenis_peserta);
        $this->db->order_by('kode_group_status_keluarga', 'asc');

        $query = $this->db->get();
        $temp = $query->result();



		return $temp;
	}

	public function getTransaksiRekap($data){

		$this->db->select('*');
        $this->db->from('transaksi');
        $this->db->join('jenis_transaksi', 'jenis_transaksi.kode_jenis_transaksi = transaksi.kode_jenis_transaksi', 'left');
        $this->db->join('kelompok_transaksi', 'kelompok_transaksi.kode_kelompok_transaksi = transaksi.kode_kelompok_transaksi', 'left');
        $this->db->join('admin', 'admin.kode_admin = transaksi.kode_admin', 'left');
        $this->db->like('transaksi.kode_kelompok_transaksi', $data['kode_kelompok_transaksi']);
        $this->db->like('kode_jenis_peserta', $data['kode_group_jenis_peserta']);
        $this->db->like('transaksi.kode_jenis_transaksi', $data['kode_jenis_transaksi']);
        $this->db->where('waktu >=', $data['periode_1']);
        $this->db->where('waktu <=', $data['periode_2']);

        $query = $this->db->get();
        $temp = $query->result();



		return $temp;
	}


	public function getJenisTransaksiByJenisPeserta($kode_jenis_peserta)
	{
		$query = $this->db->get_where('jenis_transaksi', array('kode_jenis_peserta' => $kode_jenis_peserta));

		return $query->result();
	}

	public function getKelompokTransaksi($kode_kelompok_transaksi)
	{
		$query = $this->db->get_where('kode_kelompok_transaksi', array('kode_kelompok_transaksi' => $kode_kelompok_transaksi));

		return $query->result();
	}

	public function getJenisTransaksiById($kode_jenis_transaksi)
	{
		$query = $this->db->select('*')
							->from('jenis_transaksi')
							->where('kode_jenis_transaksi', $kode_jenis_transaksi)->get();

		return $query->result();
	}

	public function getTransaksi($kode_jenis_transaksi)
	{
		$query = $this->db->get_where('transaksi', array('kode_jenis_transaksi' => $kode_jenis_transaksi));

		return $query->result();
	}




}

?>